


<select name="{{$currencyName}}" id="{{$currencyId}}" class="form-control select-search {{$currencyClass}}" {{$otherAttr}}>
    <option value="BDT" selected>BDT</option>
    <option value="USD">USD</option>
    <option value="EUR">EUR</option>
    <option value="INR">INR</option>
    <option value="GBP">GBP</option>
    <option value="JPY">JPY</option>
    <option value="CNY">CNY</option>
    <option value="RUB">RUB</option>
</select>

