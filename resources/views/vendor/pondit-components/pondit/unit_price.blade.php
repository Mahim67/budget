<div class="">
    <label for="{{$id}}" class="col-form-label">{{$label}}</label>
    <div class="row">
        <div class="col-8">
            <input type="number" name="{{$name}}" id="{{$id}}" class="form-control {{$class}}" {{$otherAttr}} value="{{$value}}" placeholder="{{ $unitPricePlaceHolder }}">
        </div>
        <div class="col-4">
            <x-pondit-currency currencyName="{{$currencyName}}" class="w-50 ml-3" />
        </div>
    </div>
</div>