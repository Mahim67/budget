<input list="{{$qtyUnitList}}" name="{{$qtyUnitName}}" id="{{$qtyUnitId}}" class="form-control {{$qtyUnitClass}}" {{$otherAttr}} value="Each">

<datalist id="{{$qtyUnitList}}" placeholder="Select Qty Unit" >
  <option value="Piece" >
  <option value="Each">
  <option value="Set">
  <option value="Pair">
</datalist>
