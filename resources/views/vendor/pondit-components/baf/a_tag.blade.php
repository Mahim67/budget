
<a href="{{ $url }}" id="{{ $id }}"
    class="btn btn-sm bg-{{ $color }} {{ $btnType }} {{ $btnSize }} {{ $class }}"  data-popup="tooltip" title="{{ $tooltip }}" data-original-title="Top tooltip">
    <i class=" fas fa-{{ $icon }} {{ $iconSize }}"></i>
    {{ $title }}
</a>