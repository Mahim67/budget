<div class="form-group">
    @if ($label == true)
    <label for="{{$id}}">@lang($label)</label>
    @endif
    <select name="{{$name}}" id="{{$id}}" class="form-control {{$class}}" {{$otherAttr}} >
        <option value=""> Select Trade </option>
        @foreach ($trades as $key=>$item)
            @if ($key != $selected)
            <option value="{{$key}}">{{$item}}</option>
            @else
            <option value="{{$key}}" selected='selected'>{{$item}}</option>
            @endif
        @endforeach
    </select>
</div>