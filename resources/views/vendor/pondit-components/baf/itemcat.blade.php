<div class="form-group row">
    <label for="{{$id}}" class="col-md-2">@lang($label)</label>
    <div class="col-md-10">
        <select name="{{$name}}" id="{{$id}}" class="form-control select-search {{$class}}" {{$otherAttr}}>
            <option value="">-- Select Item Category --</option>
            @foreach ($lists as $key=>$item)
            @if ($key != $selected)
            <option value="{{$key}}" >{{$item}}</option>
            @else 
            <option value="{{$key}}" selected='selected'>{{$item}}</option>
            @endif
            @endforeach
        </select>
    </div>
</div>
