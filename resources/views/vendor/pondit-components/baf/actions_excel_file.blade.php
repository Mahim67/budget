<a data-popup="tooltip" title= @lang($tooltip) data-original-title= @lang($tooltip)  href="{{ $url }}" class="{{ $class }}" id="{{ $id }}">
    <i class="fas fa-{{ $icon }} px-1 py-1 bg-{{$color}}"></i>
    {{ $title }}
</a>