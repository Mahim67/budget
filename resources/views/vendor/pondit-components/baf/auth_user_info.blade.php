
<?php
if (date('m') > 6) {
    $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
} else {
    $fin_year = date('Y') . "-" . (date('Y') + 1);
}
?>

<input name="fin_year" type="hidden" value="{{$fin_year}}"/>
<input name="bdnumber" id="bdnumber" type="hidden" />
<input name="base_name" id="base_name" type="hidden" />
<input name="office_id" id="office_id" type="hidden" />
<input name="office_name" id="office_name" type="hidden" />


@push('js')
<script>
    var authUser = sessionStorage.getItem('authUser');
    var parseAuthUser = JSON.parse(authUser);
    console.log(parseAuthUser);
    document.getElementById("bdnumber").value = parseAuthUser.service_number;
    document.getElementById("base_name").value = parseAuthUser.base_or_unit;
    document.getElementById("office_id").value = parseAuthUser.office_id;
    document.getElementById("office_name").value = parseAuthUser.office_name;
</script>
@endpush