<table class="table table-bordered table-hover table-responsive">
    <thead class="bg-light">
        <tr class="text-center">
            <th class="w-50">Name Of User</th>
            <th class="w-50">Received At</th>
            <th class="w-50">Status</th>
            <th class="w-50">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr class="text-center">
            <td>Bashar Base</td>
            <td>02/03/2021</td>
            <td><i class="fa fa-check text-success"></i></td>
            <td><a href="#" class="btn btn-primary btn-sm">Reminder <i class="fa fa-bell"></i></a></td>
        </tr>
    </tbody>
</table>