var sendHttpRequest = (method, url, data = null) => {
	return fetch(url, {
		method: method,
		body: data ? JSON.stringify(data) : null,
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
			Authorization: "Bearer " + auth.getToken(),
		},
	}).then((response) => {
		if (response.status >= 400) {
			return response.json().then((errResData) => {
				var error = new Error("Something went wrong!");
				error.data = errResData;
				throw error;
			});
		}
		return response.json();
	});
};

var sendHttpRequestFile = (method, url, data) => {
	return fetch(url, {
		method: method,
		body: data,
		headers: data
			? {
					Accept: "application/json",
					Authorization: "Bearer " + auth.getToken(),
			  }
			: {},
	}).then((response) => {
		if (response.status >= 400) {
			// !response.ok
			return response.json().then((errResData) => {
				var error = new Error("Something went wrong!");
				error.data = errResData;
				throw error;
			});
		}
		return response.json();
	});
};
function get_updated_auth_user() {
	sendHttpRequest("POST", API_BASE_URL + `user`)
		.then((responseData) => {
			if (responseData.data == "Unauthenticated") {
				auth.destroyToken();
				window.location.href = BASE_URL;
			}

			if (responseData.status == "error") {
				alert(responseData.message);
				return true;
			}
			var user = JSON.stringify(responseData.data);
			update_storage(user)
		})
		.catch((err) => {
			console.log(err, err.data);
		});
}

function update_storage(input_auth_user) {
	var authUser = sessionStorage.getItem("authUser");
	if (authUser) {
		sessionStorage.setItem("authUser", input_auth_user);
		
	}
}

const INDENT_COVERING_LETTER_SIGN = 4;
const PROCUREMENT_TYPE_INDENT = "indent";
const DTE_SUP_OFFICE_ID = 15;
const ACT_BACK = "back";
const ACT_FWD = "fwd";
const ACK = "ack";
const CONCERN_GROUP = "Concern Group";
const APPOINTMENT = "Appointment";
const CHECKER = "Checker";

const STORAGE_AUTH_USER = sessionStorage.getItem('authUser');
const PARSE_STORAGE_AUTH_USER = JSON.parse(STORAGE_AUTH_USER);


