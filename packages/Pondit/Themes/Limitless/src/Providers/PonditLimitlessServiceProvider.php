<?php

namespace Pondit\Themes\Limitless\Providers;

use Illuminate\Support\ServiceProvider;

class PonditLimitlessServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadTranslationsFrom(__DIR__.'/../../Resources/lang', 'pondit-limitless');

        $this->loadRoutesFrom(__DIR__ . '/../Routes/web.php');
        
        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('vendor/pondit/themes/limitless/assets'),

            __DIR__ . '/../../publishable/global_assets' => public_path('vendor/pondit/themes/limitless/global_assets'),
            
        ], 'public');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'pondit-limitless');
    }
}
