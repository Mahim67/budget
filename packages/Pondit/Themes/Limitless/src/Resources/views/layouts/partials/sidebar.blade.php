<!-- Main sidebar -->

<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">
    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        <span class="font-weight-semibold">Navigation</span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body">
                <div class="card-body text-center">
                </div>

                <div class="sidebar-user-material-footer">
                    <a href="#user-profile-nav"
                        class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle legitRipple"
                        data-toggle="collapse" aria-expanded="true"><span>My account</span></a>
                </div>
            </div>
            @push('js')
            <script>
                $(document).ready(function() {
                        $("#my_account").click();
                    });

            </script>
            @endpush



            <div class="collapse hide" id="user-profile-nav" style="">

                <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="#" class="nav-link legitRipple">
                            <i class="icon-user-plus"></i>
                            <span>{{ __('Profile') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link legitRipple">
                            <i class="fa fa-pen"></i>
                            <span>{{ __('Update Profile') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link legitRipple">
                            <i class="icon-comment-discussion"></i>
                            <span>Pending Documents</span>
                            <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <form method="POST" action="#">
                            @csrf
                            <a href="#" onclick="event.preventDefault();
        this.closest('form').submit();" class="nav-link legitRipple">
                                <i class="icon-switch2"></i>
                                <span>{{ __('Log Out') }}</span>
                            </a>
                        </form>
                    </li>
                    <li class="nav-item">

                    </li>
                </ul>
            </div>

            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">
                    <div class="card  mx-1 my-2 border-primary progressbar-card">
                        <div class="appointment d-flex justify-content-around pb-2 pl-2 pr-2 mt-1">
                            <div class="appointment-image pr-1">
                                <img src="{{ asset('/vendor/pondit/themes/limitless/assets/img/user.png') }}"
                                    width="60px" class="mt-1" height="auto" alt="">
                            </div>
                            <div class="appointment-details pl-1">

                            </div>
                        </div>

                    </div>
                </ul>
                <ul class="nav nav-sidebar" data-nav-type="accordion">
                    <div class="check_zero">
                        <div class="card  mx-1 my-2 border-primary progressbar-card check_zero">
                            <div class="card-header header-elements-inline justify-content-end">
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        <a class="list-icons-item" data-action="remove"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <span class="mx-1 text-dark"> You have <a href="#"
                                        class="text-danger pending_task_count"> </a> pending
                                    documents.</span>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>


        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">

            <ul class="nav nav-sidebar">
                <li class="nav-item bg-success">
                    <a href="#" class="nav-link text-light">
                        <i class="icon-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

            
                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('budgetcodes') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Budgetcode</span>
                    </a>
                </li>
                {{-- <li class="nav-item nav-item-submenu ">
                    <a href="" class="nav-link ">
                        <i class="fas fa-arrows-alt"></i>
                        <span></span>
                    </a>
                </li> --}}


                



                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('budget-allotments') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Budget Allotment</span>
                    </a>
                </li>
                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('budget-transfers') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Budget Transfer</span>
                    </a>
                </li>
                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('suplimentary-budget-allotments') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Supplementary  Allotments</span>
                    </a>
                </li>
                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('allotment-brackdown') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Allotment Breakdown</span>
                    </a>
                </li>
                {{-- <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('budget-allotment-brackdown') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Budget Allotment Brackdown</span>
                    </a>
                </li> --}}
                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('expenditure-detials') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Expenditure Detials</span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ route('base-letter.index') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Budget Letter ( Base )</span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{route('register-book.index')}}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Register Book</span>
                    </a>
                </li>

                <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{route('base-expenditure.index')}}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Budget for Base</span>
                    </a>
                </li>
                {{-- <li class="nav-item nav-item-submenu bg-success">
                    <a href="{{ url('budgetcode-to-item') }}" class="nav-link text-light">
                        <i class="fas fa-arrows-alt"></i>
                        <span>Budgetcode To Item</span>
                    </a>
                </li> --}}
            </ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->
</div>
<!-- /main sidebar -->
