<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>BAF - Airforce Inventory Management System</title>
    {{-- <link rel="shortcut icon" href="{{ asset('') }}{{ $themepath }}/global_assets/images/logo-sm.png"> --}}
    <link rel="shortcut icon" href="{{ asset('') }}/global_assets/images/logo-sm.png">


    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/font-family.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/icons/fontawesome/styles.min.css"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/icons/material/styles.min.css"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/icons/icomoon/styles.min.css"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/bootstrap.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/bootstrap_limitless.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/layout.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/components.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/colors.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/custom.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/style.css" rel="stylesheet"
        type="text/css">
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/assets/js/app.js"></script>


    @stack('css')
    <!-- /global stylesheets -->
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/assets/js/axios.min.js"></script>
</head>

<body>

    <!-- Main navbar -->
    @include('pondit-limitless::layouts.partials.header')
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        @include('pondit-limitless::layouts.partials.sidebar')

        <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            @include('pondit-limitless::layouts.partials.page_header')
            
            {{-- @include('pondit-limitless::elements.success')
            @include('pondit-limitless::elements.error') --}}
            
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                @yield('content')
            </div>
            <!-- /content area -->

            <!-- Footer -->
            @include('pondit-limitless::layouts.partials.footer')
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    <!-- Core JS files -->
    

    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/main/jquery.min.js"></script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/main/bootstrap.bundle.min.js">
    </script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/loaders/blockui.min.js">
    </script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/ui/ripple.min.js"></script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/demo_charts/loader.js"></script>
    <!-- /core JS files -->
    <!-- Bootstrap Comfirmation Js -->
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/assets/js/bootstrap-confirmation/bootstrap-confirmation.js"></script>

    @stack('js')



</body>

</html>
