@if (session('message'))

    <div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible" id="success">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold">Well done!</span> {{ session('message') }}
    </div>

@endif
<link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/iziToast.min.css" rel="stylesheet"
    type="text/css">
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/notifications/iziToast.min.js"></script>
@if (session()->has('success'))
    <script>
        iziToast.success({
            message: ' {!! session('success') !!}',
            position: "topRight"
        });

    </script>
@endif

<script>
    // setTimeout(function() {
    //     let success = document.getElementById('success');
    //     if (success) {
    //         success.style.display = "none";
    //     }
    // }, 5000);
</script>
