@if (isset($errors) && $errors->any())
    <div class="alert alert-danger alert-styled-left alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold">Oh snap!</span>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

@endif

 @if(session()->has('looping_error'))
        <div class="alert alert-warning alert-dismissible bg-white text-danger fade show" role="alert" id="error">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            @if (is_array(session('looping_error')))      
            @forelse (session('looping_error') as $key=>$item)
            <div>
                @if (gettype($item) == "string")
                {{ $item }}
                @endif
            </div>
            @empty
            @endforelse
            @endif
        </div>
@endif

<script>
    setTimeout(function() {
        let error = document.getElementById('error');
        if (error) {
            error.style.display = "none";
        }
    }, 5000);
</script>
