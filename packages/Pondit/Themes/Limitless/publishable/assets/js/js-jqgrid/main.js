(function($){
	$(document).ready(function(){

		get_batch_current_data()

		$.jgrid.defaults.styleUI = 'Bootstrap4';
        $.jgrid.defaults.iconSet = "Octicons";

	});
})(jQuery)

function get_batch_current_data()
{

		$.ajax({
		url 		: `http://127.0.0.1:8000/api/appointment`,
		// url 		: `http://batch-tracking-api.test/report/batch-current-stock`,
		method		: 'get',
		dataType 	: 'json',
		beforeSend 	: function(data){
			console.log('loading ...');
		},
		success 	: function(res){
			console.log(res);
			render_batch_current_report(res);
		},
		error 	 	: function(error){
			console.log("ERROR :"+ error);
		},
        complete 	: function(){
			
				$("#jqGrid").jqGrid('filterToolbar', 
				{ 
					stringResult : true, 
					searchOnEnter: false, 
					defaultSearch: "cn" 
				});
				$("#jqGrid").jqGrid("navGrid","#jqGridPager",
				{
					edit 		: false,
                    add 		: false,
                    del 		: false,
                    search 		: true,
                    refresh		: true,
                    position  	: "left",
                    cloneToTop	: false
				},
				{}, // edit options
	            {}, // add options
	            {},
				{ 
					multipleSearch: true, 
					multipleGroup : true,
				});
        }
	});
}


function render_batch_current_report(res)
{
	$('#jqGrid').jqGrid({
		datatype: "local",
		data 	: res.data,
		colModel: [
		{ label : 'Title', name:'title',align : 'left',firstsortorder:'asc',formatter:'string'},
		{ label : 'Title Short',name:'short_title',formatter:'string',align :'left',firstsortorder:'asc'},
		// { label : 'Style No', name:'style_no',formatter:'string',firstsortorder:'asc'},
		// { label : 'Color', name:'color',firstsortorder:'asc' },
		// { label : 'Color Type.', name:'color_type',firstsortorder:'asc' },
		// { label :'Size',name:'size',formatter:'string',align:'center',firstsortorder:'asc'},
		// { label : 'Dia', name:'dia', firstsortorder:'asc', align:'center'},
		// { label : 'Gauge', name:'gauge', firstsortorder:'asc', align:'center'},
		// { label : 'Gsm', name:'gsm', formatter:'string',firstsortorder:'asc', align:'center'},
		// { label : 'Yarn Lot', name:'yarn_lot', formatter:'string',firstsortorder:'asc', align:'center'},
		// { label : 'G/Delivary Ref', name:'gstore_delivery_ref', formatter:'string',firstsortorder:'asc',align:'center',},
		// { label : 'Batch No', name:'batch_no',formatter:'string',sorttype:'string',firstsortorder:'asc', align:'center',summaryTpl : "{0}", summaryType: "count"},
		// { label : 'Rolls', name:'rolls', formatter:'integer',firstsortorder:'asc', align : 'center',summaryTpl : "{0}", summaryType: "sum"},
		// { label : 'Gray Qty', name:'grey_qty', formatter:'number',firstsortorder:'asc', align:'right',summaryTpl : "{0}", summaryType: "sum"},
		// { label : 'Received at',name:'received_at',formatter:'string',firstsortorder:'asc',align:'center'},
		// { label : 'Received by',name:'username',formatter:'string',firstsortorder:'asc',align:'center'},
		  
		],
		rowNum  			: 5000,
		rownumbers			: true,
		pager   			: '#jqGridPager',
		pgbuttons			: true,
		toppager			: false,
		caption 			: `CURRENT STOCK REPORT`,
		height  			: '500',
		width  				: '1600',
		headertitles        : false,
		gridview			: true,
		footerrow			: true,
		userDataOnFooter 	: true,
		viewrecords			: true,
		// sortname			: 'buyer_code',
		subGrid 			: false,
		subGridModel 		: [],
		responsive 			: true,
		multiselect			: false,
		grouping 			: true,
		// groupingView 		: {
			
		// 	// groupField 	   : ['buyer_code'],	
		// 	groupColumnShow: [true],
		// 	// groupText	   : [
		// 	// 	"BUYER : <b>{0}</b>"
		// 	// ],
		// 	groupOrder	  : ["asc", "asc","asc"],
		// 	groupSummary  : [false,false,false,true],
		// 	groupCollapse : false,	   	
		// },
		// gridComplete 		: function(){
		// 	// total batch_no count
		// 	var $grid 		 = $('#jqGrid');
		// 	var get_batch_column = $grid.jqGrid('getCol', 'batch_no');
				
		// 	$grid.jqGrid('footerData', 'set', { 
		// 		'batch_no'	: get_batch_column.length==0?0:get_batch_column.length,
		// 	});
			

		// 	// var allBuyers	= $(document).find('.jqGridghead_0');
		// 	total_G_Qty 	= 0;
		// 	total_rolls_Qty = 0;

		// 	// total grey qty sum
		// 	var get_grey_qty = $grid.jqGrid('getCol', 'grey_qty');
		// 	$.each(get_grey_qty,function (i,v) {
		// 		total_G_Qty += parseFloat(v);
		// 	});

		// 	$grid.jqGrid('footerData', 'set', { 
		// 		'grey_qty'	: total_G_Qty,
		// 	});


		// 	// total rolls qty count
		// 	var get_rolls = $grid.jqGrid('getCol', 'rolls');
		// 	$.each(get_rolls,function (i,v) {
		// 		total_rolls_Qty += parseFloat(v);
		// 	});

		// 	$grid.jqGrid('footerData', 'set', { 
		// 		'rolls'	: total_rolls_Qty,
		// 	});

		// 	// total buyers count

		// 	var get_buyers = $grid.jqGrid('getCol', 'buyer_code');
		// 	var buyers=[];
		// 	for(var i=0; i<get_buyers.length; i++)
		// 	{
		// 		buyers[get_buyers[i]]=(get_buyers[i]);
		// 	}
		
		// 	var countBuyers = Object.keys(buyers).length;
		// 	// console.log(ol);
		// 	$grid.jqGrid('footerData', 'set', { 
		// 		'buyer_code'	: 'BUYERS : '+countBuyers,
		// 	});

		// 	$('.ui-jqgrid').css('overflow-x', 'auto');
		// },
		colMenu : true, 
	});

}
