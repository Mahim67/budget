<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionsIndex extends Component
{
    public $url
           ,$id
           ,$class
           ,$icon
           ,$tooltip
           ,$title;

    public function __construct(
        $url = '#', $icon = 'list',
        $tooltip = "widgets::lang.list", $id = false,
        $title = false, $class = false  )
    {
        $this->id = $id;
        $this->url = $url;
        $this->icon = $icon;
        $this->class = $class;
        $this->title = $title;
        $this->tooltip = $tooltip;
    }
    
    public function render()
    {
        return view('widgets::baf.actions_index');
    }
}
