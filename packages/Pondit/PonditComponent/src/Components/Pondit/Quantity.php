<?php

namespace Pondit\PonditComponent\Components\Pondit;

use Illuminate\View\Component;

class Quantity extends Component
{
    public  $class
           ,$label
           ,$value
           ,$name
           ,$id
           ,$qtyUnitName
           ,$qtyUnitId
           ,$qtyUnitList
           ,$qtyUnitClass
           ,$qtyPlaceholder
           ,$otherAttr;

    public function __construct(
        $id = "qty",
        $class = false,
        $label =  false,
        $name = false,
        $value = false,
        $qtyUnitName = false,
        $qtyUnitId = false,
        $qtyUnitList = false,
        $qtyUnitClass = false,
        $qtyPlaceholder = false,
        $otherAttr = false)
    {
        $this->id             = $id;
        $this->class          = $class;
        $this->label          = $label;
        $this->name           = $name;
        $this->value          = $value;
        $this->qtyUnitName    = $qtyUnitName;
        $this->qtyUnitId      = $qtyUnitId;
        $this->qtyUnitList    = $qtyUnitList;
        $this->qtyUnitClass   = $qtyUnitClass;
        $this->qtyPlaceholder = $qtyPlaceholder;
        $this->otherAttr      = $otherAttr;
    }
    
    public function render()
    {
        return view('widgets::pondit.quantity');
    }
}
