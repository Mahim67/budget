<?php

namespace Pondit\PonditComponent\Components\Pondit;

use Illuminate\View\Component;

class ExcelImporter extends Component
{
    public $topClass
           ,$labelClass
           ,$label
           ,$name
           ,$selectClass
           ,$selectPlaceholder
           ,$finYears;

    public function __construct(
        $topClass = false,
        $labelClass = false,
        $label =  "widgets::lang.fin year",
        $name = false,
        $selectClass = false,
        $selectPlaceholder = false,
        $finYears = false)
    {
        $this->topClass          = $topClass;
        $this->labelClass        = $labelClass;
        $this->label             = $label;
        $this->name              = $name;
        $this->selectClass       = $selectClass;
        $this->selectPlaceholder = $selectPlaceholder;
        $this->finYears          = $finYears;
    }
    
    public function render()
    {
        return view('widgets::pondit.excel_importer');
    }
}
