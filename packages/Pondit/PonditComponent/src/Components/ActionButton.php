<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionButton extends Component
{
    public $href
           ,$id
           ,$bg
           ,$class
           ,$icon
           ,$tooltip
           ,$title;

    public function __construct(
        $href = '#', $icon, $tooltip, 
        $id, $bg, $class, $title )
    {
        $this->href     = $href;
        $this->id       = $id;
        $this->bg       = $bg;
        $this->class    = $class;
        $this->icon     = $icon;
        $this->tooltip  = $tooltip;
        $this->title    = $title;
    }
    
    public function render()
    {
        return view('widgets::pondit.limitless.action_button');
    }
}
