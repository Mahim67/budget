<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class Form extends Component
{
    public $action
           ,$id
           ,$class
           ,$method;

    public function __construct(
        $action = '#',  
        $id = false, $class = false, $method = 'POST' )
    {
        $this->action   = $action;
        $this->id       = $id;
        $this->class    = $class;
        $this->method   = $method;
    }
    
    public function render()
    {
        return view('widgets::pondit.form');
    }
}
