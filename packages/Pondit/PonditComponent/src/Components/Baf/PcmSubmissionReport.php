<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;
use Pondit\PonditComponent\Components\Baf\PcmSubmissionReport;

class PcmSubmissionReport extends Component
{
    
    // public function __construct(
       
    // )
    // {
       
    // }
    
    public function render()
    {
        return view('widgets::baf.pcm-submission-report');
    }
}
