<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;

class AuthUserInfo extends Component
{
    public function __construct()
    { }
    
    public function render()
    {
        return view('widgets::baf.auth_user_info');
    }
}
