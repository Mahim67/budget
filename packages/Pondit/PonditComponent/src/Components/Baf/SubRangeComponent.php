<?php

namespace Pondit\PonditComponent\Components\Baf;
use Illuminate\View\Component;
use Pondit\Baf\MasterData\Models\SubRange;
use Pondit\PonditComponent\Components\Baf\SubRangeComponent;

class SubRangeComponent extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$selected
           ,$otherAttr;

    public function __construct(
        $id         =  false,
        $label      =  false,
        $name       =  false,
        $class      =  false,
        $otherAttr  =  false,
        $selected   =  false
    )
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->otherAttr    = $otherAttr;
        $this->selected     = $selected;
    }
    
    public function render()
    {
        $ranges = SubRange::pluck('title', 'id')->toArray();
        return view('widgets::baf.sub-range', compact('ranges'));
    }
}
