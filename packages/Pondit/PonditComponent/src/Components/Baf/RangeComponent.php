<?php

namespace Pondit\PonditComponent\Components\Baf;
use Illuminate\View\Component;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\PonditComponent\Components\Baf\RangeComponent;

class RangeComponent extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$selected
           ,$otherAttr;

    public function __construct(
        $id         =  false,
        $label      =  false,
        $name       =  false,
        $class      =  false,
        $otherAttr  =  false,
        $selected   =  false
    )
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->otherAttr    = $otherAttr;
        $this->selected     = $selected;
    }
    
    public function render()
    {
        $ranges = Range::orderBy('sequence_number','ASC')->pluck('title', 'id')->toArray();
        return view('widgets::baf.range', compact('ranges'));
    }
}
