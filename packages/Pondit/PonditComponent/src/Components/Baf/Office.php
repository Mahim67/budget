<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;

class Office extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$otherAttr;

    public function __construct(
        $id         =  "officeName",
        $label      =  "widgets::lang.Office Name",
        $name       =  "officename",
        $class      =  false,
        $otherAttr  =  false
    )
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->otherAttr    = $otherAttr;
    }
    
    public function render()
    {
        return view('widgets::baf.office');
    }
}
