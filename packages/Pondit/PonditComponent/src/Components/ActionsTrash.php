<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionsTrash extends Component
{
    public $url
           ,$id
           ,$class
           ,$icon
           ,$tooltip
           ,$title;

    public function __construct(
        $url = '#', $icon = 'list', $title = false,
        $tooltip = false, $class = false, $id = false )
    {
        $this->id = $id;
        $this->url = $url;
        $this->icon = $icon;
        $this->class = $class;
        $this->title = $title;
        $this->tooltip = $tooltip;
    }
    
    public function render()
    {
        return view('widgets::baf.actions_trash');
    }
}
