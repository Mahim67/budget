<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionLinkDelete extends Component
{
    public $id
           ,$url
           ,$class
           ,$icon
           ,$tooltip
           ,$title;

    public function __construct(
        $icon = 'trash', $url = false,
        $tooltip = false, $id = false,
        $class = false, $title = false 
        )
    {
        $this->id = $id;
        $this->url = $url;
        $this->icon = $icon;
        $this->class = $class;
        $this->title = $title;
        $this->tooltip = $tooltip;
    }
    
    public function render()
    {
        return view('widgets::pondit.limitless.action_link_d');
    }
}
