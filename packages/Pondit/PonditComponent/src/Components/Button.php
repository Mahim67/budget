<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class Button extends Component
{
    public $type
           ,$id
           ,$bg
           ,$class
           ,$icon
           ,$float
           ,$title
           ,$onclick;

    public function __construct(
        $type = 'submit', $icon = false, $title = false, $float = 'right',
        $id = false, $class = false, $bg = 'success' ,$onclick = null )
    {
        $this->type     = $type;
        $this->id       = $id;
        $this->class    = $class;
        $this->icon     = $icon;
        $this->bg       = $bg;
        $this->float    = $float;
        $this->title    = $title;
        $this->onclick  = $onclick;
    }
    
    public function render()
    {
        return view('widgets::pondit.button');
    }
}
