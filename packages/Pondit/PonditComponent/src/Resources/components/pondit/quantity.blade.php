<div class="form-group">
    <label for="{{$id}}" class="col-form-label"> @lang($label)</label>
    <div class="row">
        <div class="col-8">
            <input type="number" class="form-control {{$class}}" name="{{$name}}" id="{{$id}}" {{$otherAttr}} value="{{$value}}" placeholder="{{ $qtyPlaceholder }}">
        </div>
        <div class="col-4">
            <x-pondit-qty-unit class="" qtyUnitName={{$qtyUnitName}} qtyUnitId={{$qtyUnitId}} qtyUnitList={{$qtyUnitList}}/>
        </div>
    </div>
</div>
