      
{{-- <div class='form-group row {{ $topClass }}'>
    <label for="fin_years" name="{{ $name }}" class="col-sm-2 col-form-label {{ $labelClass }}">
        @lang($label)
    </label>
    <div class='col-sm-10'>
        <select name="{{ $name }}" id="fin_years" class="form-control select-search {{ $selectClass }}" >
            @if (!empty($selectPlaceholder))
            <option>{{ $selectPlaceholder }}</option>
            @endif
        </select>
    </div>
</div> --}}
<div class="row form-group">
    {{-- <label for="" class="col-2 mt-1">Pre PCM</label> --}}
    <div class="col-10 d-flex">
        <input type="file">
        <a href="" class="ml-3">pre-pcm-sample.xlxs</a>
    </div>
    {{-- <small>Upload Your Demand</small> --}}
</div>


@push('js')

<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/financial-years/select2.min.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/financial-years/form_select2.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/financial-years/main.js"></script>

@endpush