
<table class="table {{ $tableClass }}">
    <thead class=" {{ $theadClass }}">
        <tr>
            {{ $thead }}
        </tr>
    </thead>
    <tbody class=" {{ $tbodyClass }}">
        {{ $slot }}
    </tbody>
    @if ($tfooter)
    <tfoot class=" {{ $tfootClass }}">
        {{ $tfooter }}
    </tfoot>
    @endif
</table>