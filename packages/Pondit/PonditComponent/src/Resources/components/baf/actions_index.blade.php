<a href="{{ $url }}" id="{{ $id }}" class="{{ $class }} btn btn-outline bg-indigo btn-icon text-indigo btn-sm border-indigo border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title= @lang($tooltip)  data-original-title= @lang($tooltip)>
    <i class="fas fa-{{ $icon }}"></i>
    {{ $title }}
</a>
