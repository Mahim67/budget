<div class="text-right">
    <a href="#" class="action-badge badge btn-sm text-indigo-400" data-toggle="modal" data-target="#exampleModalCenter">
        <i class="fas fa-edit"></i> {{$buttonName}}
    </a>
</div>

<form action="{{$url}}" method="POST">
    <div class="modal fade" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <input type="hidden" name="" id="appointmentList_officewise" value="">
                <input type="hidden" name="" id="concerGroupList_officewise" value="">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">{{$title}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row mb-0">
                        <label for="to_office_id" class="col-sm-5 col-form-label text-l">Office</label>
                        <div class="col-sm-7">
                            <select onchange="get_action_appointment_by_id(this)" name="to_office_id"
                                class="form-control w-25 select-search" data-fouc>
                                <option value="">Chosse Office</option>
                                @if (isset($dataList))
                                @forelse ($dataList['movement_offices'] as $key_id_of_office =>$each_office)
                                <option value="{{ $key_id_of_office }}">{{ $each_office }}</option>
                                @empty
                                <option value="">Not found</option>
                                @endforelse
                                @endif
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row mb-0 display_none_class" id="forward_entity_type">
                        <label class="col-form-label col-lg-5"></label>
                        <div class="col-sm-7">
                            @if (isset($dataList))
                            @forelse ($dataList['movement_entity_types'] as $key_entity_type=>$each_entity_type)
                            <div class="">
                                <label class="">
                                    <input type="radio" value="{{ $key_entity_type }}" class="" name="entity_type">
                                    {{ $each_entity_type }}
                                </label>
                            </div>
                            @empty
                            @endforelse
                            @endif
                        </div>
                    </div>

                    <div class="form-group row display_none_class" id="forward_action">
                        <label class="col-form-label col-lg-5" id="appointment_concern_action_heading">Action</label>
                        <div class="col-lg-7">
                            <select required name="entity_id" id="appointment_concern_action_data"
                                class="form-control w-25 select-search">
                                <option value="">Select Action</option>
                            </select>
                        </div>

                    </div>
                    <div class="form-group row mb-0 display_none_class" id="forward_ack">
                        <label for="action" class="col-sm-5 col-form-label">
                            Appointment (Ack)</label>
                        <div class="col-sm-7">
                            <select name="ackonowledge_appointment_id" id="movement_acknowledgements"
                                class="form-control select-search" data-fouc>
                                <option value="">Select Appointment</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-0 display_none_class" id="forward_pass">
                        <label for="password" class="col-sm-5 col-form-label">
                            Password</label>
                        <div class="col-sm-7">
                            <input type="password" name="password_field" id="password_field" required
                                class="form-control">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-sm bg-success">
                        Send
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </button>
                </div>

            </div>
        </div>
    </div>
</form>
@push('js')

<script>
    function get_action_appointment_by_id(input){
        var forward_entity_type = document.getElementById("forward_entity_type");
        var forward_action = document.getElementById("forward_action");
        var forward_ack = document.getElementById("forward_ack");
        var forward_password = document.getElementById("forward_pass");
        let office_id = input.value;
        sendHttpRequest(
		"POST",
		API_BASE_URL +
            `offices/${office_id}/get-entity`)
		.then((responseData) => {
            console.log(responseData.data);
             forward_action.classList.remove("display_none_class");
             forward_ack.classList.remove("display_none_class");
             forward_password.classList.remove("display_none_class");
             forward_entity_type.classList.remove("display_none_class");
            let movement_acknowledgements = document.getElementById("movement_acknowledgements");
            let password_field = document.getElementById("password_field");
             let ack = (responseData.data.acknowledge);
             movement_acknowledgements.innerHTML="";
            //  password_field.value="";
             document.getElementById("appointment_concern_action_data").innerHTML = "";
             if(document.querySelector('input[type=radio][name=entity_type]:checked')){
                document.querySelector('input[type=radio][name=entity_type]:checked').checked = false;
             }
             var options = "";
              options += "<option value=''>Select Appointment</option>";
            for (let property_ack in ack) {
            options += "<option value='"+ property_ack +"'>"+ ack[property_ack] +"</option>";
            }
            movement_acknowledgements.innerHTML = options
            document.getElementById("appointmentList_officewise").value=JSON.stringify(responseData.data.appointments)
            document.getElementById("concerGroupList_officewise").value=JSON.stringify(responseData.data.concern_groups)
		})
		.catch((err) => {
			console.log(err, err.data);
        });
    }
</script>
<script>
    function get_action_appointment_by_id(input){
        var forward_entity_type = document.getElementById("forward_entity_type");
        var forward_action = document.getElementById("forward_action");
        var forward_ack = document.getElementById("forward_ack");
        var forward_password = document.getElementById("forward_pass");
        let office_id = input.value;
        sendHttpRequest(
		"POST",
		API_BASE_URL +
            `offices/${office_id}/get-entity`)
		.then((responseData) => {
            console.log(responseData.data);
             forward_action.classList.remove("display_none_class");
             forward_ack.classList.remove("display_none_class");
             forward_password.classList.remove("display_none_class");
             forward_entity_type.classList.remove("display_none_class");
            let movement_acknowledgements = document.getElementById("movement_acknowledgements");
            let password_field = document.getElementById("password_field");
             let ack = (responseData.data.acknowledge);
             movement_acknowledgements.innerHTML="";
            //  password_field.value="";
             document.getElementById("appointment_concern_action_data").innerHTML = "";
             if(document.querySelector('input[type=radio][name=entity_type]:checked')){
                document.querySelector('input[type=radio][name=entity_type]:checked').checked = false;
             }
             var options = "";
              options += "<option value=''>Select Appointment</option>";
            for (let property_ack in ack) {
            options += "<option value='"+ property_ack +"'>"+ ack[property_ack] +"</option>";
            }
            movement_acknowledgements.innerHTML = options
            document.getElementById("appointmentList_officewise").value=JSON.stringify(responseData.data.appointments)
            document.getElementById("concerGroupList_officewise").value=JSON.stringify(responseData.data.concern_groups)
		})
		.catch((err) => {
			console.log(err, err.data);
        });
        
    }

     $(function(){
    $('input[type="radio"]').click(function(){
        if ($(this).is(':checked'))
        {
            let type_appointment_concerngroup = $(this).val();
            let appointment_concern_action_data = document.getElementById("appointment_concern_action_data");
            let appointment_concern_action_heading = document.getElementById("appointment_concern_action_heading");
            appointment_concern_action_data.innerHTML="";
            let option = "";
            // console.log(type_appointment_concerngroup);
            if(type_appointment_concerngroup == APPOINTMENT){
                let appointment_list = $("#appointmentList_officewise").val();
                appointment_concern_action_heading.innerHTML = "Appointment (Action)";
                if(!appointment_list){
                    return true;
                }
                let parse_appointment_list = JSON.parse(appointment_list);
                option+="<option value=''>Select Appointment</option>";
                for (let property in parse_appointment_list) {
                    option += "<option value='"+ property +"'>"+ parse_appointment_list[property] +"</option>";
                }

            }
            if(type_appointment_concerngroup == CONCERN_GROUP){
                let concern_group_list = $("#concerGroupList_officewise").val();
                appointment_concern_action_heading.innerHTML = "Concern Group (Action)";
                if(!concern_group_list){
                    return true;
                }
                let parse_concern_group = JSON.parse(concern_group_list);
                option+="<option value=''>Select Concern Group</option>";
                for (let property in parse_concern_group) {
                    option += "<option value='"+ property +"'>"+ parse_concern_group[property] +"</option>";
                }
            }
            appointment_concern_action_data.innerHTML = option;
        }
    });
 });
</script>
@endpush

@push('css')
<style>
    .action-badge {
        font-size: 14px;
        font-weight: bold
    }

    .display_none_class {
        display: none
    }
</style>
@endpush