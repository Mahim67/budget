<a href="{{ $url }}" id="{{ $id }}" class="{{ $class }} btn btn-outline bg-danger btn-icon text-danger btn-sm border-danger border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title="{{ $tooltip }}" data-original-title="{{ $tooltip }}">
    <i class="fas fa-{{ $icon }}"></i>
    {{ $title }}
</a>