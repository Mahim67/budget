<?php

namespace Pondit\Baf\MasterData\Models;

use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\Budget\Models\Budgetcode;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pondit\Baf\Budget\Models\RegisterBook;

class Range extends Model
{
    use SoftDeletes, RecordSequenceable;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'ranges';

    protected $fillable = ['id'
                            ,'uuid'
                            ,'title'
                            ,'sequence_number'
                            ,'description'
                        ];
    /*
    **  RELATIONSHIPS
    */
    public function budgetCode(){
        return $this->belongsToMany(Budgetcode::class,'budgetcodes_ranges','range_id','budgetcode_id');
    }

    public function registerBook(){
        return $this->belongsToMany(RegisterBook::class,'ranges_register_bookks','register_book_id', 'range_id');
    }

}
