<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WorkflowPath extends Model
{
    use HasFactory;

    protected $table    = 'WORKFLOW_PATHS';
    protected $fillable = ['id',
                            'code',
                            'if',
                            'than',
                        ];

}
