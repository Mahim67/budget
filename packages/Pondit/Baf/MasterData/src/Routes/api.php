<?php

use Illuminate\Support\Facades\Route;

use Pondit\Baf\MasterData\Http\Controllers\Api\RangeApiController;
use Pondit\Baf\MasterData\Http\Controllers\Api\OfficeApiController;
use Pondit\Baf\MasterData\Http\Controllers\Api\WorkflowPathApiController;
use Pondit\Baf\MasterData\Http\Controllers\Api\FinancialYearApiController;
use Pondit\Baf\MasterData\Http\Controllers\Api\WorkflowTemplateApiController;



Route::group(['namespace'=>'Api','prefix' => 'api/masterdata', 'middleware' => ['web'] ], function () {
    Route::group([ 'prefix' => 'financial-years'], function () {
        Route::get('/', [FinancialYearApiController::class, 'index']);
        Route::get('/show/{id}', [FinancialYearApiController::class, 'show']);
        Route::post('/store', [FinancialYearApiController::class, 'store']);
        Route::post('/update/{id}', [FinancialYearApiController::class, 'update']);
        Route::post('/{id}', [FinancialYearApiController::class, 'destroy']);
    });
    
    Route::group(['prefix' => 'ranges', 'as' => 'range.'], function () {
        Route::get('/range', [RangeApiController::class, 'range']);
        Route::get('/', [RangeApiController::class, 'index']);
        Route::post('/store', [RangeApiController::class, 'store']);
        Route::get('/show/{id}', [RangeApiController::class, 'show']);
        Route::put('/update/{id}', [RangeApiController::class, 'update']);
        Route::post('/delete/{id}', [RangeApiController::class, 'destroy']);
    });
    Route::group([ 'prefix' => 'workflow-templates'], function () {
        Route::get('/', [WorkflowTemplateApiController::class, 'index']);
        Route::get('/show/{id}', [WorkflowTemplateApiController::class, 'show']);
        Route::post('/store', [WorkflowTemplateApiController::class, 'store']);
        Route::post('/update/{id}', [WorkflowTemplateApiController::class, 'update']);
        Route::post('/{id}', [WorkflowTemplateApiController::class, 'destroy']);
        // soft deletes
        Route::get ('/trash', [WorkflowTemplateApiController::class, 'trash']);
        Route::get ('/{id}/restore', [WorkflowTemplateApiController::class, 'restore']);
        Route::get ('/perdelete/{id}', [WorkflowTemplateApiController::class, 'permanentDelete']);
    });

    Route::group(['prefix' => 'workflow-paths'], function () {
        Route::get('/', [WorkflowPathApiController::class, 'index']);
        Route::post('/store', [WorkflowPathApiController::class, 'store']);
        Route::get('/show/{id}', [WorkflowPathApiController::class, 'show']);
        Route::put('/update/{id}', [WorkflowPathApiController::class, 'update']);
        Route::post('/delete/{id}', [WorkflowPathApiController::class, 'destroy']);
    });
    
});
