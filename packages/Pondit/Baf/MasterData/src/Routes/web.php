<?php

use Illuminate\Support\Facades\Route;

use Pondit\Baf\MasterData\Http\Controllers\RangeController;
use Pondit\Baf\MasterData\Http\Controllers\TradeController;
use Pondit\Baf\MasterData\Http\Controllers\OfficeController;
use Pondit\Baf\MasterData\Http\Controllers\TosRefController;
use Pondit\Baf\MasterData\Http\Controllers\AircraftController;
use Pondit\Baf\MasterData\Http\Controllers\SubRangeController;
use Pondit\Baf\MasterData\Http\Controllers\EqptClassController;
use Pondit\Baf\MasterData\Http\Controllers\WorkflowPathController;
use Pondit\Baf\MasterData\Http\Controllers\EquipmentTypeController;
use Pondit\Baf\MasterData\Http\Controllers\FinancialYearController;
use Pondit\Baf\MasterData\Http\Controllers\SecurityLevelController;
use Pondit\Baf\MasterData\Http\Controllers\StoreController;
use Pondit\Baf\MasterData\Http\Controllers\WorkflowTemplateController;



Route::group(['prefix' => 'masterdata', 'middleware' => ['web'] ], function () {

    Route::group([ 'prefix' => 'financial-years', 'as' => 'financial_years.'], function () {
        Route::get('/', [FinancialYearController::class, 'index'])->name('index');
        Route::get('/show/{id}', [FinancialYearController::class, 'show'])->name('show');
        Route::get('/create', [FinancialYearController::class, 'create'])->name('create');
        Route::get('/{id}/edit', [FinancialYearController::class, 'edit'])->name('edit');
        Route::post('/store', [FinancialYearController::class, 'store'])->name('store');
        Route::post('/update/{id}', [FinancialYearController::class, 'update'])->name('update');
        Route::post('/{id}', [FinancialYearController::class, 'destroy'])->name('destroy');
        // soft deletes
        Route::get ('/trash', [FinancialYearController::class, 'trash'])->name ('trash');
        Route::get ('/{id}/restore', [FinancialYearController::class, 'restore'])->name ('restore');
        Route::get ('/perdelete/{id}', [FinancialYearController::class, 'permanentDelete'])->name ('permanentDelete');
    });


    Route::group(['prefix' => 'ranges', 'as' => 'range.','middleware' => ['web'] ], function () {
        Route::get('/', [RangeController::class, 'index'])->name('index');
        Route::get('/create', [RangeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [RangeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [RangeController::class, 'edit'])->name('edit');
        Route::post('/', [RangeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [RangeController::class, 'update'])->name('update');
        Route::post('/{id}', [RangeController::class, 'destroy'])->name('delete');
    });


    Route::group([ 'prefix' => 'workflow-templates', 'as' => 'workflow_template.'], function () {
        Route::get('/', [WorkflowTemplateController::class, 'index'])->name('index');
        Route::get('/preview/{id}', [WorkflowTemplateController::class, 'preview'])->name('preview');
        Route::get('/show/{id}', [WorkflowTemplateController::class, 'show'])->name('show');
        Route::get('/create', [WorkflowTemplateController::class, 'create'])->name('create');
        Route::get('/{id}/edit', [WorkflowTemplateController::class, 'edit'])->name('edit');
        Route::post('/store', [WorkflowTemplateController::class, 'store'])->name('store');
        Route::post('/update/{id}', [WorkflowTemplateController::class, 'update'])->name('update');
        Route::post('/{id}', [WorkflowTemplateController::class, 'destroy'])->name('destroy');
        // soft deletes
        Route::get ('/trash', [WorkflowTemplateController::class, 'trash'])->name ('trash');
        Route::get ('/{id}/restore', [WorkflowTemplateController::class, 'restore'])->name ('restore');
        Route::get ('/perdelete/{id}', [WorkflowTemplateController::class, 'permanentDelete'])->name ('permanentDelete');
    });

    Route::group(['prefix' => 'workflow-paths', 'as' => 'workflow_path.'], function () {
        Route::get('/', [WorkflowPathController::class, 'index'])->name('index');
        Route::get('/create', [WorkflowPathController::class, 'create'])->name('create');
        Route::get('/show/{id}', [WorkflowPathController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [WorkflowPathController::class, 'edit'])->name('edit');
        Route::post('/store', [WorkflowPathController::class, 'store'])->name('store');
        Route::put('/update/{id}', [WorkflowPathController::class, 'update'])->name('update');
        Route::post('/{id}', [WorkflowPathController::class, 'destroy'])->name('destroy');
    });
    Route::group([ 'prefix' => 'security-levels', 'as' => 'security_level.'], function () {

        Route::get('/', [SecurityLevelController::class, 'index'])->name('index');
        Route::get('/show/{id}', [SecurityLevelController::class, 'show'])->name('show');
        Route::get('/create', [SecurityLevelController::class, 'create'])->name('create');
        Route::get('/{id}/edit', [SecurityLevelController::class, 'edit'])->name('edit');
        Route::post('/store', [SecurityLevelController::class, 'store'])->name('store');
        Route::post('/update/{id}', [SecurityLevelController::class, 'update'])->name('update');
        Route::post('/{id}', [SecurityLevelController::class, 'destroy'])->name('destroy');
        // soft deletes
        Route::get ('/trash', [SecurityLevelController::class, 'trash'])->name ('trash');
        Route::get ('/{id}/restore', [SecurityLevelController::class, 'restore'])->name ('restore');
        Route::get ('/perdelete/{id}', [SecurityLevelController::class, 'permanentDelete'])->name ('permanentDelete');

    });

});
