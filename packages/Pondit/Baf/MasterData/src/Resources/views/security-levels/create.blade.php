@extends('pondit-limitless::layouts.master')

@push('css')

@endpush

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top mb-2 header-elements-inline bg-success">
        <h5 class="card-title">
            Security Gradings
        </h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        {!! Form::open([
        'route'=>['security_level.store'],
        'method' => 'POST',
        'enctype' => 'multipart/form-data'
        ]) !!}
        <div class='form-group row'>
            {!! Form::label('title', 'Title', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class='col-sm-10'>
                {!! Form::text('title', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('description', 'Description', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class='col-sm-10'>
                {!! Form::text('description', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('is_status_approved', 'Status', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class='col-sm-10 mt-2'>
                {!! Form::radio('is_status_approved', '1', true) !!}
                {!! Form::label('is_status_approved', 'Active') !!}
                {!! Form::radio('is_status_approved', '0', false) !!}
                {!! Form::label('is_status_approved', 'Inactive') !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('picture', 'Picture', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class='col-sm-10'>
                {!! Form::file('picture', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='text-right mt-3'>
            <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
                Save</button>
            <a href="{{ route('security_level.index') }}" class="btn btn-sm bg-danger float-right"> <i
                    class="fas fa-times"></i>
                Cancel</a>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted"></div>
        <div class="text-muted">
            <a href="{{ route('security_level.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple"
                data-popup="tooltip" title="Security Levels List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
        </div>
        <span></span>
    </div>
</div>
@endsection



@push('js')

<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
<!-- Theme JS files -->
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/uploaders/dropzone.min.js"></script>
{{-- <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script> --}}
{{-- <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/uploader_dropzone.js">
</script> --}}

<!-- /theme JS files -->


@endpush