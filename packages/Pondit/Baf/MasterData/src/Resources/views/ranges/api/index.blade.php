@extends('pondit-limitless::layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/ui.jqgrid-bootstrap4.css">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/fontawesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/main.css">
@endpush
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="Budget Codes">
    {{ csrf_token() }}
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
    <span class="oi oi-person"></span>
    <div id="userForm" class="modal fade" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Budget Codes</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <form id="form">
                        <meta name="csrf-token" content="{{ csrf_token() }}" />
                        {{ csrf_token() }}
                        <input type="hidden" name="id" id="id">
                        {{-- <div class="form-group">
                            <label for="introduced_fin_year">Financial Years</label>
                            <input name="introduced_fin_year" type="text" placeholder="Fin Year" id="introduced_fin_year" class="form-control">
                        </div> --}}
                        <x-pondit-fin-year label="Financial Year" name="introduced_fin_year" />
                        <div class="form-group">
                            <label for="sequence_number">Sequence Number</label>
                            <input name="sequence_number" type="number" placeholder="Sequence Number"
                                id="sequence_number" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label for="new_code">New Code</label>
                            <input name="new_code" type="text" placeholder="New Code" id="new_code" class="form-control"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="old_code">Old Code</label>
                            <input name="old_code" type="text" placeholder="Old Code" id="old_code"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="activity_type">Activity Type</label>
                            <select name="activity_type" id="activity_type" placeholder="Select Activity Type"
                                class="form-control select">
                                <option value="general">General</option>
                                <option value="special">Special</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="budget_head">Budget Head</label>
                            <input name="budget_head" type="text" placeholder="Budget Head" id="budget_head"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input name="description" type="text" placeholder="Description" id="description"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            <button type="button" id="form-reset" class="btn btn-info btn-sm float-left"><i
                                    class="fa fa-sync"></i> CLEAR</button>
                            <button type="button" id="form-submit" class="btn btn-success btn-sm float-right"><i
                                    class="fa fa-save"></i> SAVE</button>
                        </div>

                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="userDetails" class="modal fade show" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-success">
                    <h5 class="modal-title text-white">Budget Codes</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <table class="table table-striped table-bordered" id="user-details-table"></table>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
</x-pondit-card>







<!-- /basic example -->

@endsection

@push('js')

{{-- <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script> --}}

<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/i18n/grid.locale-en.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/context-menu.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/jquery.jqGrid.min.js"></script>

<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/main.js"></script>
@endpush