@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Ranges">
<x-pondit-form action="{{route('range.store')}}">
       
        <div class='form-group row'>
            <label name="sequence_number" class="col-sm-2 col-form-label">Sequence Number</label>
            <div class='col-sm-10'>
                <input type="number" name="sequence_number" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label name="title" class="col-sm-2 col-form-label">Title</label>
            <div class='col-sm-10'>
                <input type="text" name="title" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">Description</label>
            <div class='col-sm-10'>
                <input type="text" name="description" class="form-control">
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('range.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
{{-- @push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}