@extends('pondit-limitless::layouts.master')

@push('css')

@endpush

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top mb-2 header-elements-inline bg-success">
        <h5 class="card-title">
            Financial Years
        </h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        {!! Form::open([
        'route'=>['financial_years.store'],
        'method' => 'POST',
        'enctype' => 'multipart/form-data'
        ]) !!}
        {{-- <x-pondit-fin-year class="select-search"/> --}}
        
        <div class='form-group row'>
            {!! Form::label('year_range', 'Year Range', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('year_range', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('from_year', 'From Year', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('from_year', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('to_year', 'To Year', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('to_year', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='text-right mt-3'>
            <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
                Save</button>
            <a href="{{ route('financial_years.index') }}" class="btn btn-sm bg-danger float-right"> <i
                    class="fas fa-times"></i> Cancel</a>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted"></div>
        <div class="text-muted">
            <a href="{{ route('financial_years.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple"
                data-popup="tooltip" title="Financial Years List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
        </div>
        <span></span>
    </div>
</div>
@endsection
@push('js')
<script>
     $(document).ready(function()
    {
        // $.ajax
        // ({
        //     type: "GET",
        //     url: "/api/masterdata/financial-years",
        //     // data:id,
        //     cache: false,
        //     success: function(response)
        //     {
        //         let data = response.data;
        //         var html;
        //         // console.log(data);
        //         data.forEach(datum => {
        //             html = `<option value="${datum.id}">${datum.year_range}</option>`;
        //             console.log(html);
        //         $("#fin_year").append(html);
        //         });
        //     }
        // });

        /* Fetch get data use axios */
        // axios.get('/api/masterdata/financial-years')
        // .then(function (response) {
        //     let data = response.data.data;
        //     data.forEach(datum => {
        //         html = `<option value="${datum.id}">${datum.year_range}</option>`;
        //         console.log(html);
        //     });
        // })
        // .catch(function (error) {
        //     console.log(error);
        // });
        
        
        /* Fetch get data use axios */
        fetch('/api/masterdata/financial-years')
        .then(response => response.json())
        .then(data => {
            let lists = data.data;
            lists.forEach(list => {
                let html = `<option value="${list.id}">${list.year_range}</option>`;
                $("#fin_year").append(html);
            });
        })
        .catch(function (error) {
            console.log(error);
        });
    });
</script>
@endpush