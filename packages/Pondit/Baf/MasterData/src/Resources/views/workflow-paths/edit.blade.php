@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Workflow Paths</h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {{ Form::model($data,[
            'route'=>['workflow_path.update',$data->id],
            'method' => 'PUT',
            'id'=>'mainForm'
        ]) }}

        <div class="form-group row">
            {!! Form::label('if', 'If', ['class'=>'col-sm-2 col-form-label']) !!}
            <div class="col-sm-10">
                {!! Form::textarea('if',null, ['class'=>'form-control', 'rows'=>'3']) !!}
            </div>
        </div>
        <div class="form-group row">
            {{ Form::label('than', 'Then  ', ['class' => 'col-md-2 col-form-label']) }}
            <div class="col-md-10">
                {!! Form::textarea('than',null, ['class'=>'form-control', 'rows'=>'3']) !!}
            </div>
        </div>
    
        <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
            Update</button>
        <a href="{{ route('workflow_path.index') }}" class="btn btn-sm bg-danger float-right">
            <i class="fas fa-times"></i>
            Cancel
        </a>
        {!! Form::close() !!}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted">

        </div>
        <div class="text-muted d-inline-flex">
            <a href="{{ route('workflow_path.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('workflow_path.show', $data->id) }}"
                class="btn btn-outline bg-success btn-icon text-success border-success border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Single Show" data-original-title="Top tooltip">
                <i class="fas fa-eye"></i>
            </a>
            <a href="{{ route('workflow_path.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Create" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
            <span>
                {!! Form::open([
                'route' => ['workflow_path.destroy', $data->id],
                'method' => 'POST'
                ]) !!}
                <button
                    class="btn btn-outline bg-danger btn-icon text-danger border-danger border-2 rounded-round legitRipple mr-1"
                    data-popup="tooltip" title="Remove" data-original-title="Top tooltip"
                    onclick="return confirm('Are You Sure!')">
                    <i class="fas fa-trash"></i>
                </button>
                {!! Form::close() !!}
            </span>
        </div>

        <span>

        </span>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('vendor/select2') }}/select2.min.js"></script>
@endpush