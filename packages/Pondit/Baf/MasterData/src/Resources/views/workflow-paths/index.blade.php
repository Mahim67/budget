@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Workflow Paths</h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <table class="table datatable-colvis-basic">
        <thead>
            <tr>
                <th>Ser No</th>
                <th> Code</th>
                <th> If</th>
                <th> Then </th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl = 1 ?>
            @foreach ($data as $item)
            <tr>
                <td>{{ $sl++ }}</td>
                <td>{{ $item->code }}</td>
                <td>{{$item->if }}</td>
                <td>{{$item->than }}</td>
                
                <td class="d-flex justify-content-center">
                    <span>
                        <a href="{{route('workflow_path.show', $item->id)}}"
                            class="btn bg-success btn-circle btn-circle-sm">
                            <i class=" fas fa-eye"></i>
                        </a>
                    </span>
                    <span>
                        <a href="{{route('workflow_path.edit', $item->id)}}"
                            class="btn bg-primary btn-circle btn-circle-sm">
                            <i class=" fas fa-pen"></i>
                        </a>
                    </span>

                     <span>
                        {!! Form::open([
                        'route' => ['workflow_path.destroy', $item->id],
                        'method' => 'POST'
                        ]) !!}
                        <button class="btn bg-danger btn-circle btn-circle-sm"
                            onclick="return confirm('Are You Sure!')"><i class=" fas fa-trash"></i></button>
                        {!! Form::close() !!}
                    </span>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted">  </div>
        <div class="text-muted">
            <a href="{{ route('workflow_path.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple" data-popup="tooltip" title="Create" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
             {{-- <a href="{{ route('workflow_path.trash') }}"
                class="btn btn-outline bg-danger btn-icon text-danger border-danger border-2 rounded-round legitRipple" data-popup="tooltip" title="Trash List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a> --}}
        </div>

        <span>

        </span>
    </div>
</div>
<!-- /basic example -->
@endsection
