@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Workflow Paths</h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {{ Form::open([
            'route'=>['workflow_path.store'],
            'method' => 'POST',
            'id'=>'mainForm'
        ]) }}
        <div class="form-group row">
            {!! Form::label('if', 'If', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class="col-sm-10">
                {!! Form::textarea('if',null, ['class'=>'form-control', 'rows'=>'3']) !!}
            </div>
        </div>
        <div class="form-group row">
            {{ Form::label('than', 'Then', ['class' => 'col-md-2 col-form-label']) }}
            <div class="col-md-10">
                {!! Form::textarea('than',null, ['class'=>'form-control', 'rows'=>'3']) !!}
            </div>
        </div>


    <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
        Save</button>
    <a href="{{ route('workflow_path.index') }}" class="btn btn-sm bg-danger float-right">
        <i class="fas fa-times"></i>
        Cancel
    </a>
    {!! Form::close() !!}
</div>
<div class="card-footer d-flex justify-content-between align-items-center ">
    <div class="text-muted"></div>
    <div class="text-muted">
        <a href="{{ route('workflow_path.index') }}"
            class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple"
            data-popup="tooltip" title=" List" data-original-title="Top tooltip">
            <i class="fas fa-list"></i>
        </a>
    </div>
    <span></span>
</div>
</div>

@endsection

@push('js')

<script src="{{ asset('vendor/select2') }}/select2.min.js"></script>
<script>
    function getOfficeId() {
        var officeId = document.getElementById('officeInput').value;
        axios.get("{{url('/')}}/workflow-paths/getData/"+ officeId)
        .then(function (response) {
            let data = response.data;
            console.log(data)

            let acknowledgements = document.getElementById("ackEntity");
             let ack = (response.data.appointmentId);
             acknowledgements.innerHTML="";
             document.getElementById("appointment_concern_action_data").innerHTML = "";
            //  if(document.querySelector('input[type=radio][name=entity_type]:checked')){
            //     document.querySelector('input[type=radio][name=entity_type]:checked').checked = false;
            //  }
             var options = "";
              options += "<option value=''>Select Appointment</option>";
            for (let property_ack in ack) {
            options += "<option value='"+ property_ack +"'>"+ ack[property_ack] +"</option>";
            }
            acknowledgements.innerHTML = options
            document.getElementById("appointmentList_officewise").value=JSON.stringify(response.data.appointmentId)
            document.getElementById("concerGroupList_officewise").value=JSON.stringify(response.data.concernGroupsId)

        })
        .catch(function (error) {
            console.log(error);
        })
       
    }
    
        $(function(){
    $('input[type="radio"]').click(function(){
        if ($(this).is(':checked'))
        {
            let type_appointment_concerngroup = $(this).val();

            // console.log(type_appointment_concerngroup)

            let appointment_concern_action_data = document.getElementById("appointment_concern_action_data");
            let appointment_concern_action_heading = document.getElementById("appointment_concern_action_heading");
            appointment_concern_action_data.innerHTML="";
            let option = "";

            console.log(appointment_concern_action_heading);

            if(type_appointment_concerngroup == "Appointment"){
                let appointment_list = $("#appointmentList_officewise").val();
                appointment_concern_action_heading.innerHTML = "Appointment (Action)";
                if(!appointment_list){
                    return true;
                }
                let parse_appointment_list = JSON.parse(appointment_list);
                option+="<option value=''>Select Appointment</option>";
                for (let property in parse_appointment_list) {
                    option += "<option value='"+ property +"'>"+ parse_appointment_list[property] +"</option>";
                }

            }
            if(type_appointment_concerngroup == "Concern Group"){

                let concern_group_list = $("#concerGroupList_officewise").val();
                appointment_concern_action_heading.innerHTML = "Concern Group (Action)";
                if(!concern_group_list){
                    return true;
                }
                let parse_concern_group = JSON.parse(concern_group_list);
                option+="<option value=''>Select Concern Group</option>";
                for (let property in parse_concern_group) {
                    option += "<option value='"+ property +"'>"+ parse_concern_group[property] +"</option>";
                }
            }
            appointment_concern_action_data.innerHTML = option;
            
        }
    });
 });
    
</script>
@endpush