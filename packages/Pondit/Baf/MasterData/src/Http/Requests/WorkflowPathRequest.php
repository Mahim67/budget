<?php

namespace Pondit\Baf\MasterData\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkflowPathRequest extends FormRequest
{
   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            // 'year_range' => ['required', new FinancialYearRule],
            // 'name' => ['required'],
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'name.required' => 'Office name is required',
        ];
                    
}
}
