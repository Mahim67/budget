<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Image;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\WorkflowTemplate;
use Pondit\Baf\MasterData\Http\Controllers\MasterDataController;
use Pondit\Baf\MasterData\Http\Requests\WorkflowTemplateRequest;

class WorkflowTemplateController extends MasterDataController
{
    public function index()
    {
        $data  =  WorkflowTemplate::all();

        return view('masterdata::workflow-templates.index', compact('data'));
    }

    public function create()
    {
        return view('masterdata::workflow-templates.create');
    }

    public function store(WorkflowTemplateRequest $request)
    {
        // dd($request->all());
        try
        {
            $validated = $request->validated();

            $data = $request->all();
            if($request->dynamic_values != null){ 
                $data['dynamic_values'] = json_encode($request->dynamic_values);
                }else {
                    $data['dynamic_values'] = null;
                }
            if($request->dynamic_values != null){ 
                $data['copy_type'] = json_encode($request->copy_type);
                }else {
                    $data['copy_type'] = null;
                }

            $data['htmlized_template'] = $request->htmlized_template;


            if ($request->hasFile('template_path')) {
                $data['template_path'] = $this->uploadFile($request->template_path, $request->title);
            }

            WorkflowTemplate::create($data);
            // dd($data);
            return redirect()
                        ->route('workflow_template.index')
                        ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('workflow_template.create')
                        ->withErrors($th->getMessage());
        }
    }

    public function preview($id)
    {
        $data  =  WorkflowTemplate::findOrFail($id);
        $htmlized = $data->htmlized_template;
        
        return view('masterdata::workflow-templates.preview', compact('htmlized'));
    }

    public function show($id)
    {
        $data  =  WorkflowTemplate::findOrFail($id);
        return view('masterdata::workflow-templates.show', compact('data'));
    }

    public function edit($id)
    {
        $data  =   WorkflowTemplate::findOrFail($id);
        // dd($data);
        return view('masterdata::workflow-templates.edit', compact('data'));
    }

    public function update(WorkflowTemplateRequest $request, $id)
    {
        // dd($request->all());
        try
        {
            $validated  = $request->validated();

            $data = WorkflowTemplate::findOrFail($id);
            $data->title = $request->title;
            $data->number = $request->number;
            $data->purpose = $request->purpose;
            $data->htmlized_template = $request->htmlized_template;
            if($request->dynamic_values != null){ 
                $data['dynamic_values'] = json_encode($request->dynamic_values);
                }else {
                    $data['dynamic_values'] = null;
            }
            if($request->dynamic_values != null){ 
                $data['copy_type'] = json_encode($request->copy_type);
                }else {
                    $data['copy_type'] = null;
            }
            $data->number_of_copies = $request->number_of_copies;
            $data->from_storage_table = $request->from_storage_table;
            $data->is_storage_exists = $request->is_storage_exists;
            $data->created_by = $request->created_by;
            $data->updated_by = $request->updated_by;
            $data->deleted_by = $request->deleted_by;

            if ($request->hasFile('template_path')) {
                $this->unlink($data->template_path);
                $data['template_path'] = $this->uploadFile($request->template_path, $request->title);
            }

            $data->save();

            return redirect()
                ->route('workflow_template.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('workflow_template.edit', $id)
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = WorkflowTemplate::find($id);

        $data->delete();

        return redirect()
                    ->route('workflow_template.index')
                    ->withMessage('Entity has been deleted successfully!');
    }

    public function trash(){
        $data = WorkflowTemplate::onlyTrashed()->orderBy('created_at', 'desc')->get();
        
    	return view('masterdata::workflow-templates.trash', compact('data'));
    }

    public function restore($id){
    	WorkflowTemplate::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();

    	return redirect()->route('workflow_template.trash')
       	                 ->withMessage('Entity has been Restore Successfully');
    }

    public function permanentDelete($id){
       
        $data = WorkflowTemplate::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();

        return redirect()->route('workflow_template.trash')
       	                 ->withMessage('Entity has been Permanent Deleted Successfully');
        //    return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

    private function uploadFile($file, $name)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp .'-'.$name. '-' .$file->getClientOriginalExtension();
        $pathToUpload = $file->move( (storage_path().'/app/public/workflow-templates/'), $file_name);
        // Image::save($pathToUpload.$file_name);
        // Image::make($file_name)->save($pathToUpload.$file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/workflow-templates/';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }

}
