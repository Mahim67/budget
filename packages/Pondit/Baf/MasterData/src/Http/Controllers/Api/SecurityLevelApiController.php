<?php

namespace Pondit\Baf\MasterData\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\SecurityLevel;
use Pondit\Baf\MasterData\Http\Requests\SecurityLevelRequest;

class SecurityLevelApiController extends Controller
{
    private $res;

    private function prepareResponse($success, $message)
    {
        $this->res['success']   =   $success;
        $this->res['message']   =   $message;

        return $this->res;
    }

    public function index()
    {
        $data = SecurityLevel::orderBy('id', 'DESC')->get();

        return response()->json($data);
    }

    public function store(SecurityLevelRequest $request)
    {
        try
        {
            $validated = $request->validated();

            $data = SecurityLevel::create($request->all());

            $this->prepareResponse(true, 'Entity has been created successfully!');

            $this->res['data'] = $data;

            return response()->json($this->res);
        }
        catch (QueryException $th)
        {
            return response()->json($this->prepareResponse(false, $th->getMessage()));
        }
    }

    public function show($id)
    {
        $data = SecurityLevel::findOrFail($id);

        return response()->json($data);
    }

    public function edit($id)
    {
        $data = SecurityLevel::findOrFail($id);

        return response()->json($data);
    }

    public function update(SecurityLevelRequest $request, $id)
    {
        try
        {
            $validated = $request->validated();

            $data = SecurityLevel::findOrFail($id);

            // Field updating goes here
            $data->title = $request->title;
            $data->description = $request->description;
            $data->is_status_approved = $request->is_status_approved;

            if ($request->hasFile('picture')) {
                $this->unlink($data->picture);
                $data['picture'] = $this->uploadPicture($request->picture, $request->title);
            }
            
            $data->deleted_by          = $request->deleted_by;
            $data->updated_by          = $request->updated_by;
            $data->created_by          = $request->created_by;

            $data->save();

            $this->prepareResponse(true, 'Entity has been updated successfully!');

            $this->res['data'] = $data;

            return response()->json($this->res);
        }
        catch (QueryException $th)
        {
            return response()->json($this->prepareResponse(false, $th->getMessage()));
        }
    }

    public function destroy($id)
    {
        $data = SecurityLevel::findOrFail($id);

        $data->delete();

        return response()->json($this->prepareResponse(true, 'Entity has been deleted successfully!'));
    }

    public function trash(){
        $data = SecurityLevel::onlyTrashed()->orderBy('created_at', 'desc')->get();
        
    	return response()->json($data);
    }

    public function restore($id){
    	SecurityLevel::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();
        return response()->json($this->prepareResponse(true, 'Entity has been restored successfully!'));
                            
    }

    public function permanentDelete($id){
       
        $data = SecurityLevel::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

}
