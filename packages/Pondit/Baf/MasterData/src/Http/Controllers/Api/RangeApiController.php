<?php

namespace Pondit\Baf\MasterData\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\Range;

class RangeApiController extends Controller
{
    public function range ()
    {
        return view('masterdata::ranges.api.index');
    }

    public function index()
    {
        $data = Range::all();
        
        return response()->json($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        try
        {
            $sequenceNumber = 1;
            $sequence = Range::orderBy('id', 'desc')->first();

            $sequenceNumber  = $sequence ? $sequence->sequence_number+1 : $sequenceNumber;
            // dd($sequenceNumber);        

            $data  = new Range();
            $data->sequence_number      = $sequenceNumber;
            $data->title          = $request->title;
            $data->description          = $request->description;
            // dd($data);
            $data->save();
            return response()->json($data);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
    }

    public function show($id)
    {
        $data = Range::findOrFail($id);

        return response()->json($data);
    }

    public function edit($id)
    {
        $data = Range::findOrFail($id);

        return response()->json($data);
    }

    public function update(Request $request, Range $Range, $id)
    {
        try{
            // dd($request->all());
            $data = Range::where('id', $id)->first();
            // dd($data);
            // Field updating goes here
            
            $data->title          = $request->title;
            $data->description          = $request->description;
            // dd($data);
            $data->reArrangeSequence($request->sequence_number);

            $data->save();

            return response()->json($data);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
       
    }

    public function destroy($id)
    {
       try {
            $data = Range::findOrFail($id);

            $data->delete();
            return response()->json($data);

       } catch (QueryException $th) {
           return response()->json($th->getMessage());
       }

    }

    public function trash(){
        $data = Range::onlyTrashed()->orderBy('created_at', 'desc')->get();
        
    	return response()->json($data);
    }

    public function restore($id){
    	Range::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();
        return response()->json($this->prepareResponse(true, 'Entity has been restored successfully!'));
                            
    }

    public function permanentDelete($id){
       
        $data = Range::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

}
