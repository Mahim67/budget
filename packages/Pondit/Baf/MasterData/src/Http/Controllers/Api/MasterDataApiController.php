<?php

namespace Pondit\Baf\MasterData\Http\Controllers\Api;

use Pondit\Baf\MasterData\Models\MasterData;
use Pondit\Baf\MasterData\Http\Requests\MasterDataRequest;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class MasterDataApiController extends Controller
{
    private $res;

    private function prepareResponse($success, $message)
    {
        $this->res['success']   =   $success;
        $this->res['message']   =   $message;

        return $this->res;
    }

    public function index()
    {
        $masterdatas  =   MasterData::orderBy('id', 'DESC')->get();

        return response()->json($masterdatas);
    }

    public function store(MasterDataRequest $request)
    {
        try
        {
            $validated = $request->validated();

            $masterdata    =   MasterData::create($request->all());

            $this->prepareResponse(true, 'Entity has been created successfully!');

            $this->res['data'] = $masterdata;

            return response()->json($this->res);
        }
        catch (QueryException $th)
        {
            return response()->json($this->prepareResponse(false, $th->getMessage()));
        }
    }

    public function show($id)
    {
        $masterdata = MasterData::find($id);

        return response()->json($masterdata);
    }

    public function edit($id)
    {
        $masterdata = MasterData::find($id);

        return response()->json($masterdata);
    }

    public function update(MasterDataRequest $request, $id)
    {
        try
        {
            $validated = $request->validated();

            $masterdata = MasterData::findOrFail($id);

            // Field updating goes here

            $masterdata->save();

            $this->prepareResponse(true, 'Entity has been updated successfully!');

            $this->res['data'] = $masterdata;

            return response()->json($this->res);
        }
        catch (QueryException $th)
        {
            return response()->json($this->prepareResponse(false, $th->getMessage()));
        }
    }

    public function destroy($id)
    {
        $masterdata = MasterData::find($id);

        $masterdata->delete();

        return response()->json($this->prepareResponse(true, 'Entity has been deleted successfully!'));
    }
}
