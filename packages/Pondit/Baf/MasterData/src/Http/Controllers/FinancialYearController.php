<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Image;
use Exception;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\FinancialYear;
use Pondit\Baf\MasterData\Http\Requests\FinancialYearRequest;
use Pondit\Baf\MasterData\Http\Controllers\MasterDataController;

class FinancialYearController extends MasterDataController
{
    public function index()
    {
        $data  =  FinancialYear::orderBy('year_range', 'DESC')->get();
        return view('masterdata::financial-years.index', compact('data'));
    }

    public function create()
    {
        return view('masterdata::financial-years.create');
    }

    public function store(FinancialYearRequest $request)
    {
        try
        {
            $validated = $request->validated();
                
            $data = $request->all();
            
            $data = FinancialYear::create($data);
            if(!$data)
                throw new Exception("Could not be saved", 1);
                
            return redirect()
                        ->route('financial_years.index')
                        ->withMessage('Financial Year created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('financial_years.create')
                        ->withErrors($th->getMessage());
        }
    }

    public function show($id)
    {
        $data  =  FinancialYear::findOrFail($id);
        

        return view('masterdata::financial-years.show', compact('data'));
    }

    public function edit($id)
    {
        $data  =   FinancialYear::findOrFail($id);

        return view('masterdata::financial-years.edit', compact('data'));
    }

    public function update(FinancialYearRequest $request, $id)
    {
        try
        {
            $validated  = $request->validated();

            $data = FinancialYear::findOrFail($id);
            // Field updating goes here
            $data->year_range   = $request->year_range;
            $data->from_year    = $request->from_year;
            $data->to_year      = $request->to_year;

            $data->save();
            if(!$data)
                throw new Exception("Could not be updated", 1);

            return redirect()
                ->route('financial_years.index')
                ->withMessage('Financial Year has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('financial_years.edit', $id)
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $data = FinancialYear::find($id);
            $data->delete();
            return redirect()
                        ->route('financial_years.index')
                        ->withMessage('Entity has been deleted successfully!');
        } catch (QueryException $th) {
            return redirect()->back()->withErrors($th->getMessage());
        }
        
    }

    public function trash(){
        $data = FinancialYear::onlyTrashed()->orderBy('created_at', 'desc')->get();
        // dd($supplier);
    	return view('masterdata::financial-years.trash', compact('data'));
    }

    public function restore($id){
    	FinancialYear::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();

    	return redirect()->route('financial_years.trash')
       	                 ->withMessage('Entity has been Restore Successfully');
    }

    public function permanentDelete($id){
       
        $data = FinancialYear::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

}
