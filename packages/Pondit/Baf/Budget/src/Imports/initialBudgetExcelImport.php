<?php

namespace Pondit\Baf\Budget\Imports;

use Exception;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\ToModel;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class initialBudgetExcelImport implements ToModel, WithHeadingRow, WithValidation {
    use Importable;

    public $columns, $model, $validationInfo;
    public $whiteListedExcel = null;
    public $hiddenColumn = null;

    public function __construct($model, array $columns, array $validationInfo, $whiteListedExcel = null, $hiddenColumn = null ) {
        
        $this->model                        = $model;
        $this->whiteListedExcel             = $whiteListedExcel;
        $this->columns                      = $columns;
        $this->validationInfo               = $validationInfo;
        $this->hiddenColumn                 = $hiddenColumn;
    }
    public $count = 1 ;
    public function model(array $row) {
            $parseRow   = [];
            $newRows    = [];
            
            if (is_array($row)) {
                if (isset($this->whiteListedExcel)) {
                    foreach ($row as $key => $value) {
                        if (isset($this->whiteListedExcel[$key])) {
                            $parseRow[$this->whiteListedExcel[$key]] = $value;
                        }
                        else{
                            $parseRow[$key] = $value ;
                        }
                    }
                    if(isset($row['old_code']) ||  isset($row['new_code'])){

                        $getBudgetCode = Budgetcode::where('oldcode',(string)$row['old_code'])->where('newcode',(string)$row['new_code'])->first();
                        if(is_null($getBudgetCode)){
                            $getBudgetCode = Budgetcode::where('newcode',$row['new_code'])->first();
                        } 
                    }
                    $erros = null;
                    if (!$getBudgetCode){
                       
                        $erros = [$row['new_code']     => 'BudgetCode '.$row['new_code'].' doesnot exist'] ;
                        $getSeccion = session('excelErrors');
                        $getSeccion ? $getSeccion : $getSeccion = [];
                        $message = array_merge($getSeccion, $erros);

                        session(['excelErrors' => $message]);
                    }
                        
                    $parseRow['budgetcode_id']  = $getBudgetCode->id ?? null ;

                    if(isset($row['range'])){
                        $rangeInfo = Range::where('title',$row['range'])->first();
                        if(!is_null($rangeInfo)){
                            $parseRow['range_name'] = $rangeInfo->title ;
                            $parseRow['range_id'] = $rangeInfo->id ;
                        }
                        else{
                            $parseRow['range_name'] = $row['range'] ;
                        }
                    }

                    $parseRow['balance']  = $row['initial_amount'];
                    $parseRow['total_budgetcode_amount']  = $row['initial_amount'];

                    $newRows = array_merge($parseRow, $row);

                }
                if (count($newRows) > 0) {
                    if (isset($this->hiddenColumn)) {
                        $newRows = array_merge($newRows, $this->hiddenColumn);
                    }
                    if (is_null($erros)) {
                        $this->storeData($this->model, $this->columns, $newRows);
                    }
                } else {
                    if (isset($this->hiddenColumn)) {
                        $row = array_merge($row, $this->hiddenColumn);
                    }
                    if (is_null($erros)) {
                        $this->storeData($this->model, $this->columns, $row);
                    }
                }
            }

        $this->count++;
    }

    public function rules(): array
    {
        return $this->validationInfo;
    }

    public function storeData($model, $columns, $arrayOfRow) {
        if (is_array($columns)) {
            $matchCol = array_intersect($columns, array_keys($arrayOfRow));
            $dataArray = [];
            foreach ($matchCol as $eachColumn) {
                array_push($dataArray, [
                    $eachColumn => isset($arrayOfRow[$eachColumn]) ? $arrayOfRow[$eachColumn] : null,
                ]);
            }
            $makeSingleArray = (collect($dataArray)->collapse())->toArray();
            $hasData = $model::where('budgetcode_id',$makeSingleArray['budgetcode_id'])->where('range_id',$makeSingleArray['range_id'])->first();
            if (isset($hasData)) {
                $model::where('id',$hasData->id)->update($makeSingleArray);
            }
            else{
                $model::create($makeSingleArray);
            }
        }
    }
}
