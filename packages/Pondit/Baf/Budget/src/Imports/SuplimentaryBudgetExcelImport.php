<?php

namespace Pondit\Baf\Budget\Imports;

use Exception;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\MasterData\Models\Range;

class SuplimentaryBudgetExcelImport  implements ToModel, WithHeadingRow, WithValidation {
    use Importable;

    public $columns, $model, $validationInfo;
    public $whiteListedExcel = null;
    public $hiddenColumn = null;

    public function __construct($model, array $columns, array $validationInfo, $whiteListedExcel = null, $hiddenColumn = null ) {
        
        $this->model                        = $model;
        $this->whiteListedExcel             = $whiteListedExcel;
        $this->columns                      = $columns;
        $this->validationInfo               = $validationInfo;
        $this->hiddenColumn                 = $hiddenColumn;
    }
    public $count = 1 ;
    public function model(array $row) {
            $parseRow   = [];
            $newRows    = [];
            
            if (is_array($row)) {

                if (isset($this->whiteListedExcel)) {
                    foreach ($row as $key => $value) {
                        if (isset($this->whiteListedExcel[$key])) {
                            $parseRow[$this->whiteListedExcel[$key]] = $value;
                        }
                        else{
                            $parseRow[$key] = $value ;
                        }
                    }

                    if(isset($row['old_code']) ||  isset($row['new_code'])){
                        
                        $getBudgetCode = Budgetcode::where('oldcode',(string)$row['old_code'])->where('newcode',(string)$row['new_code'])->first();
                        if(is_null($getBudgetCode)){
                            $getBudgetCode = Budgetcode::where('newcode',(string)$row['new_code'])->first();
                        } 
                    }
                    $erros = null ;
                    if (is_null($getBudgetCode)){
                        $erros = [$row['new_code']     =>$this->count . ' No row "' . $row['new_code'].'" This code not exit in BudgetCode'] ;
                        $getSeccion = session('excelErrors');
                        $getSeccion ? $getSeccion : $getSeccion = [];
                        $message = array_merge($getSeccion, $erros);
                        session(['excelErrors' => $message]);
                    }

                        
                    $parseRow['budgetcode_id']  = $getBudgetCode->id ?? null ;

                    if(isset($row['range'])){
                        $rangeInfo = Range::where('title',$row['range'])->first();
                        if(!is_null($rangeInfo)){
                            $parseRow['range_name'] = $rangeInfo->title ;
                            $parseRow['range_id'] = $rangeInfo->id ;
                        }
                        else{
                            $erros = [ $this->count . ' No row "' . 'Range Not Match'] ;
                            $getSeccion = session('excelErrors');
                            $getSeccion ? $getSeccion : $getSeccion = [];
                            $message = array_merge($getSeccion, $erros);
                            session(['excelErrors' => $message]);
                        }
                    }
                    else{
                        $erros = [ $this->count . ' No row "' . 'Range Not Found'] ;
                        $getSeccion = session('excelErrors');
                        $getSeccion ? $getSeccion : $getSeccion = [];
                        $message = array_merge($getSeccion, $erros);
                        session(['excelErrors' => $message]);
                    }



                    $newRows = array_merge($parseRow, $row);

                    $hasAllotmentData = BudgetAllotment::where('budgetcode_id',$parseRow['budgetcode_id'])->where('range_id',$parseRow['range_id'])->first();

                    if(is_null($hasAllotmentData)){
                        $erros = [ $this->count . ' No row "' . 'BudgetCode And Range Not Match'] ;
                        $getSeccion = session('excelErrors');
                        $getSeccion ? $getSeccion : $getSeccion = [];
                        $message = array_merge($getSeccion, $erros);
                        session(['excelErrors' => $message]);
                    }


                }
                if (count($newRows) > 0) {
                    if (isset($this->hiddenColumn)) {
                        $newRows = array_merge($newRows, $this->hiddenColumn);
                    }
                    if (is_null($erros)) {
                        $this->storeData($this->model, $this->columns, $newRows);
                    }
                } else {
                    if (isset($this->hiddenColumn)) {
                        $row = array_merge($row, $this->hiddenColumn);
                    }
                    if (is_null($erros)) {
                        $this->storeData($this->model, $this->columns, $row);
                    }
                }
            }

        $this->count++;
    }

    public function rules(): array
    {
        return $this->validationInfo;
    }

    public function storeData($model, $columns, $arrayOfRow) {
        if (is_array($columns)) {
            $matchCol = array_intersect($columns, array_keys($arrayOfRow));
            $dataArray = [];
            foreach ($matchCol as $eachColumn) {
                array_push($dataArray, [
                    $eachColumn => isset($arrayOfRow[$eachColumn]) ? $arrayOfRow[$eachColumn] : null,
                ]);
            }
            $makeSingleArray = (collect($dataArray)->collapse())->toArray();
            $hasAllotmentData = BudgetAllotment::where('budgetcode_id',$makeSingleArray['budgetcode_id'])->where('range_id',$makeSingleArray['range_id'])->first();

            
            
            
            $lastId = $model::orderBy('id','desc')->first();
            $lastId ? $insertId = $lastId->id : $insertId = 0 ;
            
            $id = $insertId + 1 ;
            $makeSingleArray['id'] = $id ;

            $createData = $model::create($makeSingleArray);

            $suplimentary_budget_detail = json_encode($createData);
            $total_no_suplimentary_budgets = $hasAllotmentData->total_no_suplimentary_budgets ? $hasAllotmentData->total_no_suplimentary_budgets+1 : 1 ;
            $total_suplimentary_amount  = $hasAllotmentData->total_suplimentary_amount != null ? $hasAllotmentData->total_suplimentary_amount + $makeSingleArray['suplimentary_amount'] : $makeSingleArray['suplimentary_amount'];
            $total_balance  = $hasAllotmentData->balance != null ? $hasAllotmentData->balance + $makeSingleArray['suplimentary_amount'] : $hasAllotmentData->initial_amount + $makeSingleArray['suplimentary_amount'];
            
            $prepareData = [
                'total_no_suplimentary_budgets'         => $total_no_suplimentary_budgets ,
                'suplimentary_budget_detail'            => $suplimentary_budget_detail,
                'total_suplimentary_amount'             => $total_suplimentary_amount,
                'total_budgetcode_amount'               => $hasAllotmentData->initial_amount + $total_suplimentary_amount,
                'balance'                               => $total_balance
            ];
          
            BudgetAllotment::where('budgetcode_id',$makeSingleArray['budgetcode_id'])->update($prepareData);

        }
    }
}
