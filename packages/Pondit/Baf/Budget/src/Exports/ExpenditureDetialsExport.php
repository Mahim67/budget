<?php

namespace Pondit\Baf\Budget\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Pondit\Baf\Budget\Models\ExpenditureDetials;
use Pondit\Baf\Budget\Models\ExpenditureDetials as ModelsExpenditureDetials;

class ExpenditureDetialsExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('budget::expenditure-detials.expenExcel', [
            'data' => ExpenditureDetials::all()
        ]);
        
    }
    
}
