<?php

namespace Pondit\Baf\Budget\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Pondit\Baf\Budget\Models\ExpenditureDetials;

class BalanceSheetExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('budget::expenditure-detials.balancedExcel', [
            'data' => ExpenditureDetials::all()
        ]);
        
    }
    
}
