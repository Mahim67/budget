<?php

namespace Pondit\Baf\Budget\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Pondit\Baf\Budget\Models\AllotmentBreakdown;
use Illuminate\Contracts\Auth\Access\Authorizable;

class AllotmentsExport implements FromView, ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('budget::allotment-brackdown.allotmentExcel', [
            'data' => AllotmentBreakdown::all()->groupBy('range_id')
        ]);
        
    }

    public function headings(): array
    {
        return [
            'ID',
            'New Code',
            'Old Code',
            'Description',
            'BSR Base',
            'MTR Base',
            'ZHR Base',
            'BBD Base',
            'PKB Base',
            'CXB Base',
            '201MU',
            '201U',
            'BRU',
            'MRU',
            'BALANCE'
        ];
    }
}
