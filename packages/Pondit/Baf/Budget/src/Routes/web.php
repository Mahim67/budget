<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\Budget\Http\Controllers\PDFController;
use Pondit\Baf\Budget\Http\Controllers\BudgetController;
use Pondit\Baf\Budget\Http\Controllers\BaseLevelBudgetTransferController;
use Pondit\Baf\Budget\Http\Controllers\BaseLetterController;
use Pondit\Baf\Budget\Http\Controllers\BudgetcodeController;
use Pondit\Baf\Budget\Http\Controllers\ExcelExportController;
use Pondit\Baf\Budget\Http\Controllers\RegisterBookController;
use Pondit\Baf\Budget\Http\Controllers\BudgetTransferController;
use Pondit\Baf\Budget\Http\Controllers\BaseExpenditureController;
use Pondit\Baf\Budget\Http\Controllers\BudgetAllotmentController;
use Pondit\Baf\Budget\Http\Controllers\BudgetCodeToItemController;
use Pondit\Baf\Budget\Http\Controllers\AllotmentBreakdownController;
use Pondit\Baf\Budget\Http\Controllers\ExpenditureDetialsController;
use Pondit\Baf\Budget\Http\Controllers\BudgetAllotmentBrackdownController;
use Pondit\Baf\Budget\Http\Controllers\BDemandAllotmentBreakdownController;
use Pondit\Baf\Budget\Http\Controllers\SuplimentaryBudgetAllotmentController;

Route::group(['prefix' => 'budgetcodes', 'as' => 'budgetcode.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BudgetcodeController::class, 'index'])->name('index');
        Route::get('/create', [BudgetcodeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BudgetcodeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetcodeController::class, 'edit'])->name('edit');
        Route::post('/', [BudgetcodeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BudgetcodeController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetcodeController::class, 'destroy'])->name('delete');

        Route::get('get-budgetcode', [BudgetcodeController::class, 'getBudgetcode'])->name('get_budgetcode');
        Route::get('get-range', [BudgetcodeController::class, 'getRange'])->name('get_range');
        Route::get('get-budgetcode-code', [BudgetcodeController::class, 'getBudgetcodeByCode'])->name('get_budgetcode_by_code');

    });

    Route::group(['prefix' => 'budget-transfers', 'as' => 'budget-transfer.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BudgetTransferController::class, 'index'])->name('index');
        Route::get('/create', [BudgetTransferController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BudgetTransferController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetTransferController::class, 'edit'])->name('edit');
        Route::post('/', [BudgetTransferController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BudgetTransferController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetTransferController::class, 'destroy'])->name('delete');
    });

    Route::group(['prefix' => 'budgets', 'as' => 'budget.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BudgetController::class, 'index'])->name('index');

        Route::get('initial-budget-create', [BudgetController::class, 'initialBudgetCreate'])->name('initial_budget_create');
        Route::post('initial-budget-store', [BudgetController::class, 'initialBudgetStore'])->name('initial_budget_store');

        Route::get('suplimentary-budget-create', [BudgetController::class, 'suplimentaryBudgetCreate'])->name('suplimentary_budget_create');
        Route::post('suplimentary-budget-store', [BudgetController::class, 'suplimentaryBudgetStore'])->name('suplimentary_budget_store');

        Route::get('/show/{id}', [BudgetController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetController::class, 'edit'])->name('edit');
        Route::put('/update/{id}', [BudgetController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetController::class, 'destroy'])->name('delete');
    });

    Route::group(['prefix' => 'suplimentary-budget-allotments', 'as' => 'suplimentary_budget_allotment.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [SuplimentaryBudgetAllotmentController::class, 'index'])->name('index');
        Route::get('/create', [SuplimentaryBudgetAllotmentController::class, 'create'])->name('create');
        Route::get('/show/{id}', [SuplimentaryBudgetAllotmentController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [SuplimentaryBudgetAllotmentController::class, 'edit'])->name('edit');
        Route::post('/', [SuplimentaryBudgetAllotmentController::class, 'store'])->name('store');
        Route::put('/update/{id}', [SuplimentaryBudgetAllotmentController::class, 'update'])->name('update');
        Route::post('/{id}', [SuplimentaryBudgetAllotmentController::class, 'destroy'])->name('delete');
    });

    Route::group(['prefix' => 'budget-allotment-brackdown', 'as' => 'budget_allotment_brackdown.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BudgetAllotmentBrackdownController::class, 'index'])->name('index');
        Route::get('/create', [BudgetAllotmentBrackdownController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BudgetAllotmentBrackdownController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetAllotmentBrackdownController::class, 'edit'])->name('edit');
        Route::post('/', [BudgetAllotmentBrackdownController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BudgetAllotmentBrackdownController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetAllotmentBrackdownController::class, 'destroy'])->name('delete');
    });

    Route::group(['prefix' => 'allotment-brackdown', 'as' => 'allotment_brackdown.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [AllotmentBreakdownController::class, 'index'])->name('index');
        Route::get('/create', [AllotmentBreakdownController::class, 'create'])->name('create');
        Route::get('/show/{id}', [AllotmentBreakdownController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [AllotmentBreakdownController::class, 'edit'])->name('edit');
        Route::post('/', [AllotmentBreakdownController::class, 'store'])->name('store');
        Route::put('/update/{id}', [AllotmentBreakdownController::class, 'update'])->name('update');
        Route::post('/{id}', [AllotmentBreakdownController::class, 'destroy'])->name('delete');



        Route::get('range-report', [AllotmentBreakdownController::class, 'rangeWiseReport'])->name('range_wise_report');
        Route::get('base-amount-report', [PdfController::class, 'baseAllotementReport'])->name('baseAllotement');
        Route::get('range-report-excel', [ExcelExportController::class, 'export'])->name('allotment_breakdown_excel');


    });

    Route::group(['prefix' => 'budget-allotments','middleware' => ['web'] , 'as' => 'budget_allotment.'], function () {
        Route::get('/', [BudgetAllotmentController::class, 'index'])->name('index');
        Route::get('/create', [BudgetAllotmentController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BudgetAllotmentController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetAllotmentController::class, 'edit'])->name('edit');
        Route::post('/', [BudgetAllotmentController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BudgetAllotmentController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetAllotmentController::class, 'destroy'])->name('delete');
        
        Route::get('range-wise-report', [BudgetAllotmentController::class, 'rangeWiseReport'])->name('range_wise_report');
        Route::get('type-wise-report', [BudgetAllotmentController::class, 'TypeWiseReport'])->name('type_wise_report');
    });

    
    Route::group(['prefix' => 'budgetcode-to-item', 'as' => 'budgetcode_to_item.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BudgetCodeToItemController::class, 'index'])->name('index');
        Route::get('/create', [BudgetCodeToItemController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BudgetCodeToItemController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetCodeToItemController::class, 'edit'])->name('edit');
        Route::post('/', [BudgetCodeToItemController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BudgetCodeToItemController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetCodeToItemController::class, 'destroy'])->name('delete');


    });

    Route::group(['prefix' => 'pdf', 'as' => 'pdf.' , 'middleware' => ['web'] ], function () {
        
        Route::get('range-wise-report-pdf', [PDFController::class, 'rangeWiseReportPDF'])->name('range_wise_report_pdf');
        Route::get('type-wise-report-pdf', [PDFController::class, 'TypeWiseReportPDF'])->name('type_wise_report_pdf');

    });



    
    Route::group(['prefix' => 'expenditure-detials', 'as' => 'expenditure_detials.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [ExpenditureDetialsController::class, 'index'])->name('index');
        Route::get('/create', [ExpenditureDetialsController::class, 'create'])->name('create');
        Route::get('/show/{id}', [ExpenditureDetialsController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [ExpenditureDetialsController::class, 'edit'])->name('edit');
        Route::post('/', [ExpenditureDetialsController::class, 'store'])->name('store');
        Route::put('/update/{id}', [ExpenditureDetialsController::class, 'update'])->name('update');
        Route::post('/{id}', [ExpenditureDetialsController::class, 'destroy'])->name('delete');
        Route::get('excel', [ExcelExportController::class, 'expenditureExport'])->name('expenditure_excel');
        Route::get('balance-report', [ExpenditureDetialsController::class, 'balanceReport'])->name('balance_report');
        Route::get('balancesheet-report', [ExpenditureDetialsController::class, 'balanceSheetReport'])->name('balancesheet_report');
        Route::get('balancesheet-excel', [ExcelExportController::class, 'balanceSheetExport'])->name('balancesheet_excel');


    });


    //  Budget Letter

    Route::group(['prefix' => 'base-letter', 'as' => 'base-letter.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BaseLetterController::class, 'index'])->name('index');
        Route::get('base-report', [PdfController::class, 'baseReportPdf'])->name('baseReportPdf');

    });

    Route::group(['prefix' => 'base-expenditure', 'as' => 'base-expenditure.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BaseExpenditureController::class, 'index'])->name('index');
        Route::get('/create', [BaseExpenditureController::class, 'create'])->name('create');
        Route::get('/spent/{id}', [BaseExpenditureController::class, 'spent'])->name('spent');
        Route::get('/show/{id}', [BaseExpenditureController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BaseExpenditureController::class, 'edit'])->name('edit');
        Route::post('/', [BaseExpenditureController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BaseExpenditureController::class, 'update'])->name('update');
        Route::post('/{id}', [BaseExpenditureController::class, 'destroy'])->name('delete');

    });

    
    Route::get('/', function () {
        return view('budget::dashboard');
    });

   
    Route::group(['prefix' => 'base-level-budget-transfers', 'as' => 'base-level-budget-transfer.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BaseLevelBudgetTransferController::class, 'index'])->name('index');
        Route::get('/create', [BaseLevelBudgetTransferController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BaseLevelBudgetTransferController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BaseLevelBudgetTransferController::class, 'edit'])->name('edit');
        Route::post('/', [BaseLevelBudgetTransferController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BaseLevelBudgetTransferController::class, 'update'])->name('update');
        Route::post('/{id}', [BaseLevelBudgetTransferController::class, 'destroy'])->name('delete');



        
    });




    Route::group(['prefix' => 'register-book', 'as' => 'register-book.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [RegisterBookController::class, 'index'])->name('index');
        Route::get('/create', [RegisterBookController::class, 'create'])->name('create');
        Route::get('/show/{id}', [RegisterBookController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [RegisterBookController::class, 'edit'])->name('edit');
        Route::post('/', [RegisterBookController::class, 'store'])->name('store');
        Route::put('/update/{id}', [RegisterBookController::class, 'update'])->name('update');
        Route::post('/{id}', [RegisterBookController::class, 'destroy'])->name('delete');

    });


    Route::group(['prefix' => 'base-demand-allotment-breakdown', 'as' => 'base-demand-allotment-breakdown.' , 'middleware' => ['web'] ], function () {
        Route::get('/', [BDemandAllotmentBreakdownController::class, 'index'])->name('base_demand');
    });


    Route::get('/letter/{scanfile}', [BudgetTransferController::class, 'showFile'])->name('letter');
    Route::get('/supplementary-letter/{scanfile}', [SuplimentaryBudgetAllotmentController::class, 'showFile'])->name('supplementary-letter');


    Route::get('/getBudgetData/{id}', [BudgetAllotmentController::class, 'getBudgetInfo']);
    Route::get('fin-year/{id}', [RegisterBookController::class, 'finYear']);

    Route::get('/getBalance/{id}', [BaseLevelBudgetTransferController::class, 'getBalance']);

    Route::get('/update-item', [BudgetCodeToItemController::class, 'updateItem']);



    