<?php

namespace Pondit\Baf\Budget\Providers;

use Illuminate\Support\ServiceProvider;

class BudgetServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../Routes/api.php');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        // $this->publishes([
        //     __DIR__ . '/../../publishable/assets' => public_path('vendor/pondit/baf/budget/assets'),
        // ], 'public');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'budget');
    }

    public function register(): void
    {
        // Register stuffs
    }
}
