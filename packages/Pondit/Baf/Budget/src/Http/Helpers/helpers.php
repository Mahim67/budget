<?php

use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\SuplimentaryBudgetAllotment;

function getSuplimentary($id){
    $data = BudgetAllotment::find($id);
    $suplimentryData = SuplimentaryBudgetAllotment::where('budgetcode_id',$data->budgetcode_id)->where('range_id',$data->range_id)->get();
    return $suplimentryData ;
}

function getBudgetCode($id){
    $budgetCode = Budgetcode::find($id);
    return $budgetCode;
}


function rangeName($rangeId){
    
    $rangeName = Range::where('id', $rangeId)
                ->select('title')            
                ->first();
    if(!is_null($rangeName)) {
    	return $rangeName->title;
    } else {
        return '';
    }

}

function nameofCode($allotmentId){
   $name =  BudgetAllotment::where('id', $allotmentId)->select('new_code')->first();
   return $name->new_code;
}

function currentBalance($allotmentId){
    $balance =  BudgetAllotment::where('id', $allotmentId)->select('balance')->first();
    return $balance->balance;
 
}

function nameBudgetCode($id){
    $name =  Budgetcode::where('id', $id)->select('newcode')->first();
    return $name->newcode ?? '';
 }

 
 function currentFY(){
    if (date('m') <= 6) {
        $financial_year = (date('Y')-1) . '-' . date('Y');
    } else {
        $financial_year = date('Y') . '-' . (date('Y') + 1);
    }
    return $financial_year;
}

?>
