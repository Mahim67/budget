<?php


namespace Pondit\Baf\Budget\Http\Controllers;

use App\Http\Controllers\Controller;

use Pondit\Baf\Budget\Exports\AllotmentsExport;
use Pondit\Baf\Budget\Exports\BalanceSheetExport;
use Maatwebsite\Excel\Facades\Excel;
use Pondit\Baf\Budget\Exports\ExpenditureDetialsExport;

class ExcelExportController extends Controller
{
    public function export() 
    {
        return Excel::download(new AllotmentsExport, 'allotment.xlsx');
    }

    public function expenditureExport()
    {
        return Excel::download(new ExpenditureDetialsExport, 'expenditure.xlsx');

    }

    public function balanceSheetExport()
    {
        return Excel::download(new BalanceSheetExport, 'balance-excel.xlsx');
    }
    
}

