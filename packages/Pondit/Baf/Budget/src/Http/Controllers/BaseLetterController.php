<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\AllotmentBreakdown;

class BaseLetterController extends Controller
{
    public function index()
    {
        $userBase  = "base_bbd";
        $data = AllotmentBreakdown::where($userBase, '!=', null)->get();
        return view('budget::base-letter.index', compact('data', 'userBase'));
    }
}
