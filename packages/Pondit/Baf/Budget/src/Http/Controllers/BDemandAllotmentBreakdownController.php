<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\BDemandAllotmentBreakdown;


class BDemandAllotmentBreakdownController extends Controller
{
    public function index()
    {
        $bases = ['base_bsr','base_bbd','base_mtr','base_zhr'];
        $arr = [];
        foreach($bases as $base){
           $singleData =  BDemandAllotmentBreakdown::where($base,  '!=', null)->select($base,'new_code','old_code','description')->get();
           array_push( $arr, $singleData);
        }  

        $data = array_combine( $bases, $arr);
        
        return view('budget::allotment-brackdown.base-wise-demad', compact('data','bases'));
    }
}
