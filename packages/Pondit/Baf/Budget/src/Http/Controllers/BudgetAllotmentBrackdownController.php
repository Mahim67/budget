<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\BudgetAllotmentBrackdown;


class BudgetAllotmentBrackdownController extends Controller
{
    public function index()
    {
    
        $data = BudgetAllotmentBrackdown::latest()->get();
        return view('budget::budget-allotment-brackdown.index', compact('data'));
    }
    public function create()
    {
        $budgetAllotmentBrackdons = BudgetAllotment::pluck('description','id')->toArray();
        return view('budget::budget-allotment-brackdown.create',compact('budgetAllotmentBrackdons'));
    }
    public function store(Request $request)
    {
        try
        {   

            $data = [
                'budget_allotment_id'       => $request->budgetcode_id,
                'amount'                    => $request->amount,
                'alloted_to'                => $request->alloted_to,
                'reference_no'              => $request->reference_no,
            ];
 
            if ($files = $request->file('letter')) {
                $currentDate = Carbon::now()->toDateString();
                $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
                $dir = "storage/letter";
                $files->move($dir, $imageName);
                $data['letter'] ='storage/letter/' . $imageName;
            }    
        

            BudgetAllotmentBrackdown::create($data);

            return redirect()->route('budget_allotment_brackdown.index')->withMessage('Entity Create Successfull');
        }
        catch (Exception $th)
        {
            dd($th->getMessage());
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = BudgetAllotmentBrackdown::findOrFail($id);
        return \view('budget::budget-allotment-brackdown.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = BudgetAllotmentBrackdown::findOrFail($id);
        return \view('budget::budget-allotment-brackdown.edit', compact('data'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $data = BudgetAllotmentBrackdown::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);
                 
            $prepareData = [
                'budget_allotment_id'       => $request->budgetcode_id,
                'amount'                    => $request->amount,
                'alloted_to'                => $request->alloted_to,
                'reference_no'              => $request->reference_no,
            ];
 
            if ($files = $request->file('letter')) {
                $currentDate = Carbon::now()->toDateString();
                $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
                $dir = "storage/letter";
                $files->move($dir, $imageName);
                $prepareData['letter'] ='storage/letter/' . $imageName;
            }  

            $update = BudgetAllotmentBrackdown::where('id',$id)->update($prepareData);
            if(!$update)
                throw new Exception("Unable To Update", 403);
                

            return redirect()
                ->route('budget_allotment_brackdown.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            dd($th->getMessage());
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = BudgetAllotmentBrackdown::findOrFail($id);
        $data->delete();

        return redirect()
                    ->route('budget_allotment_brackdown.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    
    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }
}