<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use DateTime;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\BaseLevelBudgetTransfer;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\BudgetTransfer;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\AllotmentBreakdown;

class BaseLevelBudgetTransferController extends Controller
{
    public function create(Request $request)
    {
        $userBase  = "base_bbd";
        $budgetCodes = AllotmentBreakdown::where($userBase, '!=', null)->get();

        if(isset($request->filter_min)){
            $time = Carbon::now()->subMinutes($request->filter_min);
            $ct = $time->toDateTimeString();
            $data = BaseLevelBudgetTransfer::where('created_at', '>=', $ct )->get();
        }else{
            $data = BaseLevelBudgetTransfer::whereDate('created_at', Carbon::today())->get();
        }

        return view('budget::base-level-budget-transfer.create',compact('budgetCodes','userBase','data'));
    }
    public function store(Request $request)
    {
        try
        {   
            $userBase  = "base_bbd";
    
            $data = [

                'from_code'                 => $request->from_code,
                'to_code'                   => $request->to_code,
                'amount'                    => $request->amount,
                'transfer_at'               => Carbon::now()->toDateTimeString(),
                'transfer_by'               => Auth::user()->id ?? 1 ,
                'reference_no'              => 1,
                'base_name'                 =>  $userBase,
            ];

        
       
            
            BaseLevelBudgetTransfer::create($data);

            $fromCodeAllotment = AllotmentBreakdown::where('budget_allotment_id',$request->from_code)->first();
           
          
            $fromCodeAllotment->update([
                $userBase => $fromCodeAllotment->$userBase - $request->amount
            ]);

            $toCodeAllotment = AllotmentBreakdown::where('budget_allotment_id', $request->to_code)->first();
         
          
            $toCodeAllotment->update([
                $userBase => $toCodeAllotment->$userBase + $request->amount
            ]);

           
            return redirect()->back()->withSuccess('Balance Transfer Successfull');
             
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }

    public function getBalance($id)
    {
        // $userBase  = "base_bbd";
        $data = BudgetAllotment::where('id', $id)->pluck('balance')->first();
        return response()->json($data);
    }

}
