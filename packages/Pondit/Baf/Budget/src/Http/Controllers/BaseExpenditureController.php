<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Pondit\Baf\Budget\Models\BaseExpenditure;
use Pondit\Baf\Budget\Models\AllotmentBreakdown;


class BaseExpenditureController extends Controller
{
    public function index()
    {
        $userBase  = "base_bsr";
        $data = AllotmentBreakdown::where($userBase, '!=', null)->get();

        Session::put('base_name', $userBase);
        
        return view('budget::base-expenditure.index', compact('data', 'userBase'));
    }

    public function spent($id)
    {
        $budgetAllotment = AllotmentBreakdown::where('budget_allotment_id', $id)->first();
        $data = BaseExpenditure::where('budget_allotment_id', $id)->get();
        return view('budget::base-expenditure.spent-list', compact('data','budgetAllotment'));
    }

    public function  store(Request $request)
    {
        try{
            $base_name = Session::get('base_name');
            $data = $request->all();
            $data['base_name'] = $base_name;

            BaseExpenditure::create($data);
            return redirect()
                ->back()
                ->withSuccess('Expenditure has been Added successfully!');
        
        }catch(Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
        
    }
    
}
