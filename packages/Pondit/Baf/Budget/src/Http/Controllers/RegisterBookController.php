<?php

namespace Pondit\Baf\Budget\Http\Controllers;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\RegisterBook;

class RegisterBookController extends Controller
{
    public function index(Request $request)
    {
        if(isset($request->fin_year)){
           $data =  RegisterBook::where('fin_year', $request->fin_year)->get()->groupBy('budget_code');

        }else{
            $data = RegisterBook::all()->groupBy('budget_code');

        }

        return view('budget::register-book.index', compact('data'));
    }



    public function create()
    {
        $budgetcodes = Budgetcode::all();
        $ranges = Range::pluck('title','id')->toArray();


        return view('budget::register-book.create',compact('budgetcodes', 'ranges'));
    }

    public function  store(Request $request)
    {
        try{
            $data = $request->except('_token','range_id');
            $data['fin_year'] = currentFY();

            $registerBook = RegisterBook::create($data);

            $registerBook->range()->attach($request->range);

            return redirect()
                ->route('register-book.index')
                ->withSuccess('Entity has been Added successfully in Register Book !');
        
        }catch(Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
}
