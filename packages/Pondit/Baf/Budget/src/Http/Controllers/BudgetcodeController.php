<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\MasterData\Models\Range;

class BudgetcodeController extends Controller
{
    public function index()
    {
        $data = Budgetcode::latest()->get();
        return view('budget::budgetcode.index', compact('data'));
    }
    public function create()
    {
        $ranges = Range::pluck('title','id')->toArray();
        return view('budget::budgetcode.create',compact('ranges'));
    }
    public function store(Request $request)
    {
        try
        {   
            $data = $request->except('_token','range_id');
            if (isset($request->is_duplicate)) {
                $data['is_duplicate'] = 1 ;
            }
            else{
                $data['is_duplicate'] = 0 ;
            }
            $range_id = $request->range;
            $budgetcode = Budgetcode::create($data);
            $budgetcode->range()->attach($range_id);

            return redirect()->route('budgetcode.index')->withSuccess('Budget Code Create Successfull');
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = Budgetcode::findOrFail($id);
        // dd($data);
        return \view('budget::budgetcode.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = Budgetcode::findOrFail($id);
        $ranges = Range::pluck('title','id')->toArray();
        $seleced_ranges = $data->range->pluck('id')->toArray();
        return \view('budget::budgetcode.edit', compact('data','ranges','seleced_ranges'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $requestData = $request->except('_token','_method');
            if(is_null($requestData))
                throw new Exception("Give Input Data", 403);
                
            $data = Budgetcode::findOrFail($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);
                
            $range_id = $request->range;

            $data->update($requestData);
            $data->range()->sync($range_id);

            return redirect()
                ->route('budgetcode.index')
                ->withSuccess('Budget Code has been updated successfully!');
        }
        catch (Exception $th)
        {
            dd($th->getMessage());
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Budgetcode::findOrFail($id);
        $range = $data->range;
        $data->delete();
        $data->range()->detach($range);

        return redirect()
                    ->route('budgetcode.index')
                    // ->with('success','Entity has been deleted successfully!');
                    ->withMessage('Entity has been deleted successfully!');
    }

    public function getBudgetcode()
    {
        $range_id = request()->range_id ;
        $range = Range::find($range_id);
        $budgetcodes = $range->budgetCode;
        return response()->json([
            'status'    => 'ok',
            'data'      => $budgetcodes
        ]);
    }
    public function getRange()
    {
        try {
            $budgetcode_id = request()->budgetcode_id ;
            $budgetcode =   Budgetcode::find($budgetcode_id); 
    
            if(!$budgetcode)
                throw new Exception("BudgetCode NOt Found ", 404);
                
            $ranges = $budgetcode->range;
            
            return response()->json([
                'status'    => 'ok',
                'data'      => $ranges
            ]);
        } catch (\Exception $th) {

            return response()->json([
                'status'        => 'notOk',
                'message'       => $th->getMessage()
            ]);
            
        }

    }

    public function getBudgetcodeByCode()
    {
        try {
            if (isset(request()->oldCode)) {
                $oldCode = request()->oldCode ;
                $budgetcodes = Budgetcode::where('oldcode',$oldCode)->first();
                return response()->json([
                    'status'    => 'ok',
                    'data'      => $budgetcodes
                ]);
            }
            if (isset(request()->newCode)) {
                $newCode = request()->newCode ;
                $budgetcodes = Budgetcode::where('newcode',$newCode)->first();
                
                return response()->json([
                    'status'    => 'ok',
                    'data'      => $budgetcodes
                ]);
            }
        } catch (\Exception $th) {
            return response()->json([
                'status'    => 'notOk',
                'massege'   =>$th->getMessage()
            ]);
        }

    }


    
    
}