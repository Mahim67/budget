<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\SuplimentaryBudgetAllotment;

class BudgetAllotmentController extends Controller
{
    public function index()
    {
        $data = BudgetAllotment::latest()->get();
        return view('budget::budget-allotment.index', compact('data'));
    }
    public function create()
    {
        $budgetcodes = Budgetcode::all();
        $ranges = Range::pluck('title','id')->toArray();
        return view('budget::budget-allotment.create',compact('budgetcodes','ranges'));
    }
    public function store(Request $request)
    {
        try
        {        
            $budgetcode = Budgetcode::find($request->budgetcode_id);
            
            if(is_null($budgetcode))
                throw new Exception("Budgetcode Not Found", 404);
                
            $range = Range::find($request->range_id);
            if(is_null($range))
                throw new Exception("Range Not Found", 404);

            $prepareData = [
                'fin_year'                  => currentFY(),
                'budgetcode_id'             => $budgetcode->id,
                'description'               => $budgetcode->description,
                'range_name'                => $range->title,
                'range_id'                  => $range->id,
                'activity_type'             => $budgetcode->budget_type,
                'initial_amount'            => $request->initial_amount,
                'total_budgetcode_amount'   => $request->initial_amount,
                'balance'                   => $request->initial_amount,
                'new_code'                  => $budgetcode->newcode,
                'old_code'                  => $budgetcode->oldcode,
            ];

            BudgetAllotment::create($prepareData);

            return redirect()->route('budget_allotment.index')->withSuccess('Budget Allotment Create Successfull');
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = BudgetAllotment::findOrFail($id);
        $suplimentries = SuplimentaryBudgetAllotment::where('budgetcode_id',$data->budgetcode_id)->get(); 
        return \view('budget::budget-allotment.show', \compact('data','suplimentries'));
    }
    public function edit($id)
    {
        $budgetcodes = Budgetcode::all();
        $ranges = Range::pluck('title','id')->toArray();
        $data = BudgetAllotment::findOrFail($id);
        return \view('budget::budget-allotment.edit', compact('data','budgetcodes','ranges'));
    }
    public function update(Request $request,  $id)
    {
        try{

            $budgetcode = Budgetcode::find($request->budgetcode_id);
            if(is_null($budgetcode))
                throw new Exception("Budgetcode Not Found", 404);
                
            $range = Range::find($request->range_id);
            if(is_null($range))
                throw new Exception("Range Not Found", 404);

            $prepareData = [
                'fin_year'                  => currentFY(),
                'budgetcode_id'             => $budgetcode->id,
                'description'               => $request->description,
                'range_name'                => $range->title,
                'range_id'                  => $range->id,
                'activity_type'             => $budgetcode->budget_type,
                'initial_amount'            => $request->initial_amount,
                'total_budgetcode_amount'   => $request->initial_amount,
                'balance'                   => $request->initial_amount,
            ];

            BudgetAllotment::where('id', $id)->update($prepareData);
                

            return redirect()
                ->route('budget_allotment.index')
                ->withSuccess('Budget Allotment has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            
            $data = BudgetAllotment::findOrFail($id);
            $suplimentries = SuplimentaryBudgetAllotment::where('budgetcode_id',$data->budgetcode_id)->get(); 
            
            foreach($suplimentries  as $suplimentry){
                $suplimentry->delete();
            }

            $data->delete();
    
            return redirect()
                        ->route('budget_allotment.index')
                        ->withSuccess('Entity has been deleted successfully!');

        } catch (\Exception $th) {

            return redirect()->back()->withErrors($th->getMessage());
        }

    }

    public function rangeWiseReport()
    {
        $data = BudgetAllotment::all()->groupBy('range_name');

        $maxSuplymentry = BudgetAllotment::max('total_no_suplimentary_budgets');
        return view('budget::budget-allotment.range-wise-report',compact('data','maxSuplymentry'));
    }

    public function TypeWiseReport()
    {
        $data = BudgetAllotment::all()->groupBy('activity_type');
        $maxSuplymentry = BudgetAllotment::max('total_no_suplimentary_budgets');
        return view('budget::budget-allotment.type-wise-report',compact('data','maxSuplymentry'));
    }

    public function getBudgetInfo($id)
    {
        $data = DB::table('budget_allotments')
        ->select('budget_allotments.*')
        ->where('budget_allotments.budgetcode_id', '=', $id)
        ->first();

         

        $transferAmount  =  DB::table('budget_allotments')
        ->join('budget_transfers', 'budget_allotments.budgetcode_id', '=', 'budget_transfers.from_code')
        ->where('budget_transfers.from_code', '=', $id)
        ->select('budget_transfers.*')
        ->get();
       
        return \response()->json([
            'budget' => $data,
            'transfer' => $transferAmount

        ]);
    }
    
    
}