<?php

namespace Pondit\Baf\Budget\Http\Controllers;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Pondit\Baf\Budget\Models\PItem;
use App\Http\Controllers\Controller;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\BudgetCodeToItem;

class BudgetCodeToItemController extends Controller
{
    public function index()
    {
        $data = BudgetCodeToItem::latest()->get();
        return view('budget::budgetcode-to-item.index', compact('data'));
    }
    public function create()
    {
        $budgetcodes = Budgetcode::all();
        $ranges = Range::pluck('title','id')->toArray();
        return view('budget::budgetcode-to-item.create',compact('budgetcodes','ranges'));
    }
    public function store(Request $request)
    {
        try
        {   

            $range = Range::find($request->range_id);
            
            if(!$range)
                throw new Exception("Range Not Found", 404);
                

            $data = [
                'budgetcode_id'       => $request->budgetcode_id,
                'item_name'           => $request->item_name,
                'range_id'            => $range->id,
                'range_name'          => $range->title,
            ];

            BudgetCodeToItem::create($data);

            return redirect()->route('budgetcode_to_item.index')->withSuccess('Entity Create Successfull');
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = BudgetCodeToItem::findOrFail($id);
        return \view('budget::budgetcode-to-item.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = BudgetCodeToItem::findOrFail($id);
        $budgetcodes = Budgetcode::all();
        $ranges = Range::pluck('title','id')->toArray();
        return \view('budget::budgetcode-to-item.edit', compact('data','budgetcodes','ranges'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $data = BudgetCodeToItem::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);
                 
            $range = Range::find($request->range_id);
        
            if(!$range)
                throw new Exception("Range Not Found", 404);
                

            $data = [
                'budgetcode_id'       => $request->budgetcode_id,
                'item_name'           => $request->item_name,
                'range_id'            => $range->id,
                'range_name'          => $range->title,
            ];


            $update = BudgetCodeToItem::where('id',$id)->update($data);
            if(!$update)
                throw new Exception("Unable To Update", 403);
                

            return redirect()
                ->route('budgetcode_to_item.index')
                ->withSuccess('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = BudgetCodeToItem::findOrFail($id);
        $data->delete();

        return redirect()
                    ->route('budgetcode_to_item.index')
                    ->withSuccess('Entity has been deleted successfully!');
    }
    
    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }

    public function updateItem(){
        $datas = BudgetCodeToItem::all();

        foreach ($datas as $data) {
            
            if ( isset($data->item_name) && is_null($data->item_id)) {

                $product = PItem::where('title',$data->item_name)->first();
                if (!is_null($product)) {
                    BudgetCodeToItem::where('id',$data->id)->update(['item_id' => $product->id]);
                }
                else{
                    $itemData = [
                        'title'                          => $data->item_name,
                        'w_part_no'                      => $data->part_no ?? null,
                        'is_suggested_item'              => 2,
                    ];

                    $insertProduct = PItem::create($itemData);
                    BudgetCodeToItem::where('id',$data->id)->update(['item_id' => $insertProduct->id]);
                }
            }
        }

        // $getData =  BudgetCodeToItem::whereNull('item_id')->get();
        // $item = PItem::where('title','Digital Weighing Scale  Cap: 100Kg')->first();
        // dd($item);
        // dd($getData);

        return response()->json([
            'status'    => 'ok'
        ]);
    }
}
