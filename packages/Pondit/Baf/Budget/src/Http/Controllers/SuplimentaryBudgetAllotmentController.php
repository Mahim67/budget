<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Pondit\Baf\Budget\Models\Budget;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\SuplimentaryBudgetAllotment;

class SuplimentaryBudgetAllotmentController extends Controller
{
    public function index()
    {

        $data = SuplimentaryBudgetAllotment::latest()->get();
        return view('budget::suplimentary-budget-allotment.index', compact('data'));
    }
    public function create()
    {
        $budgetcodes = BudgetAllotment::all();

        return view('budget::suplimentary-budget-allotment.create',compact('budgetcodes'));
    }
    public function store(Request $request)
    {
        try
        {   
            $input                  = $request->all();
            $budget_allotment       = BudgetAllotment::whereIn('id',$request->budget_allotment_id)->get();
            
            $suplimentary_amount    = $input['suplimentary_amount'];

            $budgetData = [   
                'fin_year'                  => currentFY(),
                'reference_no'              => $request->reference_no,
                'type'                      => 'Suplimentary Budget',
            ];

           
            DB::beginTransaction();

            $budget = Budget::create($budgetData);

            for($i=0; $i< count($budget_allotment); $i++) {

                $lastId = SuplimentaryBudgetAllotment::orderBy('id','desc')->first();
                $lastId ? $insertId = $lastId->id : $insertId = 0 ;
                $id = $insertId + 1 ;

                $suplimentaryData= [
                    'id'                        => $id,
                    'budgetcode_id'             => $budget_allotment[$i]->budgetcode_id,
                    'range_id'                  => $budget_allotment[$i]->range_id,
                    'range_name'                => $budget_allotment[$i]->range_name,
                    'budget_id'                 => $budget->id,
                    'fin_year'                  => currentFY(),
                    'suplimentary_amount'       => (int)$suplimentary_amount[$i],
                    'budget_allotment_id'       => $budget_allotment[$i]->id,
                    'activity_type'             => $budget_allotment[$i]->activity_type ?? null,

                ];

                if ($request->file('letter')) {
                    $suplimentaryData['letter'] = $this->uploadFile($request->letter, $request->reference_no ?? null );
                }

                $initialBudgetAllotment = BudgetAllotment::where('id',$budget_allotment[$i]->id)->first();
                if(is_null($initialBudgetAllotment)){
                    continue;
                }
                $SuplimentaryBudgetAllotment =  SuplimentaryBudgetAllotment::create($suplimentaryData);
                    
                $suplimentary_budget_detail = json_encode($SuplimentaryBudgetAllotment);
                $total_no_suplimentary_budgets = $initialBudgetAllotment->total_no_suplimentary_budgets ? $initialBudgetAllotment->total_no_suplimentary_budgets+1 : 1 ;
                $total_suplimentary_amount  = $initialBudgetAllotment->total_suplimentary_amount != null ? $initialBudgetAllotment->total_suplimentary_amount + $suplimentary_amount[$i] : $suplimentary_amount[$i];
                $total_balance  = $initialBudgetAllotment->balance != null ? $initialBudgetAllotment->balance + $suplimentary_amount[$i] : $initialBudgetAllotment->initial_amount + $suplimentary_amount[$i];
                
                $prepareData = [
                    'total_no_suplimentary_budgets'         => $total_no_suplimentary_budgets ,
                    'suplimentary_budget_detail'            => $suplimentary_budget_detail,
                    'total_suplimentary_amount'             => $total_suplimentary_amount,
                    'total_budgetcode_amount'               => $initialBudgetAllotment->initial_amount + $total_suplimentary_amount,
                    'balance'                               => $total_balance
                    
                ];

              
                BudgetAllotment::where('id',$initialBudgetAllotment->id)->update($prepareData);
    
            }

            DB::commit(); 
        
            return redirect()->route('suplimentary_budget_allotment.index')->withSuccess('Entity Create Successfull');
        }
        catch (Exception $th)
        {
            // DB::rollBack();
            dd($th->getMessage());
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = SuplimentaryBudgetAllotment::findOrFail($id);
        return \view('budget::suplimentary-budget-allotment.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = SuplimentaryBudgetAllotment::findOrFail($id);
        $budgetcodes = Budgetcode::pluck('newcode','id')->toArray();
        $ranges = Range::pluck('title','id')->toArray();
        return \view('budget::suplimentary-budget-allotment.edit', compact('data','budgetcodes','ranges'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $data = SuplimentaryBudgetAllotment::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);
                 
            $prepareData = [
                'budgetcode_id'             => $request->budgetcode_id,
                'range_id'                  => $request->range_id,
                'suplimentary_amount'       => $request->suplimentary_amount,
            ];
 
            if ($request->hasFile('letter')) {
                $this->unlink($data->letter);
                $prepareData['letter'] = $this->uploadFile($request->letter, $request->reference_no ?? null);
            }

            $update = SuplimentaryBudgetAllotment::where('id',$id)->update($prepareData);
            if(!$update)
                throw new Exception("Unable To Update", 403);
                

            return redirect()
                ->route('suplimentary_budget_allotment.index')
                ->withSuccess('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = SuplimentaryBudgetAllotment::findOrFail($id);
        $data->delete();

        return redirect()
                    ->route('suplimentary_budget_allotment.index')
                    ->withSuccess('Entity has been deleted successfully!');
    }
    
    private function uploadFile($file, $name)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp .'-'.$name. '.' .$file->getClientOriginalExtension();

        $path = storage_path().'/app/public/supplementary-letter/';

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }

        $file->move($path, $file_name);

        return $file_name;
    }

    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/supplementary-letter/';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }

    public function showFile($scanFile)
    {
        $filename = $scanFile;
        $path = public_path('storage/supplementary-letter'). "/". $scanFile;

       
        $ext =File::extension($filename);
              
                if($ext=='pdf'){
                    $content_types='application/pdf';
                   }elseif ($ext=='doc') {
                     $content_types='application/msword';  
                   }elseif ($ext=='docx') {
                     $content_types='application/vnd.openxmlformats-officedocument.wordprocessingml.document';  
                   }elseif ($ext=='xls') {
                     $content_types='application/vnd.ms-excel';  
                   }elseif ($ext=='xlsx') {
                     $content_types='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';  
                   }elseif ($ext=='pptx') {
                    $content_types='application/vnd.openxmlformats-officedocument.spreadsheetml.PPT';  
                  }elseif ($ext=='txt') {
                     $content_types='application/octet-stream';  
                   }else{

                    exit('Requested file does not exist on our server!');
                   }

        return response(file_get_contents($path),200) ->header('Content-Type',$content_types);


    }
}