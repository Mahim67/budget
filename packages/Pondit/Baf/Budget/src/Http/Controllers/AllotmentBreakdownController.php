<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\AllotmentBreakdown;
use Pondit\Baf\Budget\Models\ExpenditureDetials;
use Pondit\Baf\Budget\Models\BDemandAllotmentBreakdown;

class AllotmentBreakdownController extends Controller
{

    public function index()
    {
        $data = AllotmentBreakdown::all();
        return view('budget::allotment-brackdown.index', compact('data'));
    }
    public function create()
    {
        $budgetcodes = BudgetAllotment::all();

        $ranges = Range::pluck('title','id')->toArray();
    
        return view('budget::allotment-brackdown.create',compact('budgetcodes','ranges'));
    }
    public function store(Request $request)
    {
        try
        {   

            $budgetAllotmentInfo = BudgetAllotment::where('id', $request->budget_allotment_id)->first();
            
            
            $data = [
                'budget_allotment_id'           =>$request->budget_allotment_id ?? null,
                'new_code'                      =>$budgetAllotmentInfo->new_code ?? null,
                'fin_year'                      =>currentFY(),
                'activity_type'                 =>$budgetAllotmentInfo->activity_type ?? null,
                'old_code'                      =>$budgetAllotmentInfo->old_code ?? null,
                'description'                   =>$budgetAllotmentInfo->description ??  null,
                'budget_code'                   => $request->budget_code ?? null,
                'range_id'                      => $budgetAllotmentInfo->range_id ?? null,
                'base_bsr'                      => $request->base_bsr ?? null,
                'base_mtr'                      => $request->base_mtr ?? null,
                'base_zhr'                      => $request->base_zhr ?? null,
                'base_bbd'                      => $request->base_bbd ?? null ,
                'base_pkp'                      => $request->base_pkp ?? null,
                'base_cxb'                      => $request->base_cxb ?? null,
                'base_air_hq'                   => $request->base_air_hq ?? null,
                'base_201mu'                    => $request->base_201mu ?? null,
                'base_201u'                     => $request->base_201u ?? null,
                'base_bru'                      => $request->base_bru ?? null,
                'base_mru'                      => $request->base_mru ?? null,
                'range_id'                      => $budgetAllotmentInfo->range_id ?? null,
                'intial_budget'                 => $budgetAllotmentInfo->initial_amount ?? null,
                'total_spent'                   => ( $request->base_bsr + $request->base_mtr + $request->base_zhr + $request->base_bbd + $request->base_pkp + $request->base_cxb + $request->base_air_hq + $request->base_201mu + $request->base_201u + $request->base_bru + $request->base_mru )
                
            ];
 
            // if ($files = $request->file('letter')) {
            //     $currentDate = Carbon::now()->toDateString();
            //     $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
            //     $dir = "storage/letter";
            //     $files->move($dir, $imageName);
            //     $data['letter'] ='storage/letter/' . $imageName;
            // }    
                
            if($request->demand_of_base == 1){

                $budgetAllotment = BDemandAllotmentBreakdown::create($data);   

            }else{

                $budgetAllotment = AllotmentBreakdown::create($data);
                $range = rangeName($budgetAllotment->range_id);
                $new_code = $budgetAllotment->new_code;

                $expenditureDetial = ExpenditureDetials::where('new_code',  $new_code)
                                    ->where('range', $range)
                                    ->first();
                                    
                if($expenditureDetial != null){
                    $expenditureDetial->update(['spent_by_base' => $budgetAllotment->total_spent ?? null]);   
                }

            }


            return redirect()->route('allotment_brackdown.index')->withSuccess('Budget Breakdown Successfull');
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = AllotmentBreakdown::findOrFail($id);
        return \view('budget::allotment-brackdown.show', \compact('data'));
    }
    public function edit($id)
    {
        $budgetcodes = BudgetAllotment::all();

        $ranges = Range::pluck('title','id')->toArray();
        $data = AllotmentBreakdown::findOrFail($id);
        return \view('budget::allotment-brackdown.edit', compact('data','budgetcodes','ranges'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $data = AllotmentBreakdown::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);
                
            $totalBaseAllotment = $request->base_bsr + $request->base_mtr + $request->base_zhr + $request->base_bbd + $request->base_pkp + $request->base_cxb + $request->base_air_hq + $request->base_201mu + $request->base_201u + $request->base_bru + $request->base_mru ;
            $budgetAllotment = BudgetAllotment::find($request->budget_allotment_id);
            $totalSpent = $totalBaseAllotment + $request->spent_fc_army +$request->spent_airhq ;
            $balance =  $budgetAllotment->balance - $totalSpent ;
            $data = [
                'budget_allotment_id'           => $request->budget_allotment_id,
                'old_code'                      => $budgetAllotment->budgetcode->oldcode,
                'new_code'                      => $budgetAllotment->budgetcode->newcode,
                'description'                   => $budgetAllotment->description,
                'range'                         => $budgetAllotment->range_name,
                'activity_type'                 => $budgetAllotment->activity_type,
                'intial_budget'                 => $budgetAllotment->initial_amount,
                'base_bsr'                      => $request->base_bsr,
                'base_mtr'                      => $request->base_mtr,
                'base_zhr'                      => $request->base_zhr,
                'base_bbd'                      => $request->base_bbd,
                'base_pkp'                      => $request->base_pkp,
                'base_cxb'                      => $request->base_cxb,
                'base_air_hq'                   => $request->base_air_hq,
                'base_201mu'                    => $request->base_201mu,
                'base_201u'                     => $request->base_201u,
                'base_bru'                      => $request->base_bru,
                'base_mru'                      => $request->base_mru,
                'total_base_allotment'          => $totalBaseAllotment,
                'spent_fc_army'                 => $request->spent_fc_army,
                'spent_base_unit'               => $totalBaseAllotment,
                'spent_airhq'                   => $request->spent_airhq,
                'total_spent'                   => $totalSpent,
                'balance'                       => $balance,
            ];
    
 
            // if ($files = $request->file('letter')) {
            //     $currentDate = Carbon::now()->toDateString();
            //     $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
            //     $dir = "storage/letter";
            //     $files->move($dir, $imageName);
            //     $prepareData['letter'] ='storage/letter/' . $imageName;
            // }  

            $update = AllotmentBreakdown::where('id',$id)->update($data);
            if(!$update)
                throw new Exception("Unable To Update", 403);
                

            return redirect()
                ->route('allotment_brackdown.index')
                ->withSuccess('Budget Breakdown has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = AllotmentBreakdown::findOrFail($id);
        $data->delete();

        return redirect()
                    ->route('allotment_brackdown.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    

    // range wise 
     public function rangeWiseReport()
     {
        $data = AllotmentBreakdown::all()->groupBy('range_id');
        return view('budget::allotment-brackdown.range-wise', compact('data'));
     }
}