<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\BudgetTransfer;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Illuminate\Support\Facades\Response;



class BudgetTransferController extends Controller
{
    public function index()
    {
        $data = BudgetTransfer::latest()->get();
        return view('budget::budget-transfer.index', compact('data'));
    }
    public function create()
    {
        $budgetCodes = BudgetAllotment::all();
      
        return view('budget::budget-transfer.create',compact('budgetCodes'));
    }
    public function store(Request $request)
    {
        try
        {   
    
            $data = [

                'from_code'                 => $request->from_code,
                'to_code'                   => $request->to_code,
                'fin_year'                  => currentFY(),
                'amount'                    => $request->amount,
                'transfer_at'               => Carbon::now()->toDateTimeString(),
                'transfer_by'               => Auth::user()->id ?? 1 ,
                'reference_no'              => $request->reference_no,
            ];

            

            if ($request->file('letter')) {
                $data['letter'] = $this->uploadFile($request->letter, $request->from_code);
            }


            $transter = BudgetTransfer::create($data);

            $transferJSON = json_encode($transter);
            
            $fromCodeAllotment = BudgetAllotment::where('id',$request->from_code)->first();
            $fromCodeUpdateValue = [

                'total_budgetcode_amount'       => $fromCodeAllotment->total_budgetcode_amount - $transter->amount ,
                'balance'                       => $fromCodeAllotment->balance -  $transter->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;
            $fromCodeAllotment->update($fromCodeUpdateValue);

            $toCodeAllotment = BudgetAllotment::where('id',$request->to_code)->first();
            $toCodeUpdateValue = [

                'total_budgetcode_amount'       => $toCodeAllotment->total_budgetcode_amount + $transter->amount ,
                'balance'                       => $toCodeAllotment->balance +  $transter->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;
            $toCodeAllotment->update($toCodeUpdateValue);

            return redirect()->route('budget-transfer.index')->withSuccess('Budget Transfer Successfull');
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = BudgetTransfer::findOrFail($id);
        return \view('budget::budget-transfer.show', \compact('data'));
    }
    public function edit($id)
    {
        $budgetCodes = BudgetAllotment::all();
        

        $data    = BudgetTransfer::findOrFail($id);
        $balance = BudgetAllotment::where('id', $data->from_code)->pluck('balance')->first();
    
        return \view('budget::budget-transfer.edit', compact('data', 'budgetCodes', 'balance'));
    }
    public function update(Request $request,  $id)
    {
        try{

            $data = BudgetTransfer::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);

            $prepareData = [
                'amount'                    => $request->amount,
                'reference_no'              => $request->reference_no,
            ];

            
            if ($request->hasFile('letter')) {
                $this->unlink($data->letter);
                $prepareData['letter'] = $this->uploadFile($request->letter, $request->from_code);
            }


            $update = BudgetTransfer::where('id',$id)->update($prepareData);
            if(!$update)
                throw new Exception("Unable To Update", 403);


            $transfereddata = BudgetTransfer::find($id);

            $transferJSON = json_encode($transfereddata);
        
            $fromCodeAllotment = BudgetAllotment::where('id',$transfereddata->from_code)->first();
            $fromCodeUpdateValue = [

                'total_budgetcode_amount'       => ($fromCodeAllotment->total_budgetcode_amount + $data->amount) - $transfereddata->amount ,
                'balance'                       => ($fromCodeAllotment->balance + $data->amount) -  $transfereddata->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;



            $fromCodeAllotment->update($fromCodeUpdateValue);

            $toCodeAllotment = BudgetAllotment::where('id',$transfereddata->to_code)->first();
            $toCodeUpdateValue = [

                'total_budgetcode_amount'       => ($toCodeAllotment->total_budgetcode_amount - $data->amount ) + $transfereddata->amount ,
                'balance'                       => ($toCodeAllotment->balance - $data->amount ) +  $transfereddata->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;
            $toCodeAllotment->update($toCodeUpdateValue);

            return redirect()
                ->route('budget-transfer.index')
                ->withSuccess('Budget  Updated Successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = BudgetTransfer::findOrFail($id);
        $fromCodeAllotment = BudgetAllotment::where('budgetcode_id',$data->from_code)->first();
        $fromCodeUpdateValue = [

            'total_budgetcode_amount'       => $fromCodeAllotment->total_budgetcode_amount + $data->amount ,
            'balance'                       => $fromCodeAllotment->balance + $data->amount,
            'transfered_budget_detail'      => null,
        ] ;



        $fromCodeAllotment->update($fromCodeUpdateValue);

        $toCodeAllotment = BudgetAllotment::where('budgetcode_id',$data->to_code)->first();
        $toCodeUpdateValue = [

            'total_budgetcode_amount'       => $toCodeAllotment->total_budgetcode_amount - $data->amount ,
            'balance'                       => $toCodeAllotment->balance - $data->amount,
            'transfered_budget_detail'      => null,
        ] ;
        $toCodeAllotment->update($toCodeUpdateValue);
        
        $data->delete();

        return redirect()
                    ->route('budget-transfer.index')
                    ->withSuccess('Entity has been deleted successfully!');
    }


    public function getBudgetCode($id){
        $budgetcode = Budgetcode::find($id);
        return $budgetcode;
    }


    private function uploadFile($file, $name)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp .'-'.$name. '.' .$file->getClientOriginalExtension();

        $path = storage_path().'/app/public/letter/';

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }

        $file->move($path, $file_name);

        return $file_name;
    }

    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/letter/';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }
    

    public function showFile($scanFile)
    {
        $filename = $scanFile;
        $path = public_path('storage/letter'). "/". $scanFile;

       
        $ext =File::extension($filename);
              
                if($ext=='pdf'){
                    $content_types='application/pdf';
                   }elseif ($ext=='doc') {
                     $content_types='application/msword';  
                   }elseif ($ext=='docx') {
                     $content_types='application/vnd.openxmlformats-officedocument.wordprocessingml.document';  
                   }elseif ($ext=='xls') {
                     $content_types='application/vnd.ms-excel';  
                   }elseif ($ext=='xlsx') {
                     $content_types='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';  
                   }elseif ($ext=='pptx') {
                    $content_types='application/vnd.openxmlformats-officedocument.spreadsheetml.PPT';  
                  }elseif ($ext=='txt') {
                     $content_types='application/octet-stream';  
                   }else{

                    exit('Requested file does not exist on our server!');
                   }

        return response(file_get_contents($path),200) ->header('Content-Type',$content_types);


    }


}
