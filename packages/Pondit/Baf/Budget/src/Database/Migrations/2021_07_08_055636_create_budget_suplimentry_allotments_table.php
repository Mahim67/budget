<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetSuplimentryAllotmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_suplimentry_allotments', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_allotment_id')->nullable();
            $table->integer('budgetcode_id')->nullable();
            $table->integer('range_id')->nullable();
            $table->string('range_name')->nullable();
            $table->integer('budget_id')->nullable();
            $table->string('fin_year')->nullable();
            $table->string('activity_type')->nullable();
            $table->integer('suplimentary_amount')->nullable();
            $table->integer('total_allotment')->nullable();
            $table->string('letter')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_suplimentry_allotments');
    }
}
