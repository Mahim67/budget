<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CraeteBudgetAllotmentBrackdownTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_allotment_brackdown', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_allotment_id')->nullable();
            $table->integer('amount')->nullable();
            $table->string('alloted_to')->nullable();
            $table->string('letter')->nullable();
            $table->string('reference_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_allotment_brackdown');
    }
}
