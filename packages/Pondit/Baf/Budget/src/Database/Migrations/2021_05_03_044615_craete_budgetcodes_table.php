<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CraeteBudgetcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgetcodes', function (Blueprint $table) {
            $table->id();
            $table->string('oldcode')->nullable();
            $table->string('newcode')->nullable();
            $table->string('description')->nullable();
            $table->string('budget_type')->nullable();
            $table->tinyInteger('is_duplicate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budgetcodes');
    }
}
