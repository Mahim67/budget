<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBDemandAllotmentBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_demand_allotment_breakdowns', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_allotment_id')->nullable();
            $table->integer('old_code')->nullable();
            $table->integer('new_code')->nullable();
            $table->string('description')->nullable();
            $table->string('fin_year')->nullable();
            $table->string('range')->nullable();
            $table->string('activity_type')->nullable();
            $table->string('intial_budget')->nullable();
            $table->string('base_bsr')->nullable();
            $table->string('base_mtr')->nullable();
            $table->string('base_zhr')->nullable();
            $table->string('base_bbd')->nullable();
            $table->string('base_pkp')->nullable();
            $table->string('base_cxb')->nullable();
            $table->string('base_air_hq')->nullable();
            $table->string('base_201mu')->nullable();
            $table->string('base_201u')->nullable();
            $table->string('base_bru')->nullable();
            $table->string('base_mru')->nullable();
            $table->string('total_base_allotment')->nullable();
            $table->string('spent_fc_army')->nullable();
            $table->string('spent_base_unit')->nullable();
            $table->string('spent_airhq')->nullable();
            $table->string('total_spent')->nullable();
            $table->string('balance')->nullable();
            $table->integer('range_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_demand_allotment_breakdowns');
    }
}
