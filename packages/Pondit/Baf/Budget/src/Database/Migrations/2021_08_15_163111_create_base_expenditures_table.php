<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_expenditures', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_allotment_id')->nullable();
            $table->integer('base_id')->nullable();
            $table->string('base_name')->nullable();
            $table->string('fin_year')->nullable();
            $table->string('remarks')->nullable();
            $table->string('spent')->nullable();
            $table->string('balance')->nullable();
            $table->string('budget_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_expenditures');
    }
}
