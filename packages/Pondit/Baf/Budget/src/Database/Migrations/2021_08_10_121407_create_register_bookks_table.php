<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisterBookksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_bookks', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_allotment_id')->nullable();
            $table->integer('budget_code')->nullable();
            $table->string('fin_year')->nullable();
            $table->string('contract_no')->nullable();
            $table->string('date')->nullable();
            $table->string('description')->nullable();
            $table->string('foriegn_currency')->nullable();
            $table->string('local_currency')->nullable();
            $table->string('insurance_charge')->nullable();
            $table->string('date_of_delivery')->nullable();
            $table->string('name_of_firm')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_register_books');
    }
}
