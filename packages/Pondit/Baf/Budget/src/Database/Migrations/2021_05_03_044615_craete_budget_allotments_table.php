<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CraeteBudgetAllotmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_allotments', function (Blueprint $table) {
            $table->id();
            $table->integer('budgetcode_id')->nullable();
            $table->integer('budget_id')->nullable();
            $table->string('range_name')->nullable();
            $table->integer('range_id')->nullable();
            $table->string('fin_year')->nullable();
            $table->string('description')->nullable();
            $table->string('activity_type')->nullable();
            $table->integer('total_no_suplimentary_budgets')->nullable();
            $table->json('suplimentary_budget_detail')->nullable();
            $table->json('transfered_budget_detail')->nullable();
            $table->json('alloted_to_detail')->nullable();
            $table->integer('no_of_allotee')->nullable();
            $table->integer('initial_amount')->nullable();
            $table->integer('total_suplimentary_amount')->nullable();
            $table->integer('total_alloted_to_amount')->nullable();
            $table->integer('total_budgetcode_amount')->nullable();
            $table->integer('balance')->nullable();
            $table->string('old_code')->nullable();
            $table->string('new_code')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_allotments');
    }
}
