<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenditureDetialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenditure_detials', function (Blueprint $table) {
            $table->id();
            $table->string('dgdp')->nullable();
            $table->string('acceptance_no')->nullable();
            $table->string('contract_no')->nullable();
            $table->string('type_of_acc')->nullable();
            $table->string('old_code')->nullable();
            $table->string('new_code')->nullable();
            $table->string('file_rep_no')->nullable();
            $table->string('date')->nullable();
            $table->string('description')->nullable();
            $table->string('dte')->nullable();
            $table->string('range')->nullable();
            $table->string('qty')->nullable();
            $table->string('spent_by_airhq')->nullable();
            $table->string('spent_by_base')->nullable();
            $table->string('status')->nullable();
            $table->string('currency')->nullable();
            $table->string('print_code')->nullable();
            $table->string('fin_year')->nullable();
            $table->string('lc_commission')->nullable();
            $table->string('collection_charge')->nullable();
            $table->string('swift_charge')->nullable();
            $table->string('agency_comm')->nullable();
            $table->string('country')->nullable();
            $table->string('spl_code')->nullable();
            $table->string('name_of_company')->nullable();
            $table->string('acceptance_amount')->nullable();
            $table->string('spent_by_bank')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenditure_detials');
    }
}
