<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CraeteBudgetTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_transfers', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_allotment_id')->nullable();
            $table->string('from_code')->nullable();
            $table->string('to_code')->nullable();
            $table->string('fin_year')->nullable();
            $table->string('amount')->nullable();
            $table->timestamp('transfer_at')->nullable();
            $table->integer('transfer_by')->nullable();
            $table->string('letter')->nullable();
            $table->string('reference_no')->nullable();
            $table->integer('range_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_transfers');
    }
}
