<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetCodeToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_code_to_items', function (Blueprint $table) {
            $table->id();
            $table->integer('budgetcode_id');
            $table->integer('item_id')->nullable();
            $table->string('item_name')->nullable();
            $table->integer('range_id')->nullable();
            $table->string('range_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_code_to_items');
    }
}
