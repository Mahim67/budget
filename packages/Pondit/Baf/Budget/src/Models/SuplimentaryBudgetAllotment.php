<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Model;

class SuplimentaryBudgetAllotment extends Model
{


    protected $table = 'budget_suplimentry_allotments';
    protected $guarded = [];
    
    public function budgetcode(){
        return $this->belongsTo(Budgetcode::class);
    }
    

    public static $modelColumnsInfo = 
    [
        'fin_year',
        'budgetcode_id',
        'budget_id',
        'range_name',
        'range_id',
        'suplimentary_amount'

    ];


}
