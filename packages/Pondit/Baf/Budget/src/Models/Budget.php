<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{


    protected $table    = 'budgets';

    protected $guarded = [];
    
    /*
    **  RELATIONSHIPS
    */
    public static $modelColumnsInfo = 
    [
        'fin_year',
        'budgetcode_id',
        'budget_id',
        'description',
        'range_name',
        'range_id',
        'activity_type',
        'initial_amount',
        'total_budgetcode_amount',
        'balance',
    ];
    



}
