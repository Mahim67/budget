<?php

namespace Pondit\Baf\Budget\Models;

// use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\MasterData\Models\Range;

// use Illuminate\Database\Eloquent\SoftDeletes;

class Budgetcode extends Model
{
    // use SoftDeletes, RecordSequenceable;
    
    // protected $dates    = ['deleted_at'];

    protected $table    = 'budgetcodes';

    protected $fillable = ['id'
                            ,'oldcode'
                            ,'newcode'
                            ,'description'
                            ,'budget_type'
                            ,'is_duplicate'
                        ];
    /*
    **  RELATIONSHIPS
    */
    public static $budget_type = 
    [
        'general'       => 'General',
        'special'       => 'Special',
    ];
    
    public function suplimentaryBudget(){
        return $this->hasMany(SuplimentaryBudgetAllotment::class,'budgetcode_id');
    }

    public function budgetAllotment(){
        return $this->hasMany(BudgetAllotment::class);
    }

    public function budgetCodeToItem(){
        return $this->hasMany(BudgetCodeToItem::class , 'budgetcode_id');
    }

    public function range(){
        return $this->belongsToMany(Range::class,'budgetcodes_ranges','budgetcode_id','range_id');
    }

    

}
