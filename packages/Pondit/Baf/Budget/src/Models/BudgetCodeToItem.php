<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetCodeToItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function budgetcode(){
        return $this->belongsTo(Budgetcode::class);
    }

}
