<?php

namespace App\Models;

namespace Pondit\Baf\Budget\Models;

use App\Traits\GroupByPagination;
use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\MasterData\Models\Range;

class RegisterBook extends Model
{
    protected $table    = 'register_bookks';

    protected $fillable = [
        'id',
        'budget_code',
        'contract_no',
        'description',
        'date',
        'foriegn_currency',
        'local_currency',
        'insurance_charge',
        'date_of_delivery',
        'name_of_firm',
        'remarks',
        'fin_year'
    ];

    public function range(){
        return $this->belongsToMany(Range::class,'ranges_register_bookks','register_book_id', 'range_id');

    }
}
