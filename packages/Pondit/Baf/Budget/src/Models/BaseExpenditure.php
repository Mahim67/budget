<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Model;

class BaseExpenditure extends Model
{
    protected $table    = 'base_expenditures';

    protected $guarded = [];
}
