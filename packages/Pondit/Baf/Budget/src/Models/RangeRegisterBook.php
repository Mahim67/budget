<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RangeRegisterBook extends Model
{
    use HasFactory;

    protected $table    = 'ranges_register_bookks';

}
