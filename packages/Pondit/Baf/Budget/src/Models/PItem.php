<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PItem extends Model
{
    
    protected $connection = 'product';
    
    use HasFactory;
    
    protected $table = 'p_items';

    protected $guarded = [];
}
