<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Model;

class BDemandAllotmentBreakdown extends Model
{

    protected $table = "b_demand_allotment_breakdowns";
    protected $fillable = [
        'budget_allotment_id',
        'old_code',
        'new_code',
        'description',
        'fin_year',
        'range',
        'activity_type',
        'intial_budget',
        'base_bsr',
        'base_mtr',
        'base_zhr',
        'base_bbd',
        'base_pkp',
        'base_cxb',
        'base_air_hq',
        'base_201mu',
        'base_201u',
        'base_bru',
        'base_mru',
        'total_base_allotment',
        'spent_fc_army',
        'spent_base_unit',
        'spent_airhq',
        'total_spent',
        'balance'
    ];
}
