<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaseLevelBudgetTransfer extends Model
{
    
    protected $table    = 'base_level_budget_transfers';

    protected $guarded = [];
}
