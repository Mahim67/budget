@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

@php
session()->forget('excelErrors');
@endphp

<x-pondit-card title="Budget Letter ( BBD )">
    <table style="width: 100%"> 
        <tr>
            <td align="center"> CONFIDENTIAL </td>
        </tr>
        <tr> 
            <td style="width: 20%" align="right">
                <p>Bangladesh Air Force</p>  
                <p>A Unit Section</p> 
                <p>Dhaka Connt</p>  
                <p>March 21</p>
            
            </td>
        </tr>
        <tr> 
            <td>
                <h5>20.3.323.23.323......</h5>
                <h5> Fin Year 2021-2022 Date: March 21</h5>
                <p> Paper no: 232324233 Date: March 21</p>
                <p> Paper no: 232324233 Date: March 21</p>
                <p> Paper no: 232324233 Date: March 21</p>
                <p> Paper no: 232324233 Date: March 21</p>
            </td>

        </tr>
        <tr> 
            <td> 
                <table class="table" align="center">
                    <thead>
                      <tr>
                        <th scope="col">Old Code</th>
                        <th scope="col">New Code</th>
                        <th scope="col">Description</th>
                        <th scope="col">Amount</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $datam)
                        <?php $i=1?>
                        <tr>
                            <td>{{ $datam->old_code ?? null }}</td>
                            <td>{{ $datam->new_code ?? null }}</td>
                            <td>{{ $datam->description ?? null }}</td>
                            <td>{{ $datam->$userBase ?? null }}</td>
                          </tr>
                        @endforeach
                    
                      
                    
                    </tbody>
                  </table>
            </td>
        </tr>
        <tr>
            <td style="width: 30%;" align="right"> 
                <div style="margin-top: 20px !important">
                    <p>Mr X Officer</p>
                    <p>Rank</p>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <p>Some Details Here</p>
                <p>...........</p>
                <p>...........</p>
                <p>...........</p>
                <p>...........</p>
            </td>
        </tr>
        <tr>
            <td align="center"> CONFIDENTIAL </td>
        </tr>
       
    </table>
      
    <x-slot name="cardFooter">
            <div></div>
            <div></div>
            <div>
                <a href="{{ route('base-letter.baseReportPdf') }}" data-popup="tooltip" title="Base Report PDF" data-original-title="Type Wise Report PDF"><i
                    class="fas fa-file-pdf px-1 py-1 bg-brown"></i></a>
            
            </div>
    </x-slot>
</x-pondit-card>
@endsection

@push('css')
<style>
    .table {
        width: 80% !important;
        border-collapse: collapse;
        border: 1px solid black;
    }
    .table tr{
        border: 1px solid black;
    }
    .table th{
        border: 1px solid black;
    }
    .table td{
        border: 1px solid black;
    }
    
</style>
@endpush
