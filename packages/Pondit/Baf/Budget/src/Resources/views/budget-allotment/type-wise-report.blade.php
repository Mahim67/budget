@extends('pondit-limitless::layouts.master')

@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')
    <!-- Basic example -->
    <x-pondit-card title="Type Wise Budget Allotment">

        <div class="single-show">
            <div class="card-body p-0">
                <button class="btn btn-primary mb-1 btn-sm " onclick="javascrit:window.history.back()"><i
                        class="fa fa-arrow-left"></i></button>

                <table class="table table-bordered table-responsive table-sm " id="table">
                    <thead>
                        <tr class="bg-success">
                            <th>Ser No</th>
                            <th>Budget Code</th>
                            <th>Fin Year</th>
                            <th>Description</th>
                            <th>Activity Type</th>
                            <th>Initial</th>
                            @for ($i = 1; $i <= $maxSuplymentry; $i++)
                                <th>Supplementary {{ $i }}</th>
                            @endfor
                            <th> Amount </th>
                            <th>Current Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        $totalInitial = 0;
                        $totalBudgetcodemount = 0;
                        $totalCurrentBalance = 0;
                        
                        ?>
                        @forelse ($data as $activity_type => $allotments)
                            <tr>
                                <td colspan="7" class="font-weight-bold text-primary">{{ ucwords($activity_type) }}</td>
                            </tr>
                            @foreach ($allotments as $key => $allotment)
                                @php
                                    $totalInitial += $allotment->initial_amount ?? null;
                                    $totalBudgetcodemount += $allotment->total_budgetcode_amount ?? null;
                                    $totalCurrentBalance += $allotment->balance ?? null;
                                    $suplimantary = getSuplimentary($allotment->id);
                                @endphp
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $allotment->budgetcode->newcode ?? '' }}</td>
                                    <td>{{ $allotment->fin_year ?? '' }}</td>
                                    <td>{{ ucwords($allotment->description) ?? '' }}</td>
                                    <td>{{ ucwords($allotment->activity_type) ?? '' }}</td>
                                    <td class="text-right">{{ $allotment->initial_amount ?? '' }}</td>
                                    @for ($i = 0; $i < $maxSuplymentry; $i++)
                                        @if (isset($suplimantary[$i]))

                                            <td class="text-right">{{ $suplimantary[$i]->suplimentary_amount ?? '' }}
                                            </td>
                                        @else
                                            <td> &nbsp; </td>
                                        @endif
                                    @endfor
                                    <td class="text-right">{{ $allotment->total_budgetcode_amount ?? '' }}</td>
                                    <td class="text-right">{{ $allotment->balance ?? '' }}</td>
                                </tr>


                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-danger text-bold">SUB TOTAL TAKA</td>
                                <td class="text-right text-danger">{{ $allotments->sum('initial_amount') }}</td>

                                @for ($i = 0; $i < $maxSuplymentry; $i++)
                                    @if (isset($suplimantary[$i]))
                                        <td class="text-right"></td>
                                    @else
                                        <td> &nbsp; </td>
                                    @endif
                                @endfor
                                <td class="text-right text-danger">{{ $allotments->sum('total_budgetcode_amount') }}</td>
                                <td class="text-right text-danger">{{ $allotments->sum('balance') }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="7">No Data Found</td>
                            </tr>
                        @endforelse
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-danger">G.TOTAL TAKA</td>
                            <td class="text-danger text-right">{{ $totalInitial }}</td>

                            @for ($i = 0; $i < $maxSuplymentry; $i++)
                                @if (isset($suplimantary[$i]))
                                    <td class="text-right"></td>
                                @else
                                    <td> &nbsp; </td>
                                @endif
                            @endfor
                            <td class="text-right text-danger">{{ $totalBudgetcodemount }}</td>
                            <td class="text-right text-danger">{{ $totalCurrentBalance }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <x-slot name="cardFooter">
            <div></div>
            <div></div>
            <div>
                <a href="{{ route('pdf.type_wise_report_pdf') }}" data-popup="tooltip" title="Type Wise Report PDF"
                    data-original-title="Type Wise Report PDF"><i class="fas fa-file-pdf px-1 py-1 bg-brown"></i></a>
                <a href="#" id="downloadLink" data-popup="tooltip" onclick="exportF(this)" title="Type Wise Report EXCEL"
                    data-original-title="Type Wise Report EXCEL"><i class="fas fa-file-excel px-1 py-1 bg-success"></i></a>
            </div>
        </x-slot>
    </x-pondit-card>

    <!-- /basic example -->

@endsection
@push('js')
    <script>
        function exportF(elem) {
            var table = document.getElementById("table");
            var html = table.outerHTML;
            var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
            elem.setAttribute("href", url);
            elem.setAttribute("download", "Type-wise-report.xls"); // Choose the file name
            return false;
        }
    </script>
@endpush
