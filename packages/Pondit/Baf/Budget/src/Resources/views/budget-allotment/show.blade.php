@extends('pondit-limitless::layouts.master')

@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')
    <!-- Basic example -->
    <x-pondit-card title="Budget Allotment">
        <div class="single-show w-75 m-auto">
            <div class="card-body p-0">
                <ul class="nav nav-sidebar mb-2">
                    <li class="nav-item-header mt-0">Details</li>
                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{ __('Range') }} :</div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->range_name ?? '' }}</div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{ __('Budget Code') }} :
                            </div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->budgetcode->newcode ?? '' }}</div>
                        </div>
                    </li>

                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Fin Year :</div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->fin_year ?? '' }}</div>
                        </div>
                    </li>

                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Description :</div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->description ?? '' }}</div>
                        </div>
                    </li>

                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Activity Type :</div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->activity_type ?? '' }}</div>
                        </div>
                    </li>

                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Initial Amount :</div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->initial_amount ?? '' }}</div>
                        </div>
                    </li>

                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Total Suplimentary Amount :
                            </div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->total_suplimentary_amount ?? '' }}</div>
                        </div>
                    </li>

                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Total Budgetcode Amount :
                            </div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->total_budgetcode_amount ?? '' }}</div>
                        </div>
                    </li>

                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Balance :</div>
                            <div class="mt-2 mt-sm-0 ml-3">{{ $data->balance ?? '' }}</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <h4> <u> Suplimentry Budget Allotment </u></h4>
        <table class="table table-bordered">
            <thead class="bg-success">
                <tr>
                    <th>Budget Code</th>
                    <th>Description</th>
                    <th>Fin Year</th>
                    <th>Suplimentary Amount </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($suplimentries as $suplimentry)
                    <tr>
                        <td>{{ $suplimentry->budgetcode->newcode ?? '' }}</td>
                        <td>{{ $suplimentry->description ?? '' }}</td>
                        <td>{{ $suplimentry->fin_year ?? '' }}</td>
                        <td>{{ $suplimentry->suplimentary_amount ?? '' }}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>
        <x-slot name="cardFooter">
            <div></div>
            <div class="d-flex">
                <x-pondit-act-i url="{{ route('budget_allotment.index') }}" tooltip="{{ __('list') }}" />
                <x-pondit-act-c url="{{ route('budget_allotment.create') }}" tooltip="{{ __('create') }}" />
                <x-pondit-act-e url="{{ route('budget_allotment.edit', $data->id) }}" tooltip="{{ __('edit') }}" />
                <x-pondit-act-d url="{{ route('budget_allotment.delete', $data->id) }}" tooltip="{{ __('remove') }}" />
            </div>
            <div></div>
        </x-slot>
    </x-pondit-card>

    <!-- /basic example -->

@endsection
{{-- @push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}

