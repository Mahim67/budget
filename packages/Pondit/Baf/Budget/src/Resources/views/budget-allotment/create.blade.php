@extends('pondit-limitless::layouts.master')
@section('content')
    <x-pondit-card title="Budget Allotment">
        <x-pondit-form action="{{ route('budget_allotment.store') }}">

            <div class='form-group row'>
                <label class="col-sm-2 col-form-label" for="budget_type">Budgetcode</label>
                <div class='col-sm-4'>
                    <select name="budgetcode_id" id="budgetcode_id" class="form-control select-search" onchange="getRangeForBudgetCode()">
                        <option value="">--Select Budgetcode--</option>
                        @foreach ($budgetcodes as $budgetcode)
                            <option value="{{ $budgetcode->id }}">{{ $budgetcode->newcode  ?? null }} ( {{ $budgetcode->oldcode  ?? null }} )  </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class='form-group row'>
                <label class="col-sm-2 col-form-label" for="budget_type">Range</label>
                <div class='col-sm-4'>
                    <select name="range_id" id="range_id" class="form-control select-search">
                        <option value="">--Select Range--</option>
                        @foreach ($ranges as $id => $title)
                            <option value="{{ $id }}">{{ $title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class='form-group row'>
                <label for="initial_amount" class="col-sm-2 col-form-label">Initial Amount</label>
                <div class='col-sm-10'>
                    <input type="text" name="initial_amount" id="initial_amount"  class="form-control"/>
                </div>
            </div>

            <x-pondit-btn icon="check" title="{{ __('save') }}" />
            <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger"
                title="{{ __('cancel') }}" />
        </x-pondit-form>
        <x-slot name="cardFooter">
            <div></div>
            <div>
                <x-pondit-act-i url="{{ route('budget_allotment.index') }}" />
            </div>
            <div></div>
        </x-slot>
    </x-pondit-card>
@endsection

@push('js')
    <script>
        function getRangeForBudgetCode() {
            let budgetcode_id = $("#budgetcode_id").val();

            $.ajax({
                url: "{{ route('budgetcode.get_range') }}",
                method: 'GET',
                data: {
                    budgetcode_id
                },
                success: function(res) {
                    if (res.status == 'ok') {
                        let range_id = $("#range_id");
                        let html = [] ;
                        res.data.forEach(element => {
                            html += `<option value="${element.id}">${element.title}</option>`;
                        });
                        range_id.html(html);
                    }else{
                        alert(res.message);
                    }
                },
                error: function(err) {
                    console.log(err)
                },
            })
        }

    </script>
@endpush
@push('js')
    <script>
    //    var myInputs = document.querySelectorAll('#initial_amount');
    //     myInputs.forEach(function(elem) {
    //     elem.addEventListener("input", function() {
    //         var dec = elem.getAttribute('decimals');
    //         var regex = new RegExp("(\\.\\d{" + dec + "})\\d+", "g");
    //         elem.value = elem.value.replace(regex, '$1');
    //     });
    //     });

    </script>
@endpush