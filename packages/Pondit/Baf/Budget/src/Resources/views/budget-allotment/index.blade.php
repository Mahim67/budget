@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Budget Allotment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modalBody">
        <div id="budgetData"> </div>
          <table class="table table-bordered w-100 mt-3">
              <thead>
                    <th> Transfer </th>
                    <th>To Code </th>
                    <th> Transfer Date</th>
                    <th>Amount</th>

              </thead>
              <tbody id="tbody">

              </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<x-pondit-card title="Budget Allotment">
    <a href="{{ route('budget_allotment.range_wise_report') }}" class="btn bg-success" >Range Wise Report</a>
    <a href="{{ route('budget_allotment.type_wise_report') }}" class="btn bg-success" >Type Wise Report</a>
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Budget Code')}}</th>
                <th>{{__('Fin Year')}}</th>
                <th>{{__('Range')}}</th>
                <th>{{__('Initial')}}</th>
                <th>{{__('Supplementary')}}</th>
                <th>{{__('Amount')}}</th>
                <th>{{__('Current Balance')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        {{-- @dd($datam->budgetcode) --}}
       <?php $budgetcodeId = $datam->budgetcode_id?>
        <tr>
            <td>{{ ++$key }}</td>
            <td ondblclick="getData('<?php echo $budgetcodeId;?>');">{{ $datam->budgetcode->newcode ?? '' }}</td>
            <td>{{ $datam->fin_year ?? '' }}</td>
            <td>{{ $datam->range_name ?? '' }}</td>
            <td class="text-right">{{ $datam->initial_amount ?? '' }}</td>
            <td class="text-right">{{ $datam->total_suplimentary_amount ?? '' }}</td>
            <td class="text-right">{{ $datam->total_budgetcode_amount ?? '' }}</td>
            <td class="text-right">{{ $datam->balance ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budget_allotment.show', $datam->id)}}" icon="eye" bg="success" tooltip="Show"/>
                <x-pondit-act-link url="{{route('budget_allotment.edit', $datam->id)}}" icon="pen" bg="primary" tooltip="Edit"/>
                <x-pondit-act-link-d url="{{route('budget_allotment.delete', $datam->id)}}" icon="trash" tooltip="Delete"/>
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budget_allotment.create')}}" />
        </div>
        <div></div>
    </x-slot>
    
</x-pondit-card>


@endsection
@push('js')
<script>

    function getData(budgetcodeId){
        
        const url = '/getBudgetData/' + budgetcodeId;
            axios.get(url)
                .then(function (response) {
                   
                    $('#modalForm').modal('show');

                     document.getElementById("budgetData").innerHTML = `
                        <div>
                                <span class="mr-3">${response.data.budget.description ?? ''}</span>
                                <span class="mr-3"> Initial Amount : ${response.data.budget.initial_amount ?? ''}</span>
                                <span> Balance : ${response.data.budget.balance ?? ''}</span>
                        </div>


                     `;

                    document.getElementById('tbody').innerHTML = null;

                    let content = ``;
                    let i = 1;

                    $.each(response.data.transfer, function (index  ,value) {

                        content += `<tr>
                                    <td>${i++}</td>
                                    <td>${value.to_code ?? 'N/A'}</td>

                                    <td>${value.transfer_at ?? 'N/A'}</td>
                                    <td>${value.amount ?? 'N/A'}</td>
                                 </tr>
                                `

                    });

                    $('#tbody').html(content);

                })
                .catch(function (error) {
                    console.log("error")
                })
    }
    
    

</script>
    
@endpush