<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    table {
        width: 100%;
    }

    @page {
        margin: 5%;
        sheet-size: A4-L;

    }

</style>


<h4 style="text-align: center ; padding:0 ; margin: 0 ;"> RANGE WISE BUDGET ALLOCATION & EXPENDITURE FY
    {{ $FY }} </h4>
<p style="text-align: center ; color:blue ;  padding:0 ; margin: 0 ;">(Amounts in Crore)</p>
<table>
    <thead>
        <tr style="background: rgb(187, 184, 184)">
            <th>#</th>
            <th>Budget Code</th>
            <th>Fin Year</th>
            <th>Description</th>
            <th>Activity Type</th>
            <th>Initial Amount</th>
            @for ($i = 1; $i <= $maxSuplymentry; $i++)
                <th>Suplimentary {{ $i }}</th>
            @endfor
            <th>Balance</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $range => $allotmentes)
            <tr>
                <td colspan="7"><b>{{ $range }}</b></td>
            </tr>
            @foreach ($allotmentes as $key => $allotmente)
                @php
                    $suplimantary = getSuplimentary($allotmente->id);
                @endphp
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $allotmente->budgetcode->newcode ?? '' }}</td>
                    <td>{{ $allotmente->fin_year ?? ''}}</td>
                    <td>{{ $allotmente->description ?? ''}}</td>
                    <td>{{ $allotmente->activity_type ?? ''}}</td>
                    <td>{{ $allotmente->initial_amount ?? ''}}</td>
                    @for ($i = 0; $i < $maxSuplymentry; $i++)
                        @if (isset($suplimantary[$i])) <td>{{ $suplimantary[$i]->suplimentary_amount ?? '' }}</td>
                    @else
                        <td> &nbsp; </td> @endif
                    @endfor
                    <td>{{ $allotmente->balance ?? ''}}</td>
                </tr>

            @endforeach
        @endforeach
    </tbody>
</table>
