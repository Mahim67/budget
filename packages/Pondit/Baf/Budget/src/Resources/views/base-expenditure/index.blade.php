@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

@php
session()->forget('excelErrors');
@endphp

<x-pondit-card title="Base : BSR ">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('Ser No')}}</th>
                <th>{{__('Old Code')}}</th>
                <th>{{__('New Code')}}</th>
                <th>{{__('Description')}}</th>
                <th class="text-right">{{__('Amount')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
            <td>{{ ++$key }}</td>
            <td>{{ $datam->old_code ?? null  }}</td>
            <td>{{ $datam->new_code ?? null}}</td>
            <td>{{ $datam->description ?? null }}</td>
            <td class="text-right">{{ $datam->$userBase ?? null }}</td>
     
            <td class="d-flex">
                
                <x-pondit-act-link url="{{route('base-expenditure.spent', $datam->budget_allotment_id)}}"  icon="search-dollar" bg="success" />
                
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
      
    <x-slot name="cardFooter">
            <div></div>
            <div></div>
            <div>
                <a href="" data-popup="tooltip" title="Base Report PDF" data-original-title="Type Wise Report PDF"><i
                    class="fas fa-file-pdf px-1 py-1 bg-brown"></i></a>
            
            </div>
    </x-slot>
</x-pondit-card>
@endsection

@push('css')

@endpush
