@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<?php $base_name = Session::get('base_name') ?>
@php
session()->forget('excelErrors');
@endphp

<x-pondit-card title="Expenditure (  BSR  )">
    <div class="row">
        <div class="col-3">
            <x-pondit-card title="Expenditure">
                    <x-pondit-form action="{{ route('base-expenditure.store') }}">
                    <input type="hidden" name="budget_allotment_id" value="{{$budgetAllotment->budget_allotment_id}}">
                    <div class='form-group'>
                        <div class='form-group'>
                            <label for="contract_no" class="col-form-label ">Contract No</label>
                            <input type="text" name="contract_no" id="contract_no" class="form-control" />
                        </div>

                        <label for="spent" class="col-form-label ">Spent</label>
                        <input type="text" name="spent" id="spent" class="form-control" />
                    </div>
                
                    <div class='form-group'>
                        <label for="remarks" class="col-form-label ">Remarks</label>
                        <input type="text" name="remarks" id="remark" class="form-control" />
                    </div>

                    <div class='form-group mt-3'>
                        <x-pondit-btn icon="check" title="{{ __('save') }}" />
                        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger"
                            title="{{ __('cancel') }}" />
                    </div>
                </x-pondit-form>
            </x-pondit-card>

</div> 


        <div class="col-9">
            <x-pondit-card title="Expenditure :  {{$budgetAllotment->new_code }} ( {{ $budgetAllotment->old_code}}) - {{ $budgetAllotment->description }}">
                <x-pondit-datatable >
                    <x-slot name="thead">
                        <tr>
                            <th>{{__('Ser No')}}</th>
                            <th>{{__('Budget Amount')}}</th>
                            <th>{{__('Spent')}}</th>
                            <th>{{__('Remarks')}}</th>
                        </tr>
                    </x-slot>
                    @foreach ($data as $key=>$datam)
                        <td>{{ ++$key }}</td>
                        <td>{{ $budgetAllotment->$base_name ?? null  }}</td>
                        <td>{{ $datam->spent ?? null}}</td>
                        <td>{{ $datam->remarks ?? null}}</td>
                    </tr>
                    @endforeach
                </x-pondit-datatable>
            </x-pondit-card>
        </div>
    </div>
   
   
      
    <x-slot name="cardFooter">
            <div></div>
            <div>

            </div>
            <div>
           </div>
    </x-slot>
</x-pondit-card>
@endsection

@push('css')

@endpush
