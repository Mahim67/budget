@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Budget Allotment Brackdown">
    <x-pondit-form action="{{ route('budgetcode_to_item.update', $data->id) }}">
        @method('PUT')
        
        <div class='form-group row'>
            <label for="budgetcode_id" class="col-sm-2 col-form-label">Budget Code</label>
            <div class='col-sm-4'>
                <select name="budgetcode_id" id="budgetcode_id" class="form-control">
                    <option value="">-- Select Badget Code --</option>
                    @foreach ($budgetcodes as  $budgetcode)
                        @if ($data->budgetcode_id == $budgetcode->id )
                            <option value="{{ $budgetcode->id }}" selected >Old Code : {{ $budgetcode->oldcode }} || New Code : {{ $budgetcode->newcode }}</option>    
                        @else
                            <option value="{{ $budgetcode->id }}">Old Code : {{ $budgetcode->oldcode }} || New Code : {{ $budgetcode->newcode }}</option>    
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class='form-group row'>
            <label for="range_id" class="col-sm-2 col-form-label">Range</label>
            <div class='col-sm-4'>
                <select name="range_id" id="range_id" class="form-control">
                    <option value="">-- Select Range --</option>
                    @foreach ($ranges as $id => $range)
                        @if ($data->range_id == $id)
                            <option value="{{ $id }}" selected >{{ $range }}</option>    
                        @else
                            <option value="{{ $id }}">{{ $range }}</option>    
                        @endif
                    @endforeach
                </select>
            </div>
        </div>


        <div class='form-group row'>
            <label for="item_name" class="col-sm-2 col-form-label"> Item </label>
            <div class='col-sm-10'>
                <input type="text" name="item_name" id="item_name" value="{{ $data->item_name }}" class="form-control" />
            </div>
        </div>

        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budgetcode_to_item.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budgetcode_to_item.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('budgetcode_to_item.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('budgetcode_to_item.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection