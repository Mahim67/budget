@extends('pondit-limitless::layouts.master')


@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Allotment Brackdown">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Budget Code')}}</th>
                <th>{{__('Range')}}</th>
                <th>{{__('Item')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td> <b>Old Code : </b> {{ $datam->budgetcode->oldcode ?? '' }}  || <b>New Code</b> {{ $datam->budgetcode->newcode ?? '' }}</td>
            <td>{{ $datam->range_name }}</td>
            <td>{{ $datam->item_name }}</td>
            <td class="d-flex">
                {{-- <x-pondit-act-link url="{{route('budgetcode_to_item.show', $datam->id)}}" icon="eye" bg="success" /> --}}
                <x-pondit-act-link url="{{route('budgetcode_to_item.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('budgetcode_to_item.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budgetcode_to_item.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
