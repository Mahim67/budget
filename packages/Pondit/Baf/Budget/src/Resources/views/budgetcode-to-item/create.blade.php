@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Budget Transfer">
<x-pondit-form action="{{route('budgetcode_to_item.store')}}" >
        
        <div class='form-group row'>
            <label for="budgetcode_id" class="col-sm-2 col-form-label">Budget Code</label>
            <div class='col-sm-4'>
                <select name="budgetcode_id" id="budgetcode_id" class="form-control select-search">
                    <option value="">-- Select Badget Code --</option>
                    @foreach ($budgetcodes as  $budgetcode)
                        <option value="{{ $budgetcode->id }}">Old Code : {{ $budgetcode->oldcode }} || New Code : {{ $budgetcode->newcode }}</option>    
                    @endforeach
                </select>
            </div>
        </div>

        <div class='form-group row'>
            <label for="range_id" class="col-sm-2 col-form-label">Range</label>
            <div class='col-sm-4'>
                <select name="range_id" id="range_id" class="form-control">
                    <option value="">-- Select Range --</option>
                    @foreach ($ranges as $id => $range)
                        <option value="{{ $id }}">{{ $range }}</option>    
                    @endforeach
                </select>
            </div>
        </div>


        <div class='form-group row'>
            <label for="item_name" class="col-sm-2 col-form-label"> Item </label>
            <div class='col-sm-10'>
                <input type="text" name="item_name" id="item_name" class="form-control" />
            </div>
        </div>


        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('budgetcode_to_item.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
