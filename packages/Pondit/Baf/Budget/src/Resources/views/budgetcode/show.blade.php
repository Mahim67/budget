@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="Budgetcode">

    <div class="single-show w-75 m-auto">
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('Range')}} :</div>
                        <ul>
                            @foreach ($data->range as $range)
                                <li class="mt-2 mt-sm-0 ml-3">{{$range->title ?? ''}}</li>
                            @endforeach
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('New Code')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->newcode ?? ''}}</div>
                    </div>
                </li>
               
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('Old Code')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->oldcode ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Description :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->description ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Activity Type :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->budget_type ?? ''}}</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budgetcode.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budgetcode.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-e url="{{route('budgetcode.edit', $data->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('budgetcode.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>

<!-- /basic example -->

@endsection
{{-- 
@push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}