@extends('pondit-limitless::layouts.master')
@section('content')
    <x-pondit-card title="Budget Code">
        <x-pondit-form action="{{ route('budgetcode.store') }}">

            <div class='form-group row'>
                <label for="oldcode" class="col-sm-2 col-form-label">Old Code</label>
                <div class='col-sm-10'>
                    <input type="text" name="oldcode" id="oldcode" class="form-control" />
                </div>
            </div>

            <div class='form-group row'>
                <label for="newcode" class="col-sm-2 col-form-label">New Code</label>
                <div class='col-sm-10'>
                    <input type="text" name="newcode" id="newcode" class="form-control" />
                </div>
            </div>

            <div class='form-group row'>
                <label class="col-sm-2 col-form-label" for="description">Description</label>
                <div class='col-sm-10'>
                    <textarea name="description" id="description" cols="400" rows="3" class="form-control"></textarea>
                </div>
            </div>
            <div class='form-group row'>
                <label class="col-sm-2 col-form-label" for="budget_type">Activity Type</label>
                <div class='col-sm-4'>
                    <select name="budget_type" id="budget_type" class="form-control">
                        <option value="">--Select Budget Type--</option>
                        @foreach (Pondit\Baf\Budget\Models\Budgetcode::$budget_type as $value => $item)
                            <option value="{{ $value }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class='form-group row'>
                <label class="col-sm-2 col-form-label" >Range</label>
                <div class='col-sm-4 mt-2'>
                    @foreach ($ranges as $id => $range)
                        <input type="checkbox" name="range[]" value="{{ $id }}"> {{ $range }} &nbsp;&nbsp;&nbsp;
                    @endforeach
                </div>
            </div>
            
            {{-- <div class='form-group row'>
                <label class="col-sm-2 col-form-label" >Is Duplicate</label>
                <input type="checkbox" name="is_duplicate" id="is_duplicate" value="1" class="mt-2" /> &nbsp;&nbsp; <span class="mt-2">YES</span>
            </div> --}}


            <x-pondit-btn icon="check" title="{{ __('save') }}" />
            <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger"
                title="{{ __('cancel') }}" />
        </x-pondit-form>
        <x-slot name="cardFooter">
            <div></div>
            <div>
                <x-pondit-act-i url="{{ route('budgetcode.index') }}" />
            </div>
            <div></div>
        </x-slot>
    </x-pondit-card>
@endsection
