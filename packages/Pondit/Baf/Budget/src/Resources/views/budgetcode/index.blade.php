@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Code">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('Ser No')}}</th>
                <th>{{__('Old Code')}}</th>
                <th>{{__('New Code')}}</th>
                <th>{{__('Range')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('Activity Type')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td class="text-center">{{ ++$key }}</td>
            <td>{{ $datam->oldcode }}</td>
            <td>{{ $datam->newcode }}</td>
            <td>
                @foreach ($datam->range as $range)
                 <li class="text-success font-weight-bold">{{ ucwords($range->title) ?? ''}}</li>
                @endforeach
            </td>

            <td>{{ ucwords($datam->description) ?? '' }}</td>
            <td>{{ ucwords($datam->budget_type) ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budgetcode.show', $datam->id)}}" icon="eye" bg="success" tooltip="Show"/>
                <x-pondit-act-link url="{{route('budgetcode.edit', $datam->id)}}" icon="pen" bg="primary" tooltip="Edit"/>
                <x-pondit-act-link-d url="{{route('budgetcode.delete', $datam->id)}}" icon="trash" tooltip="Delete" />

            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budgetcode.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
