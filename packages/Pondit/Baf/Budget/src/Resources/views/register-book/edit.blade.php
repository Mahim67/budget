@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Budget Breakdown  in Base & Unit">
    <x-pondit-form action="{{ route('allotment_brackdown.update', $data->id) }}">
        @method('PUT')
        
             
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label" for="budget_type">Budgetcode</label>
            <div class='col-sm-4'>
                <select name="budget_allotment_id" id="budgetcode_id" class="form-control select-search" onchange="getRangeForBudgetCode()">
                    <option value="">--Select Budgetcode--</option>
                    @foreach ($budgetcodes as $budgetcode)
                        {{-- <option value="{{ $budgetcode->id }}">{{ $budgetcode->budgetcode->newcode ?? null}} ( {{ $budgetcode->budgetcode->oldcode ?? null }} ) - {{ rangeName($budgetcode->range_id)}} </option> --}}
                        @if ($budgetcode->id == $data->budget_allotment_id)
                        <option value="{{ $budgetcode->id }}" selected>{{ $budgetcode->new_code  ?? null }} ( {{ $budgetcode->old_code  ?? null }} ) - {{ rangeName($budgetcode->range_id)}}  </option>
                        @else
                        <option value="{{ $budgetcode->id }}">{{ $budgetcode->new_code  ?? null }} ( {{ $budgetcode->old_code  ?? null }} ) - {{ rangeName($budgetcode->range_id)}}  </option>

                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    
       


        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('allotment_brackdown.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('allotment_brackdown.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('allotment_brackdown.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('allotment_brackdown.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
@push('css')
<style>
    label{
        font-weight: bold
    }
</style>
    
@endpush