@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Register Book">
<x-pondit-form action="{{ route('register-book.store')}}">

    <div class="row"> 
            <div class="col-4">
                <div class='form-group'>
                    <label for="budget_code" class="col-form-label ">Budget Code</label>
                    <select name="budget_code" id="budget_code" class="form-control select-search">
                        <option value="">--Select Budgetcode--</option>
                        @foreach ($budgetcodes as $budgetcode)
                            <option value="{{ $budgetcode->id }}">{{ $budgetcode->newcode ?? null}} ( {{ $budgetcode->oldcode ?? null }} )</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class='form-group'>
                    <label class="col-form-label" >Range</label><br>
                    @foreach ($ranges as $id => $range)
                        <input type="checkbox" name="range[]" value="{{ $id }}"> {{ $range }} &nbsp;&nbsp;&nbsp;
                    @endforeach
                </div>
            </div>
            <div class="col-4">
                <div class='form-group'>
                    <label for="contract_no" class="col-form-label ">Contract No</label>
                    <input type="text" name="contract_no" id="contract_no" class="form-control" />
                </div>
            </div>
    </div>

    <div class="row">
        <div class="col-4">
            <div class='form-group'>
                <label for="description" class="col-form-label ">Description</label>
                <input type="text" name="description" id="description" class="form-control" />
            </div>
        </div>
        <div class="col-4">
            <div class='form-group'>
                <label for="date" class="col-form-label ">Date</label>
                <input type="date" name="date" id="date" class="form-control" />
            </div>
        </div>
        <div class="col-4">
            <div class='form-group'>
                <label for="foriegn_currency" class="col-form-label ">Foriegn Currency</label>
                <input type="text" name="foriegn_currency" id="foriegn_currency" class="form-control" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4">
            <div class='form-group'>
                <label for="local_currency" class="col-form-label ">Local Currency</label>
                <input type="text" name="local_currency" id="local_currency" class="form-control" />
            </div>
        </div>
        <div class="col-4">
            <div class='form-group'>
                <label for="insurance_charge" class="col-form-label ">Insurance Charge</label>
                <input type="text" name="insurance_charge" id="insurance_charge" class="form-control" />
            </div>
        </div>
        <div class="col-4">
            <div class='form-group'>
                <label for="date_of_delivery" class="col-form-label ">Date of Delivery</label>
                <input type="date" name="date_of_delivery" id="date_of_delivery" class="form-control" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4">
            <div class='form-group'>
                <label for="name_of_firm" class="col-form-label ">Name of Firm</label>
                <input type="name_of_firm" name="name_of_firm" id="name_of_firm" class="form-control" />
            </div>
        </div>
        <div class="col-8">
            <div class='form-group'>
                <label for="remarks" class="col-form-label ">Remarks</label>
                <textarea type="remarks" name="remarks" id="remarks" class="form-control" rows="10" cols="4"> </textarea>
            </div>
        </div>
    </div>

 
    <x-pondit-btn icon="check" title="{{ __('save') }}" />

</x-pondit-form>
</x-pondit-card>

@endsection


@push('js')


    
@endpush