@extends('pondit-limitless::layouts.master')

@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Register Book">
    <div class="row">
        <div class="col-md-2">
            <button class="btn btn-primary mb-1 btn-sm " onclick="javascrit:window.history.back()"><i
                class="fa fa-arrow-left"></i>
            </button>
        </div>
        <div class="col-md-8">
            <form  method="GET">
            <div class='form-group row'>
                    <label class="col-sm-2 col-form-label" for="fin_year">Register Book</label>
                    <div class='col-sm-4'>
                        <select name="fin_year" id="fin_year" class="form-control select-search">
                            <option value="">--Select Fin Year--</option>
                            <option value="2020-2021">( 2020-2021 )</option>
                            <option value="2021-2022">( 2021-2022 )</option>
                            <option value="2022-2023">( 2022-2023 )</option>
                        </select>
                    </div>
            <button class="btn btn-sm bg-success" type="submit">Search</button>
            
            </div>
        </form>
        </div>
    </div>
    
    
    <table class="table table-bordered table-responsive table-sm" id="table">
        <thead class="bg-success">
            <tr>
                <tr>
                    <th>{{__('Ser No')}}</th>
                    <th>{{__('Contract No')}}</th>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Description')}}</th>
                    <th>{{__('Qty')}}</th>
                    <th>{{__('Foreign Currency')}}</th>
                    <th>{{__('Lcoal Currency')}}</th>
                    <th>{{__('Insurance Charge')}}</th>
                    <th>{{__('Date of Delivery')}}</th>
                    <th>{{__('Name of the Firm')}}</th>
                    <th>{{__('Remarks')}}</th>
                   
                </tr>
           </thead>
        <tbody id="tbody">
            @forelse ($data as $budgetCode => $datas)
            <tr>
                <td colspan="7" class="font-weight-bold text-primary">( {{ nameBudgetCode($budgetCode)  }} )
                    <?php $arr= [] ?>
                    @foreach ($datas as $key => $datam)
                        @foreach ($datam->range as  $range)
                            <?php 
                                array_push($arr, $range->title);
                            ?>
                        @endforeach
                    @endforeach

                    @foreach (array_unique($arr) as $rangeName)
                        <span class="badge bg-success">{{$rangeName}}</span>
                    @endforeach
                </td> 
            </tr>

            @foreach ($datas as $key => $datam)
               
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{$datam->contract_no ?? null }}</td>
                <td>{{$datam->date ?? null }}</td>
                <td>{{$datam->description ?? null }}</td>
                <td>{{$datam->qty ?? null }}</td>
                <td>{{$datam->foreign_currency ?? null }}</td>
                <td>{{$datam->local_currency ?? null }}</td>
                <td>{{$datam->insurance_charge ?? null }}</td>
                <td>{{$datam->date_of_delivery ?? null }}</td>
                <td>{{$datam->name_of_firm ?? null }}</td>
                <td>{!!$datam->remarks ?? null !!}</td>
            </tr>

            @endforeach
        @empty
        <tr>
            <td class="text-center" colspan="7">No Data Found</td>
        </tr>
        @endforelse
    </tbody>
    </table>
  

    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('register-book.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

@push('js')

    <script> 
        $( document ).ready(function() {
            
            
        });
    
    </script>
@endpush
