<table>

    <thead>
        <tr>
            <th style="font-weight: bold">Ser No</th>
            <th style="font-weight: bold">Contract No</th>
            <th style="font-weight: bold">Budget Code</th>
            <th style="font-weight: bold">Acceptance No</th>
            <th style="font-weight: bold">Spent</th>
            <th style="font-weight: bold">Remaining</th>
            
        </tr>
       
    </thead>
    <tbody>
        <?php $i = 1 ?>
        @foreach ($data as $datam)
            <tr>
                <td>{{ $i++}}</td>
                <td>{{ $datam->contract_no ?? null }}</td>
                <td>{{ $datam->new_code ?? null }} ( {{$datam->old_code ?? null }} )</td>
                <td class="text-right">{{ $datam->acceptance_amount ?? null }}</td>
                <td  class="text-right">{{ $datam->spent_by_bank ?? null }}</td>
                <td  class="text-right">{{ ($datam->acceptance_amount - $datam->spent_by_bank ) }}</td> 
            </tr>
        @endforeach
       
        
    </tbody>
</table>
