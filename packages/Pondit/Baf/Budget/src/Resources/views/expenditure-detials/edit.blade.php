@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Budget Expenditure">
    <x-pondit-form action="{{ route('expenditure_detials.update', $expenditureDetial->id) }}">
        @method('PUT')
        {{-- <div class='form-group row'>
            <label for="dgdp" class="col-sm-2 col-form-label"> <b> DGDP </b></label>
            <div class='col-sm-10'>
                <input type="text" name="dgdp" id="dgdp" value="{{ $expenditureDetial->dgdp ?? '' }}" class="form-control" />
            </div>
        </div> --}}
        <div class="row">
            <div class="col-6">
                <div class='form-group row'>
                    <label for="acceptance_no" class="col-sm-4 col-form-label"> <b> Acceptance No </b> </label>
                    <div class='col-sm-8'>
                        <select name="acceptance_no" id="acceptance_no" class="form-control select-search">
                            <option value="">--Select Acceptance No--</option>
                            
                            <option value="276.138.19" @if ($expenditureDetial->acceptance_no == '276.138.19') selected @endif >276.138.19</option>
                            <option value="278.031.19" @if ($expenditureDetial->acceptance_no == '278.031.19') selected @endif >278.031.19</option>
                            <option value="274.091.19" @if ($expenditureDetial->acceptance_no == '274.091.19') selected @endif >274.091.19</option>
                            <option value="275.208.19" @if ($expenditureDetial->acceptance_no == '275.208.19') selected @endif >275.208.19</option>
        
                            {{-- @foreach ($budgetAllotmentBrackdons as $id => $description)
                                <option value="{{ $id }}">{{ $description }}</option>    
                            @endforeach --}}
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class='form-group row'>
                    <label for="contract_no" class="col-sm-4 col-form-label"> <b> Contract No </b></label>
                    <div class='col-sm-8'>
                        <select name="contract_no" id="contract_no" class="form-control select-search">
                            <option value="">--Select Contract No--</option>
                            <option value="278.030.19" @if ($expenditureDetial->contract_no == '278.030.19') selected @endif>278.030.19</option>
                            <option value="278.032.19"  @if ($expenditureDetial->contract_no == '278.032.19') selected @endif>278.032.19</option>
                            <option value="276.220.15.01" @if ($expenditureDetial->contract_no == '276.220.15.01') selected @endif>276.220.15.01</option>
                            <option value="276.001.18" @if ($expenditureDetial->contract_no == '276.001.18') selected @endif>276.001.18</option>
        
                            {{-- @foreach ($budgetAllotmentBrackdons as $id => $description)
                                <option value="{{ $id }}">{{ $description }}</option>    
                            @endforeach --}}
                        </select>
                    </div>
                </div>
            </div>
            
    
            
        </div>



        <div class='form-group row'>
            <label for="type_of_acc" class="col-sm-2 col-form-label"> <b> Type Of Equipment </b></label>
            <div class='col-sm-10'>
                <input type="text" name="type_of_acc" id="type_of_acc" value="{{ $expenditureDetial->type_of_acc ?? '' }}" class="form-control" />
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class='form-group row'>
                    <label for="old_code" class="col-sm-4 col-form-label"> <b> Old Code </b></label>
                    <div class='col-sm-8'>
                        <select name="old_code" id="old_code" class="form-control select-search">
                            <option value="">--Select Old Code--</option>
                            @foreach ($budgetCodes as $id => $budgetCode)
                                @if ($expenditureDetial->old_code == $budgetCode->oldcode)
                                    <option value="{{ $budgetCode->oldcode }}" selected>{{ $budgetCode->oldcode }}</option>    
                                @else
                                    <option value="{{ $budgetCode->oldcode }}">{{ $budgetCode->oldcode }}</option>    
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class='form-group row'>
                    <label for="new_code" class="col-sm-4 col-form-label" > <b> New Code </b> </label>
                    <div class='col-sm-8'>
                        <select name="new_code" id="new_code" class="form-control select-search">
                            <option value="">--Select New Code--</option>
                            @foreach ($budgetCodes as $id => $budgetCode)
                                @if ($expenditureDetial->new_code == $budgetCode->newcode)
                                    <option value="{{ $budgetCode->newcode }}" selected>{{ $budgetCode->newcode }}</option>    
                                @else
                                    <option value="{{ $budgetCode->newcode }}">{{ $budgetCode->newcode }}</option>    
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>




        <div class='form-group row'>
            <label for="file_rep_no" class="col-sm-2 col-form-label"> <b> File Ref No </b></label>
            <div class='col-sm-10'>
                <input type="text" name="file_rep_no" id="file_rep_no" value="{{ $expenditureDetial->file_rep_no ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="date" class="col-sm-2 col-form-label"> <b> Date </b></label>
            <div class='col-sm-10'>
                <input type="date" name="date" id="date" value="{{ $expenditureDetial->date ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="date" class="col-sm-2 col-form-label"> <b> description </b></label>
            <div class='col-sm-10'>
                <textarea name="description" id="description"  class="form-control"  rows="3">{{ $expenditureDetial->description ?? '' }}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class='form-group row'>
                    <label for="dte" class="col-sm-4 col-form-label"> <b> DTE </b></label>
                    <div class='col-sm-8'>
                        <select name="dte" id="dte" class="form-control select-search">
                            <option value="">--Select New Code--</option>
                            @foreach ($ranges as $id => $range)
                                @if ($expenditureDetial->dte == $range)
                                    <option value="{{ $range }}" selected>{{ $range }}</option>    
                                @else
                                    <option value="{{ $range }}">{{ $range }}</option>    
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class='form-group row'>
                    <label for="range" class="col-sm-4 col-form-label"><b> Range</b></label>
                    <div class='col-sm-8'>
                        <select name="range" id="range" class="form-control select-search">
                            <option value="">--Select New Code--</option>
                            @foreach ($ranges as $id => $range)
                                @if ($expenditureDetial->dte == $range)
                                    <option value="{{ $range }}" selected>{{ $range }}</option>    
                                @else
                                    <option value="{{ $range }}">{{ $range }}</option>    
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        

        <div class='form-group row'>
            <label for="qty" class="col-sm-2 col-form-label"><b> Qty</b></label>
            <div class='col-sm-10'>
                <input type="text" name="qty" id="qty" value="{{ $expenditureDetial->qty ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="spent_by_airhq" class="col-sm-2 col-form-label"><b> Spent By Airhq</b></label>
            <div class='col-sm-10'>
                <input type="text" name="spent_by_airhq" id="spent_by_airhq" value="{{ $expenditureDetial->spent_by_airhq ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="spent_by_base" class="col-sm-2 col-form-label"><b> Spent By Base</b></label>
            <div class='col-sm-10'>
                <input type="text" name="spent_by_base" id="spent_by_base" value="{{ $expenditureDetial->spent_by_base ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="status" class="col-sm-2 col-form-label"><b> Status</b></label>
            <div class='col-sm-10'>
                <input type="text" name="status" id="status" value="{{ $expenditureDetial->status ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="status" class="col-sm-2 col-form-label"><b> Currency</b></label>
            <div class='col-sm-4'>
                <x-pondit-currency name="currency" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="print_code" class="col-sm-2 col-form-label"><b> Print Code</b></label>
            <div class='col-sm-10'>
                <input type="text" name="print_code" id="print_code" value="{{ $expenditureDetial->print_code ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="lc_commission" class="col-sm-2 col-form-label"><b> LC Commission </b></label>
            <div class='col-sm-10'>
                <input type="text" name="lc_commission" id="lc_commission" value="{{ $expenditureDetial->lc_commission ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="collection_charge" class="col-sm-2 col-form-label"><b> Collection Charge </b></label>
            <div class='col-sm-10'>
                <input type="text" name="collection_charge" id="collection_charge " value="{{ $expenditureDetial->collection_charge ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="swift_charge" class="col-sm-2 col-form-label"><b> Swift Charge </b></label>
            <div class='col-sm-10'>
                <input type="text" name="swift_charge" id="swift_charge" value="{{ $expenditureDetial->swift_charge ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="agency_comm" class="col-sm-2 col-form-label"><b> Agency Comm</b></label>
            <div class='col-sm-10'>
                <input type="text" name="agency_comm" id="agency_comm" value="{{ $expenditureDetial->agency_comm ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="country" class="col-sm-2 col-form-label"><b>Country</b></label>
            <div class='col-sm-10'>
                <input type="text" name="country" id="country" value="{{ $expenditureDetial->country ?? '' }}" class="form-control" />
            </div>
        </div>


        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('expenditure_detials.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('expenditure_detials.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('expenditure_detials.show', $expenditureDetial->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('expenditure_detials.delete', $expenditureDetial->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection