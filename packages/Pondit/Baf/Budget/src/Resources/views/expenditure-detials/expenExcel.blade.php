<table>

    <thead>
        <tr>
            <th style="font-weight: bold">Ser No</th>
            <th style="font-weight: bold">Acceptance No</th>
            <th style="font-weight: bold">Contract No</th>
            <th style="font-weight: bold">Type of Eqp</th>
            <th style="font-weight: bold">New Code</th>
            <th style="font-weight: bold">Old Code</th>
            <th style="font-weight: bold">File Ref No</th>
            <th style="font-weight: bold">Date</th>
            <th style="font-weight: bold">Description</th>
            <th style="font-weight: bold">Qty</th>
            <th style="font-weight: bold">Acceptance</th>
            <th style="font-weight: bold">Spent By AirHQ</th>
            <th style="font-weight: bold">Spent By Bank</th>
            <th style="font-weight: bold">Status</th>
            <th style="font-weight: bold">Dte</th>
            <th style="font-weight: bold">Currency</th>
            <th style="font-weight: bold">Print Code</th>
            <th style="font-weight: bold">Spl Code</th>
            <th style="font-weight: bold">Fin Year</th>
            <th style="font-weight: bold">LC Commission</th>
            <th style="font-weight: bold">Collection  Charge</th>
            <th style="font-weight: bold">Swift Charge</th>
            <th style="font-weight: bold">Agency Commission</th>
            <th style="font-weight: bold">Name of Firm</th>
            <th style="font-weight: bold">Country</th>
        </tr>
       
    </thead>
    <tbody>
        <?php $i = 1 ?>
        @foreach ($data as $datam)
            <tr>
                <td>{{ $i++}}</td>
                <td>{{ $datam->acceptance_no ?? null }}</td>
                <td>{{ $datam->contract_no ?? null }}</td>
                <td>{{ $datam->type_of_acc ?? null }}</td>
                <td>{{ $datam->new_code ?? null }}</td>
                <td>{{ $datam->old_code ?? null }}</td>
                <td>{{ $datam->file_rep_no ?? null }}</td>
                <td>{{ $datam->date ?? null }}</td>
                <td>{{ $datam->description ?? null }}</td>
                <td>{{ $datam->qty ?? null }}</td>
                <td>{{ $datam->acceptance_amount ?? null }}</td>
                <td>{{ $datam->spent_by_airhq ?? null }}</td>
                <td>{{ $datam->spent_by_bank ?? null }}</td>
                <td>{{ $datam->status ?? null }}</td>
                <td>{{ $datam->dte ?? null }}</td>
                <td>{{ $datam->currency ?? null }}</td>
                <td>{{ $datam->print_code ?? null }}</td>
                <td>{{ $datam->spl_code ?? null }}</td>
                <td>{{ $datam->fin_year ?? null }}</td>
                <td>{{ $datam->lc_commission ?? null }}</td>
                <td>{{ $datam->collection_charge ?? null }}</td>
                <td>{{ $datam->swift_charge ?? null }}</td>
                <td>{{ $datam->agency_comm ?? null }}</td>
                <td>{{ $datam->company_name ?? null }}</td>
                <td>{{ $datam->country ?? null }}</td>
            </tr>
        @endforeach
       
        
    </tbody>
</table>
