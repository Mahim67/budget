@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Budget Expenditure">
    <button class="btn btn-primary mb-1 btn-sm " onclick="javascrit:window.history.back()"><i
        class="fa fa-arrow-left"></i>
    </button>
    <table class="table table-bordered">
        <thead>
          <tr class="bg-success">
            <th scope="col">Ser No</th>
            <th scope="col">Contract No</th>
            <th scope="col">Budget Code</th>
            <th class="text-right" scope="col">Acceptance</th>
            <th class="text-right" scope="col">Spent</th>
            <th class="text-right" scope="col">Remaining</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($data as  $datam)

            <tr>
                <th scope="row">1</th>
                <td>{{ $datam->contract_no ?? null }}</td>
                <td>{{ $datam->new_code ?? null }} ( {{ $datam->old_code ?? null }})</td>
                <td class="text-right">{{ $datam->acceptance_amount ?? null }}</td>
                <td class="text-right">{{ $datam->spent_by_bank ?? null }}</td>
                <td class="text-right">{{ ($datam->acceptance_amount - $datam->spent_by_bank) ?? null  }}</td>
              </tr>
                
            @endforeach
        </tbody>
      </table>
    
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('expenditure_detials.index')}}" />
        </div>
        <div>
          <a href="{{route('expenditure_detials.balancesheet_excel')}}" data-popup="tooltip" title="EXCEL"
          data-original-title="Range Wise Report"><i class="fas fa-file-excel px-1 py-1 bg-success"></i></a>
        </div>
    </x-slot>
</x-pondit-card>
@endsection

@push('js')
<script>
    
</script>
    
@endpush
