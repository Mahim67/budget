@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Expenditure">

    <div class="single-show w-75 m-auto">
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> DGDP :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$expenditureDetial->dgdp ?? ''}}</div>
                    </div>  
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Acceptance No:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$expenditureDetial->acceptance_no ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Contract No :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->contract_no ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> type Of Acc:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->type_of_acc ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Old Code:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->old_code ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> New Code:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->new_code ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> File Rep No:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->file_rep_no ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Date:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->date ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> DTE:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->dte ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Range:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->range ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Qty:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->qty ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Spent By Airhq:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->spent_by_airhq ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Spent By Base:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->spent_by_base ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Status:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->status ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Currency:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->currency ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Print Code:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->print_code ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Fin Year:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->fin_year ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> LC Commission:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->lc_commission ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Collection Charge:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->collection_charge ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Swift Charge:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->swift_charge ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Agency Comm:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->agency_comm ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Country:</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $expenditureDetial->country ?? '' }}</div>
                    </div>
                </li>


            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('expenditure_detials.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('expenditure_detials.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-e url="{{route('expenditure_detials.edit', $expenditureDetial->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('expenditure_detials.delete', $expenditureDetial->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>


@endsection