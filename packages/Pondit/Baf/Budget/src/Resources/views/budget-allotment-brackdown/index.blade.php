@extends('pondit-limitless::layouts.master')


@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Allotment Breakdown">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Amount')}}</th>
                <th>{{__('Alloted To')}}</th>
                <th>{{__('Reference No')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->amount }}</td>
            <td>{{ $datam->alloted_to }}</td>
            <td>{{ $datam->reference_no ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budget_allotment_brackdown.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('budget_allotment_brackdown.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('budget_allotment_brackdown.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budget_allotment_brackdown.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

