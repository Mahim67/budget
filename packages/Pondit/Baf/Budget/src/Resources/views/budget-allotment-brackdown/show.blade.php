@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Allotment Breakdown All Bases">

    <div class="single-show w-75 m-auto">
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Amount :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->amount ?? ''}}</div>
                    </div>  
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Alloted To :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->alloted_to ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Reference No :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $data->reference_no ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Letter :</div>
                        <div class="mt-2 mt-sm-0 ml-3"><img src="{{ asset('') . $data->letter }}" alt="" height="200" width="200"></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budget_allotment_brackdown.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budget_allotment_brackdown.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-e url="{{route('budget_allotment_brackdown.edit', $data->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('budget_allotment_brackdown.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>


@endsection