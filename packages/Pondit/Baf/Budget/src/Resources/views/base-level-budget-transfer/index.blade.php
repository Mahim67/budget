@extends('pondit-limitless::layouts.master')


@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
@php
    $object = new Pondit\Baf\Budget\Http\Controllers\BudgetTransferController();
@endphp

<x-pondit-card title="Budget Transfer">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('From Code')}}</th>
                <th>{{__('To Code')}}</th>
                <th>{{__('Fin Year')}}</th>
                <th>{{__('Amount')}}</th>
                <th>{{__('Reference No')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $object->getBudgetCode($datam->from_code)->newcode ?? '' }}</td>
            <td>{{ $object->getBudgetCode($datam->to_code)->newcode ?? '' }}</td>
            <td>{{ $datam->fin_year ?? '' }}</td>
            <td>{{ $datam->amount ?? '' }}</td>
            <td>{{ $datam->reference_no ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budget-transfer.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('budget-transfer.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('budget-transfer.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budget-transfer.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
