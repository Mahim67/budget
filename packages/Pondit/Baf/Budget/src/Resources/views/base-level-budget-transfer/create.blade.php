@extends('pondit-limitless::layouts.master')
@section('content')

<div class="row">
    <div class="col-md-6">
        <x-pondit-card title="Budget Transfer ( BBD BASE )">
            <x-pondit-form action="{{route('base-level-budget-transfer.store')}}" >
                    <div class='form-group row'>
                        <label for="from_code" class="col-sm-2 col-form-label">From Code</label>
                        <div class='col-sm-10'>
                            {{-- <input type="text" name="from_code" id="from_code" class="form-control" /> --}}
                            <select name="from_code" class="form-control select-search" id="budgetCode" required>
                                <option value="">--Select From Code--</option>
                               
                                @foreach ($budgetCodes as  $budgetCode)
                                    <option value="{{ $budgetCode->budget_allotment_id }}">oldcode :  {{ $budgetCode->old_code ?? '' }}   ||   newcode :  {{ $budgetCode->new_code ?? '' }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <p style="padding-left: 17%"> Current Amount : <span id="balance" style="color:black; background:yellow"></span> </p>
                   
                    <div class='form-group row'>
                        <label for="to_code" class="col-sm-2 col-form-label">To Code</label>
                        <div class='col-sm-10'>
                            {{-- <input type="text" name="to_code" id="to_code" class="form-control" /> --}}
                            <select name="to_code" class="form-control select-search"  required >
                                <option value="">--Select To Code--</option>
                                
                                @foreach ($budgetCodes as  $budgetCode)
                                
                                <option value="{{ $budgetCode->budget_allotment_id }}">oldcode {{ $budgetCode->old_code ?? '' }} || newcode : {{ $budgetCode->new_code ?? '' }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class='form-group row'>
                        <label for="amount" class="col-sm-2 col-form-label">Amount</label>
                        <div class='col-sm-10'>
                            <input type="number" name="amount" id="amount" class="form-control" required />
                        </div>
                    </div>
                    <div class='form-group row'>
                        <label for="reference_no" class="col-sm-2 col-form-label">Reference No</label>
                        <div class='col-sm-10'>
                            <input type="text" name="reference_no" id="reference_no" class="form-control" />
                        </div>
                    </div>
            
                    <div class='form-group row'>
                        <label for="letter" class="col-sm-2 col-form-label">Letter</label>
                        <div class='col-sm-10'>
                            <input type="file" name="letter" id="letter" class="form-control" />
                        </div>
                    </div>
            
            
                    <x-pondit-btn icon="check" title="{{ __('save') }}" />
                    <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
                </x-pondit-form>
                <x-slot name="cardFooter">
                    <div></div>
                    <div>
                    <x-pondit-act-i url="{{route('budget-transfer.index')}}" />
                    </div>
                    <div></div>
                </x-slot>
            </x-pondit-card>    
    </div>
    <div class="col-md-6">
        <x-pondit-card title="Today's Balance Transferred">
        
           <form action="" method="GET" class="form-inline">
               <input type="number" name="filter_min" class="form-control" placeholder="Search With Minutes"/>
               <x-pondit-btn icon="search" title="{{ __('Filter') }}" />
           </form>
            <table class="table table-bordered mt-3"> 
                <?php $i = 1;?>
                <thead>
                    <td>ID</td>
                    <td>FROM CODE</td>
                    <td>TO CODE</td>
                    <td>AMOUNT</td>
                    <td>TRANSFERRED</td>
                </thead>
                @foreach ($data as $todayData)
                    <tbody>
                        <td>{{ $i++ }}</td>
                        <td>{{ nameofCode($todayData->from_code ?? null ) }}</td>
                        <td>{{ nameofCode($todayData->to_code ?? null) }}</td>
                        <td>{{ $todayData->amount ?? null }}</td>
                        <td><mark>{{ $todayData->created_at->diffForHumans() ?? null }}</mark></td>
                    </tbody>
                @endforeach
              

            </table>
        </x-pondit-card>
    </div>

</div>

@endsection

@push('js')

<script> 
    var balance;
     $(document).on('change', '#budgetCode', function()
        {
            let id = $(this).val();
            console.log(id)
            $.ajax
            ({
                type: "GET",
                url: '/getBalance/' + id,
                data:id,
                cache: false,
                success: function(data)
                {
                     balance = document.getElementById("balance").innerHTML = data;
                    
                }
            });


        })

        $(document).on('keyup change', '#amount', function()
        {
           
            let d = document.getElementById("amount").value
            let currentBalance = balance - Number(d);

            document.getElementById("balance").innerHTML = currentBalance;
        })


</script>
    
@endpush



    
