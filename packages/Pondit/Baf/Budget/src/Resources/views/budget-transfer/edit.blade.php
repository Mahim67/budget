@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Edit Budget Transfer">
    <x-pondit-form action="{{ route('budget-transfer.update', $data->id) }}">
        @method('PUT')
        
        <div class='form-group row'>
            <label for="from_code" class="col-sm-2 col-form-label">From Code</label>
            <div class='col-sm-10'>
                {{-- <input type="text" name="from_code" id="from_code" class="form-control" /> --}}
                <select name="from_code" class="form-control select-search"  id="budgetCode" required>
                    <option value="">--Select From Code--</option>
                    @foreach ( $budgetCodes as  $budgetCode)
                        @if ($budgetCode->id == $data->from_code)
                            <option value="{{  $budgetCode->id }}" selected>{{ nameofCode($data->from_code) }} ( {{ $budgetCode->old_code }}) - {{ $budgetCode->range_name ?? null }}</option>
                        @else
                            <option value="{{ $budgetCode->id  }}">{{ $budgetCode->new_code ?? null  }}  ( {{ $budgetCode->old_code }}) - {{ $budgetCode->range_name ?? null }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <p style="padding-left: 17%"> Current Balance : <span id="balance" style="color:black; background:yellow"> {{ $balance ?? null }} </span> </p>

        <div class='form-group row'>
            <label for="from_code" class="col-sm-2 col-form-label">To Code</label>
            <div class='col-sm-10'>
                {{-- <input type="text" name="from_code" id="from_code" class="form-control" /> --}}
                <select name="from_code" class="form-control select-search"  required>
                    <option value="">--Select To Code--</option>
                    @foreach ( $budgetCodes as  $budgetCode)
                        @if ($budgetCode->id == $data->to_code)
                            <option value="{{  $budgetCode->id }}" selected>{{ nameofCode($data->to_code) }}  ( {{ $budgetCode->old_code }}) - {{ $budgetCode->range_name ?? null }}</option>
                        @else
                            <option value="{{ $budgetCode->id  }}">{{ $budgetCode->new_code ?? null  }}  ( {{ $budgetCode->old_code }}) - {{ $budgetCode->range_name ?? null }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class='form-group row'>
            <label for="amount" class="col-sm-2 col-form-label">Amount</label>
            <div class='col-sm-10'>
                <input type="number" name="amount" id="amount" value="{{ $data->amount ?? '' }}" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label for="reference_no" class="col-sm-2 col-form-label">Reference No</label>
            <div class='col-sm-10'>
                <input type="text" name="reference_no" id="reference_no" value="{{ $data->reference_no ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="letter" class="col-sm-2 col-form-label">Letter</label>
            <div class='col-sm-4'>
                <input type="file" name="letter" id="letter" class="form-control" />
            </div>
            @if (isset($data->letter))
            <div class="col-sm-6">
                <img src="{{ asset('') . $data->letter }}" height="300" width="300" alt="">
            </div>
            @endif

        </div>


        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budget-transfer.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budget-transfer.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('budget-transfer.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('budget-transfer.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
@push('js')

<script> 
    var balance;
     $(document).on('change', '#budgetCode', function()
        {
            let id = $(this).val();
            console.log(id)
            $.ajax
            ({
                type: "GET",
                url: '/getBalance/' + id,
                data:id,
                cache: false,
                success: function(data)
                {
                     balance = document.getElementById("balance").innerHTML = data;
                    
                }
            });


        })

        $(document).on('keyup change', '#amount', function()
        {
           
            let d = document.getElementById("amount").value
            let currentBalance = balance - Number(d);

            document.getElementById("balance").innerHTML = currentBalance;
        })


</script>
    
@endpush