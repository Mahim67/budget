@extends('pondit-limitless::layouts.master')


@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
@php
    $object = new Pondit\Baf\Budget\Http\Controllers\BudgetTransferController();
@endphp

<x-pondit-card title="Budget Transfer">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('Ser No')}}</th>
                <th>{{__('From Code')}}</th>
                <th>{{__('To Code')}}</th>
                <th>{{__('Fin Year')}}</th>
                <th class="text-right">{{__('Transferred')}}</th>
                <th class="text-right">{{__('Current Balance')}}</th>
                <th>{{__('Reference No')}}</th>
                <th class="text-right">{{__('Document')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
      
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ nameofCode($datam->from_code) }}</td>
            <td>{{nameofCode($datam->to_code)}}</td>
            <td>{{ $datam->fin_year ?? '' }}</td>
            <td class="text-right">{{ $datam->amount ?? '' }}</td>
            <td class="text-right">{{ currentBalance($datam->from_code) ?? '' }}</td>
            <td>{{ $datam->reference_no ?? '' }}</td>
            <td>
                @if($datam->letter)
                    <a class="btn btn-sm btn-toolbar" href="{{route('letter', $datam->letter)}}" target="_blank"><i class="fas fa-envelope-open-text"></i></a>
                @else
                    {{ " " }}
                @endif
            </td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budget-transfer.show', $datam->id)}}" icon="eye" bg="success" tooltip="Show"/>
                <x-pondit-act-link url="{{route('budget-transfer.edit', $datam->id)}}" icon="pen" bg="primary" tooltip="Edit"/>
                <x-pondit-act-link-d url="{{route('budget-transfer.delete', $datam->id)}}" icon="trash" tooltip="Delete"/>
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budget-transfer.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

