@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Budget Transfer">
<x-pondit-form action="{{route('budget-transfer.store')}}" >
        <div class='form-group row'>
            <label for="from_code" class="col-sm-2 col-form-label">From Code</label>
            <div class='col-sm-10'>
                {{-- <input type="text" name="from_code" id="from_code" class="form-control" /> --}}
                <select name="from_code" class="form-control select-search"  id="budgetCode" required>
                    <option value="">--Select From Code--</option>
                    @foreach ($budgetCodes as  $budgetCode)
                        <option value="{{ $budgetCode->id }}"> {{ $budgetCode->budgetcode->newcode ?? null}} ( {{ $budgetCode->budgetcode->oldcode ?? null}} ) - {{$budgetCode->range_name ?? null}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <p style="padding-left: 17%"> Current Balance : <span id="balance" style="color:black; background:yellow"></span> </p>

        <div class='form-group row'>
            <label for="to_code" class="col-sm-2 col-form-label">To Code</label>
            <div class='col-sm-10'>
                {{-- <input type="text" name="to_code" id="to_code" class="form-control" /> --}}
                <select name="to_code" class="form-control select search" required >
                    <option value="">--Select To Code--</option>
                    @foreach ($budgetCodes as  $budgetCode)
                        <option value="{{ $budgetCode->id }}"> {{ $budgetCode->budgetcode->newcode ?? null}} ( {{ $budgetCode->budgetcode->oldcode ?? null}} ) - {{$budgetCode->range_name ?? null}}</option>
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class='form-group row'>
            <label for="amount" class="col-sm-2 col-form-label">Amount</label>
            <div class='col-sm-10'>
                <input type="text" name="amount" id="amount" class="form-control" required />
            </div>
        </div>
        <div class='form-group row'>
            <label for="reference_no" class="col-sm-2 col-form-label">Reference No</label>
            <div class='col-sm-10'>
                <input type="text" name="reference_no" id="reference_no" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="letter" class="col-sm-2 col-form-label">Letter</label>
            <div class='col-sm-10'>
                <input type="file" name="letter" id="letter" class="form-control" />
            </div>
        </div>


        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('budget-transfer.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

@push('js')

<script> 
    var balance;
     $(document).on('change', '#budgetCode', function()
        {
            let id = $(this).val();
            console.log(id)
            $.ajax
            ({
                type: "GET",
                url: '/getBalance/' + id,
                data:id,
                cache: false,
                success: function(data)
                {
                     balance = document.getElementById("balance").innerHTML = data;
                    
                }
            });


        })

        $(document).on('keyup change', '#amount', function()
        {
           
            let d = document.getElementById("amount").value
            let currentBalance = balance - Number(d);

            document.getElementById("balance").innerHTML = currentBalance;
        })


</script>
    
@endpush

