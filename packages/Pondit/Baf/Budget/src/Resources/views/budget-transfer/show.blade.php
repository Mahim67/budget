@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Transfer">
    <div class="single-show w-75 m-auto">
        {{-- <div class="card-body text-center card-img-top">
            <div class="card-img-actions d-inline-block">
                <img class="img-fluid "
                    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/images/placeholders/placeholder.jpg"
                    width="200" height="150" alt="">
            </div>
        </div> --}}
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('From Code')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ getBudgetCode($data->from_code)->newcode ?? '' }}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('To Code')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{getBudgetCode($data->to_code)->newcode ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('Fin Year')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->fin_year ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Amount :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->amount ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Transfer At :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->transfer_at ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Transfer By :</div>
                        <div class="mt-2 mt-sm-0 ml-3">User</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Reference No :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{ $data->reference_no ?? '' }}</div>
                    </div>
                </li>
                @if (isset($data->letter))
                    <li class="nav-item">
                        <div class="d-flex nav-link">
                            <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Letter :</div>
                            @if(file_exists(storage_path().'/app/public/letter/'.$data->letter ) && (!is_null($data->letter)))
                                {{-- <iframe src="{{ storage_path().'app/public/letter/'.$data->letter }}" height="100"></iframe> --}}
                                <iframe  src="{{ asset('storage/letter/'.$data->letter) }}" height="100"></iframe>
                            @else
                            @dd("nai");
                            @endif

                        </div>
                    </li>
                @endif

            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budget-transfer.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budget-transfer.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-e url="{{route('budget-transfer.edit', $data->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('budget-transfer.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>


@endsection