@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

@php
session()->forget('excelErrors');
@endphp

<x-pondit-card title="Budget Code">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Title')}}</th>
                <th>{{__('Fin Year')}}</th>
                <th>{{__('Reference No')}}</th>
                <th>{{__('Amount')}}</th>
                <th>{{__('Type')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->title ?? '' }}</td>
            <td>{{ $datam->fin_year ?? '' }}</td>
            <td>{{ $datam->reference_no ?? '' }}</td>
            <td>{{ $datam->amount ?? '' }}</td>
            <td>{{ $datam->type ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budget.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('budget.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('budget.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            {{-- <x-pondit-act-c url="{{route('budget.create')}}" /> --}}
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
