@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Initial Budget Allotment">
<x-pondit-form action="{{route('budget.initial_budget_store')}}" >

    <div class='form-group row'>
        <label for="amount" class="col-sm-2 col-form-label">Title</label>
        <div class='col-sm-10'>
            <input type="text" name="title" id="title" value="Initial Budget Allotmnt For " class="form-control" />
        </div>
    </div>
    <div class='form-group row'>
        <label for="amount" class="col-sm-2 col-form-label">Amount</label>
        <div class='col-sm-10'>
            <input type="test" name="amount" id="amount" class="form-control" />
        </div>
    </div>
    <div class='form-group row'>
        <label for="reference_no" class="col-sm-2 col-form-label">Reference No</label>
        <div class='col-sm-10'>
            <input type="text" name="reference_no" id="reference_no" class="form-control" />
        </div>
    </div>

{{--    <div class='form-group row'>--}}
{{--        <label for="letter" class="col-sm-2 col-form-label">Letter</label>--}}
{{--        <div class='col-sm-10'>--}}
{{--            <input type="file" name="letter" id="letter" class="form-control" />--}}
{{--        </div>--}}
{{--    </div>--}}



{{--    <div class='form-group row'>--}}
{{--        <label for="allotment_excel" class="col-sm-2 col-form-label">Allotment Excel</label>--}}
{{--        <div class='col-sm-10'>--}}
{{--            <input type="file" name="allotment_excel" id="allotment_excel" class="form-control" />--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class='form-group row'>
        <label for="allotment_excel" class="col-sm-2 col-form-label"></label>
        <div class='col-sm-6'>
            <div class="row">
                <div class="col">
                    <div class="text-right btn bg-success btn-file btn-sm mt-2">
                        <input type="file" name="allotment_excel" accept=".xlsx, .xls, .csv" id="excelBtn"/>
                        <i class="fas fa-upload"></i> Upload Excel
                    </div><br>
                    <span id="file-chosen" class="">No Excel chosen</span>
                </div>
                <div class="col">
                    <div class="text-right btn bg-primary btn-file btn-sm mt-2">
                        <input type="file" name="letter"  id="fileBtn"/>
                        <i class="fas fa-upload"></i> Upload Letter
                    </div><br>
                    <span id="letter-file" class="">No Letter chosen</span>
                </div>
            </div>
        </div>
    </div>

        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('budget.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

@push('js')
    <script>

        const uploadBtn = document.getElementById('excelBtn');

        const fileChosen = document.getElementById('file-chosen');

        uploadBtn.addEventListener('change', function(){
            fileChosen.textContent = this.files[0].name;
        })


        const fileBtn = document.getElementById('fileBtn');

        const file = document.getElementById('letter-file');

        fileBtn.addEventListener('change', function(){
            file.textContent = this.files[0].name;
        })
    </script>
@endpush
