@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="Budget">

    <div class="single-show w-75 m-auto">
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
              
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('Fin Year')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->fin_year ?? ''}}</div>
                    </div>
                </li>
               
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('Reference No')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->reference_no ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Amount :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->amount ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Type :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->type ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Letter:</div>
                        <div class="mt-2 mt-sm-0 ml-3"><img src="{{ asset('') . $data->letter}}" alt="" height="500" width="500"></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budget.index')}}" tooltip="{{__('list')}}"/>
            {{-- <x-pondit-act-c url="{{route('budget.create')}}" tooltip="{{__('create')}}"/> --}}
            <x-pondit-act-e url="{{route('budget.edit', $data->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('budget.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>

<!-- /basic example -->

@endsection
{{-- 
@push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}