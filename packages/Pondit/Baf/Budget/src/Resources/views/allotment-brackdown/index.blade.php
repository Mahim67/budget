@extends('pondit-limitless::layouts.master')

@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Allotment Breakdown">
    <a href="{{route('allotment_brackdown.range_wise_report')}}" class="btn bg-success" >Range  Report</a>
    <a href="{{ route('base-demand-allotment-breakdown.base_demand')}}" class="btn bg-success" >Demand from Base </a>
    
  
        <x-pondit-datatable>
            <x-slot name="thead">
                <tr>
                    <th>{{__('SL')}}</th>
                    <th>{{__('Old Code')}}</th>
                    <th>{{__('New Code')}}</th>
                    <th>{{__('Range')}}</th>
                    <th class="text-right">{{__('Initial')}}</th>
                    <th class="text-right">{{__('BSR')}}</th>
                    <th class="text-right">{{__('MTR')}}</th>
                    <th class="text-right">{{__('ZHR')}}</th>
                    <th class="text-right">{{__('BBD')}}</th>
                    <th class="text-right">{{__('PKP')}}</th>
                    <th class="text-right">{{__('CXB')}}</th>
                    <th class="text-right">{{__('201MU')}}</th>
                    <th class="text-right">{{__('BAFA')}}</th>
                    <th class="text-right">{{__('BRU')}}</th>
                    <th class="text-right">{{__('MRU')}}</th>
                    <th class="text-right">{{__('Total Base Allotment')}}</th>
                    <th class="text-right">{{__('Spent FC Army')}}</th>
                    <th class="text-right">{{__('Spent Base Unit')}}</th>
                    <th class="text-right">{{__('Spent Air HQ')}}</th>
                    <th class="text-right">{{__('Total Spent')}}</th>
                    <th class="text-right">{{__('Balance')}}</th>
                    <th>{{__('Actions')}}</th>
                </tr>
            </x-slot>
            @forelse ($data as $key=>$datam)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $datam->old_code ?? null }}</td>
                <td>{{ $datam->new_code ?? null }}</td>
                <td>{{ rangeName($datam->range_id) ?? null}}</td>
                <td class="text-right">{{ $datam->intial_budget ?? null}}</td>
                <td class="text-right">{{ $datam->base_bsr ?? null}}</td>
                <td class="text-right">{{ $datam->base_mtr ?? null}}</td>
                <td class="text-right">{{ $datam->base_zhr ?? null }}</td>
                <td class="text-right">{{ $datam->base_bbd ?? null}}</td>
                <td class="text-right">{{ $datam->base_pkp ?? null}}</td>
                <td class="text-right">{{ $datam->base_cxb ?? null}}</td>
                <td class="text-right">{{ $datam->base_air_hq ?? null}}</td>
                <td class="text-right">{{ $datam->base_201mu ?? null}}</td>
                <td class="text-right">{{ $datam->base_201u ?? null}}</td>
                <td class="text-right">{{ $datam->base_bru ?? null}}</td>
                <td class="text-right">{{ $datam->base_mru ?? null}}</td>
                <td class="text-right">{{ $datam->total_base_allotment ?? null}}</td>
                <td class="text-right">{{ $datam->spent_fc_army ?? null}}</td>
                <td class="text-right">{{ $datam->spent_airhq ?? null}}</td>
                <td class="text-right">{{ $datam->total_spent ?? null}}</td>
                <td class="text-right">{{ $datam->balance ?? null}}</td>
                <td class="d-flex">
                    <x-pondit-act-link url="{{route('allotment_brackdown.show', $datam->id)}}" icon="eye" bg="success" />
                    <x-pondit-act-link url="{{route('allotment_brackdown.edit', $datam->id)}}" icon="pen" bg="primary" />
                    <x-pondit-act-link-d url="{{route('allotment_brackdown.delete', $datam->id)}}" icon="trash" />
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="10" class="center text-success" > <b> No Data Found </b></td>
            </tr>
            @endforelse
        </x-pondit-datatable>
  

    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('allotment_brackdown.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

