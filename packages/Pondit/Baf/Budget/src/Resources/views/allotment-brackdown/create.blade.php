@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Budget Breakdown  in Base & Unit">
<x-pondit-form action="{{route('allotment_brackdown.store')}}" >
        
    <div class='form-group row'>
        <label class="col-2 col-form-label" for="budget_type">Budget Code</label>
        <div class='col -4'>
            <select name="budget_allotment_id" id="budgetCode" class="form-control select-search">
                <option value="">--Select Budgetcode--</option>
                @foreach ($budgetcodes as $budgetcode)
                    <option value="{{ $budgetcode->id }}">{{ $budgetcode->budgetcode->newcode ?? null}} ( {{ $budgetcode->budgetcode->oldcode ?? null }} ) - {{ rangeName($budgetcode->range_id)}} </option>
                @endforeach
            </select>
        </div>

        <div class='col-3'> 
            <h6 style="padding-left: 17%;" class="mt-3"> <span>Balanced : </span> <span id="balance" style="color:black; background:yellow"></span> </h6>
        </div>
        
        <div class='col-3'> 

            <div class="form-group" id="lprInfo"> 
                <label class="col-form-label text-primary" for="demand_of_base">Demand form Base</label>
                <input type="checkbox" name="demand_of_base" value="1" class="option-input ml-5" id="onLpr"/> </p>
            </div>
            
        </div>
        
    </div>

    {{-- <div class='form-group row'>
        <label class="col-sm-2 col-form-label" for="budget_type">Range</label>
        <div class='col-sm-4'>
            <select name="range_id" id="range_id" class="form-control select-search">
                <option value="">--Select Range--</option>
                @foreach ($ranges as $id => $title)
                    <option value="{{ $id }}">{{ $title }}</option>
                @endforeach
            </select>
        </div>
    </div> --}}



        {{-- <div class='form-group row'>
            <label for="budget_allotment_id" class="col-sm-2 col-form-label">Budget Allotment</label>
            <div class='col-sm-4'>
                <select name="budget_allotment_id" id="budget_allotment_id" class="form-control">
                    <option value="">--Select Badget Code--</option>
                    @foreach ($budgetAllotments as $allotment)
                        <option value="{{ $allotment->id }}">{{ $allotment->budgetcode->newcode ?? null }} ({{ $allotment->range_name }})</option>    
                    @endforeach
                </select>
            </div>
        </div> --}}

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bsr" class="col-form-label ">BSR</label>
                    <input type="text" name="base_bsr" id="base_bsr" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_mtr" class="col-form-label ">MTR</label>
                    <input type="text" name="base_mtr" id="base_mtr" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group '>
                    <label for="base_zhr" class=" col-form-label ">ZHR</label>
                    <input type="text" name="base_zhr" id="base_zhr" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bbd" class="col-form-label">BBD</label>
                    <input type="text" name="base_bbd" id="base_bbd" class="form-control amount" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_pkp" class=" col-form-label">PKP</label>
                    <input type="text" name="base_pkp" id="base_pkp" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_cxb" class="col-form-label">CXB</label>
                    <input type="text" name="base_cxb" id="base_cxb" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group '>
                    <label for="base_air_hq" class="col-form-label">Air HQ</label>
                    <input type="text" name="base_air_hq" id="base_air_hq" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_201mu" class="col-form-label">201 MU</label>
                    <input type="text" name="base_201mu" id="base_201mu" class="form-control amount" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_201u" class=" col-form-label">BAFA</label>
                    <input type="text" name="base_201u" id="base_201u" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bru" class=" col-form-label">BRU</label>
                    <input type="text" name="base_bru" id="base_bru" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_mru" class=" col-form-label">MRU</label>
                    <input type="text" name="base_mru" id="base_mru" class="form-control amount" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="spent_fc_army" class=" col-form-label">Spent FC Army</label>
                    <input type="text" name="spent_fc_army" id="spent_fc_army" class="form-control amount" />
                </div>
            </div>
        </div>

        <div class='form-group row'>
            <label for="" class="col-sm-2 col-form-label">Spent Navy</label>
            <div class='col-sm-10'>
                <input type="text" name="spent_airhq" id="spent_airhq" class="form-control amount" />
            </div>
        </div>
        
        <div class='form-group row'>
            <label for="spent_airhq" class="col-sm-2 col-form-label">Spent Air HQ</label>
            <div class='col-sm-10'>
                <input type="text" name="spent_airhq" id="spent_airhq" class="form-control amount" />
            </div>
        </div>

       


        {{-- <div class='form-group row'>
            <label for="letter" class="col-sm-2 col-form-label">Letter</label>
            <div class='col-sm-10'>
                <input type="file" name="letter" id="letter" class="form-control" />
            </div>
        </div> --}}

       
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('allotment_brackdown.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

@push('css')
<style>
    label{
        font-weight: bold
    }
</style>
    
@endpush
@push('js')

<script> 
    var balance;
     $(document).on('change', '#budgetCode', function()
        {
            let id = $(this).val();
            console.log(id)
            $.ajax
            ({
                type: "GET",
                url: '/getBalance/' + id,
                data:id,
                cache: false,
                success: function(data)
                {
                     balance = document.getElementById("balance").innerHTML = data;
                    
                }
            });


        })

       
        $('.amount').on('keyup', function(){

            var total = 0;

            $('.amount').each(function(index, element) {
                var val = parseFloat($(element).val());
                if( !isNaN( val )){
                total += val;
                }
            });
            let currentBalance = balance - total;

            document.getElementById("balance").innerHTML = currentBalance;
});


</script>
    
@endpush
<style>
     @keyframes click-wave {
            0% {
                height: 40px;
                width: 40px;
                opacity: 0.35;
                position: relative;
            }
            100% {
                height: 200px;
                width: 200px;
                margin-left: -80px;
                margin-top: -80px;
                opacity: 0;
            }
        }

        .option-input {
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            appearance: none;
            position: relative;
            top: 13.33333px;
            right: 0;
            bottom: 0;
            left: 0;
            height: 40px;
            width: 40px;
            transition: all 0.15s ease-out 0s;
            background: #cbd1d8;
            border: none;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            margin-right: 0.5rem;
            outline: none;
            position: relative;
            z-index: 1000;
        }
        .option-input:hover {
            background: #086d15;
        }
        .option-input:checked {
            background: #036420;
        }
        .option-input:checked::before {
            height: 40px;
            width: 40px;
            position: absolute;
            content: '✔';
            display: inline-block;
            font-size: 26.66667px;
            text-align: center;
            line-height: 40px;
        }
        .option-input:checked::after {
            -webkit-animation: click-wave 0.65s;
            -moz-animation: click-wave 0.65s;
            animation: click-wave 0.65s;
            background: #0a8634;
            content: '';
            display: block;
            position: relative;
            z-index: 100;
        }
        
</style>