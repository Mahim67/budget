@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Budget Breakdown  in Base & Unit">
    <x-pondit-form action="{{ route('allotment_brackdown.update', $data->id) }}">
        @method('PUT')
        
             
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label" for="budget_type">Budgetcode</label>
            <div class='col-sm-4'>
                <select name="budget_allotment_id" id="budgetcode_id" class="form-control select-search" onchange="getRangeForBudgetCode()">
                    <option value="">--Select Budgetcode--</option>
                    @foreach ($budgetcodes as $budgetcode)
                        {{-- <option value="{{ $budgetcode->id }}">{{ $budgetcode->budgetcode->newcode ?? null}} ( {{ $budgetcode->budgetcode->oldcode ?? null }} ) - {{ rangeName($budgetcode->range_id)}} </option> --}}
                        @if ($budgetcode->id == $data->budget_allotment_id)
                        <option value="{{ $budgetcode->id }}" selected>{{ $budgetcode->new_code  ?? null }} ( {{ $budgetcode->old_code  ?? null }} ) - {{ rangeName($budgetcode->range_id)}}  </option>
                        @else
                        <option value="{{ $budgetcode->id }}">{{ $budgetcode->new_code  ?? null }} ( {{ $budgetcode->old_code  ?? null }} ) - {{ rangeName($budgetcode->range_id)}}  </option>

                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    
        {{-- <div class='form-group row'>
            <label class="col-sm-2 col-form-label" for="budget_type">Range</label>
            <div class='col-sm-4'>
                <select name="range_id" id="range_id" class="form-control select-search">
                    <option value="">--Select Range--</option>
                    @foreach ($ranges as $id => $title)
                        <option value="{{ $id }}">{{ $title }}</option>
                    @endforeach
                </select>
            </div>
        </div> --}}

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bsr" class="col-form-label">BSR</label>
                    <input type="text" name="base_bsr" id="base_bsr" value="{{ $data->base_bsr }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_mtr" class="col-form-label">MTR</label>
                    <input type="text" name="base_mtr" id="base_mtr" value="{{ $data->base_mtr }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group '>
                    <label for="base_zhr" class=" col-form-label">ZHR</label>
                    <input type="text" name="base_zhr" id="base_zhr" value="{{ $data->base_zhr }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bbd" class="col-form-label">BBD</label>
                    <input type="text" name="base_bbd" id="base_bbd" value="{{ $data->base_bbd }}" class="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_pkp" class=" col-form-label">PKP</label>
                    <input type="text" name="base_pkp" id="base_pkp" value="{{ $data->base_pkp }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_cxb" class="col-form-label">CXB</label>
                    <input type="text" name="base_cxb" id="base_cxb" value="{{ $data->base_cxb }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group '>
                    <label for="base_air_hq" class="col-form-label">Air HQ</label>
                    <input type="text" name="base_air_hq" id="base_air_hq" value="{{ $data->base_air_hq }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_201mu" class="col-form-label">201 MU</label>
                    <input type="text" name="base_201mu" id="base_201mu"  value="{{ $data->base_201mu }}" class="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_201u" class=" col-form-label">BAFA</label>
                    <input type="text" name="base_201u" id="base_201u"  value="{{ $data->base_201u }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bru" class=" col-form-label">BRU</label>
                    <input type="text" name="base_bru" id="base_bru"  value="{{ $data->base_bru }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_mru" class=" col-form-label">MRU</label>
                    <input type="text" name="base_mru" id="base_mru"  value="{{ $data->base_mru }}" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="spent_fc_army" class=" col-form-label">Spent FC Army</label>
                    <input type="text" name="spent_fc_army" id="spent_fc_army"  value="{{ $data->spent_fc_army }}" class="form-control" />
                </div>
            </div>
        </div>

        <div class='form-group row'>
            <label for="spent_airhq" class="col-sm-2 col-form-label">Spent AIR HQ</label>
            <div class='col-sm-10'>
                <input type="text" name="spent_airhq" id="spent_airhq"  value="{{ $data->spent_airhq }}" class="form-control" />
            </div>
        </div>



        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('allotment_brackdown.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('allotment_brackdown.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('allotment_brackdown.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('allotment_brackdown.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
@push('css')
<style>
    label{
        font-weight: bold
    }
</style>
    
@endpush