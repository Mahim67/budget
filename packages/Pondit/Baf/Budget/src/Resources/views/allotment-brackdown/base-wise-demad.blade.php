@extends('pondit-limitless::layouts.master')

@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Allotment Breakdown (Demand form Base )">
      
    <table class="table table-bordered table-sm" id="table">
        <thead>
            <tr class="bg-success">
                <th>SL NO</th>
                <th>New Code</th>
                <th>Old Code</th>
                <th>Description</th>
                <th>Amount</th>
               

            </tr>
        </thead>
        <tbody>
            @foreach ($data as $key=>$da)
            <tr>
                <td colspan="7" class="font-weight-bold text-primary">{{ $key ?? null }}</td>
            </tr>
            @foreach ($da as $datam)
            <tr>
                <td>#</td>
                <td>{{ $datam->new_code ?? null }}</td>
                <td>{{ $datam->old_code ?? null }}</td>
                <td>{{ $datam->description ?? null }}</td>
                <td>{{ $datam->$key ?? null }}</td>                
            </tr>
            @endforeach
            @endforeach
        </tbody>
        </table>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('allotment_brackdown.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

