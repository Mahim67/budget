@extends('pondit-limitless::layouts.master')

@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')
    <!-- Basic example -->
    <x-pondit-card title="Range Wise Report">

        <div class="single-show">
            <div class="card-body p-0">
                <button class="btn btn-primary mb-1 btn-sm " onclick="javascrit:window.history.back()"><i
                        class="fa fa-arrow-left"></i></button>
                <table class="table table-bordered table-responsive table-sm" id="table">
                    <thead>
                        <tr class="bg-success">
                            <th>SL NO</th>
                            <th>New Code</th>
                            <th>Old Code</th>
                            <th>Description</th>
                            <th>{{__('BSR')}}</th>
                            <th >{{__('MTR')}}</th>
                            <th >{{__('ZHR')}}</th>
                            <th>{{__('BBD')}}</th>
                            <th>{{__('PKP')}}</th>
                            <th>{{__('CXB')}}</th>
                            <th>{{__('201MU')}}</th>
                            <th>{{__('BAFA')}}</th>
                            <th>{{__('BRU')}}</th>
                            <th>{{__('MRU')}}</th>
                            <th>{{__('BANLANCE')}}</th>
                           

                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $range => $breakdownBase)
                            <tr>
                                <td colspan="7" class="font-weight-bold text-primary">{{ rangeName($range) }}</td>
                            </tr>
                            @foreach ($breakdownBase as $key => $datam)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $datam->new_code ?? null }}</td>
                                    <td>{{ $datam->old_code ?? null }}</td>
                                    <td>{{ $datam->description ?? null }}</td>
                                    <td class="text-right">{{ $datam->base_bsr ?? null }}</td>
                                    <td class="text-right">{{ $datam->base_mtr ?? null }}</td>
                                    <td class="text-right">{{ $datam->base_zhr ?? null }}</td>
                                    <td class="text-right">{{ $datam->base_bbd ?? null }}</td>
                                    <td class="text-right">{{ $datam->base_pkp ?? null }}</td>
                                    <td class="text-right">{{ $datam->base_cxb ?? null}}</td>
                                   
                                    <td class="text-right">{{ $datam->base_201mu ?? null}}</td>
                                    <td class="text-right">{{ $datam->base_201u ?? null}}</td>
                                    <td class="text-right">{{ $datam->base_bru ?? null}}</td>
                                    <td class="text-right">{{ $datam->base_mru ?? null }}</td>
                                    <td class="text-right">{{ $datam->balance ?? null }}</td>
                                    
                                </tr>
                                
                            @endforeach
                        @empty
                        <tr>
                            <td class="text-center" colspan="7">No Data Found</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <x-slot name="cardFooter">
            <div></div>
            <div></div>
            <div>
                <a href="{{ route('allotment_brackdown.baseAllotement')}}" data-popup="tooltip" title="Range Wise Report PDF"
                    data-original-title="Range Wise Report"><i class="fas fa-file-pdf px-1 py-1 bg-brown">
                        
                        </i>
                </a>

                <a href="{{ route('allotment_brackdown.allotment_breakdown_excel')}}" data-popup="tooltip" title="Range Wise Report EXCEL"
                data-original-title="Range Wise Report"><i class="fas fa-file-excel px-1 py-1 bg-success"></i></a>
            </div>
        </x-slot>
    </x-pondit-card>

    <!-- /basic example -->

@endsection
@push('js')
    <script>
        function exportF(elem) {
            var table = document.getElementById("table");
            var html = table.outerHTML;
            var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
            elem.setAttribute("href", url);
            elem.setAttribute("download", "Range-wise-report.xls"); // Choose the file name
            return false;
        }

    </script>
@endpush
