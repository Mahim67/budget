<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    table {
        width: 100%;
    }

    @page {
        margin: 5%;
        sheet-size: A4-L;

    }

</style>


<h4 style="text-align: center ; padding:0 ; margin: 0 ;"> BUDGET ALLOTMENT ALL BASE RANGE WISE FY
    2020-2021 </h4>
<p style="text-align: center ; color:blue ;  padding:0 ; margin: 0 ;">(Amounts in Crore)</p>
<table>
    <thead>
        <tr class="bg-success">
            <th>SL NO</th>
            <th>New Code</th>
            <th>Old Code</th>
            <th>Description</th>
            <th>{{__('BSR')}}</th>
            <th>{{__('MTR')}}</th>
            <th>{{__('ZHR ')}}</th>
            <th>{{__('BBD ')}}</th>
            <th>{{__('PKP ')}}</th>
            <th>{{__('CXB ')}}</th>
            <th>{{__('201MU')}}</th>
            <th>{{__('BAFA')}}</th>
            <th>{{__('BRU')}}</th>
            <th>{{__('MRU')}}</th>
            <th>{{__('BANLANCE')}}</th>
           

        </tr>
    </thead>
    <tbody>
        @forelse ($data as $range => $breakdownBase)
            <tr>
                <td colspan="7" class="font-weight-bold text-primary">{{ rangeName($range) }}</td>
            </tr>
            @foreach ($breakdownBase as $key => $datam)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $datam->new_code }}</td>
                    <td>{{ $datam->old_code}}</td>
                    <td>{{ $datam->description }}</td>
                    <td>{{ $datam->base_bsr }}</td>
                    <td>{{ $datam->base_mtr }}</td>
                    <td>{{ $datam->base_zhr }}</td>
                    <td>{{ $datam->base_bbd }}</td>
                    <td>{{ $datam->base_pkp }}</td>
                    <td>{{ $datam->base_cxb }}</td>
                   
                    <td>{{ $datam->base_201mu }}</td>
                    <td>{{ $datam->base_201u }}</td>
                    <td>{{ $datam->base_bru }}</td>
                    <td>{{ $datam->base_mru }}</td>
                    <td>{{ $datam->banlance }}</td>
                    
                </tr>

            @endforeach
        @empty
        <tr>
            <td class="text-center" colspan="7">No Data Found</td>
        </tr>
        @endforelse
    </tbody>
</table>
