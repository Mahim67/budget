

<table>
    <thead>
        <tr>
            <td colspan="15" style="font-weight: bold;padding: 5px;color:RED;text-align:center;"> <h2> BUDGET ALLOTMENT BREAKDOWN ALL BASES IN 2020-2021</h2></td>

        </tr>
        <tr> 
            <td colspan="15" style="font-weight: bold;padding: 5px;color:blue;text-align:right;"> <h2> ( Amounts in Crore )</h2></td>
        </tr>
        <tr>
            <th style="font-weight: bold;padding: 5px;color:red;">SL NO</th>
            <th style="font-weight: bold;padding: 5px;color:red;">New Code</th>
            <th style="font-weight: bold;padding: 5px;color:red;">Old Code</th>
            <th style="font-weight: bold;padding: 5px;color:red;">Description</th>
            <th style="font-weight: bold;padding: 5px;color:red;">BSR</th>
            <th style="font-weight: bold;padding: 5px;color:red;">MTR</th>
            <th style="font-weight: bold;padding: 5px;color:red;">ZHR</th>
            <th style="font-weight: bold;padding: 5px;color:red;">BBD</th>
            <th style="font-weight: bold;padding: 5px;color:red;">PKB</th>
            <th style="font-weight: bold;padding: 5px;color:red;">CXB</th>
            <th style="font-weight: bold;padding: 5px;color:red;">201MU</th>
            <th style="font-weight: bold;padding: 5px;color:red;">201U</th>
            <th style="font-weight: bold;padding: 5px;color:red;">BRU</th>
            <th style="font-weight: bold;padding: 5px;color:red;">MRU</th>
            <th style="font-weight: bold;padding: 5px;color:red;">BANLANCE</th>
           

        </tr>
    </thead>
    <tbody>
        @forelse ($data as $range => $breakdownBase)
            <tr>
                <td style="font-weight: bold;padding: 5px;color:blue;">{{ rangeName($range) }}</td>
            </tr>
            @foreach ($breakdownBase as $key => $datam)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $datam->new_code }}</td>
                    <td>{{ $datam->old_code}}</td>
                    <td>{{ $datam->description }}</td>
                    <td>{{ $datam->base_bsr }}</td>
                    <td>{{ $datam->base_mtr }}</td>
                    <td>{{ $datam->base_zhr }}</td>
                    <td>{{ $datam->base_bbd }}</td>
                    <td>{{ $datam->base_pkp }}</td>
                    <td>{{ $datam->base_cxb }}</td>
                   
                    <td>{{ $datam->base_201mu }}</td>
                    <td>{{ $datam->base_201u }}</td>
                    <td>{{ $datam->base_bru }}</td>
                    <td>{{ $datam->base_mru }}</td>
                    <td>{{ $datam->banlance }}</td>
                    
                </tr>
                

            @endforeach
        @empty
        <tr>
            <td class="text-center" colspan="7">No Data Found</td>
        </tr>
        @endforelse
    </tbody>
</table>
