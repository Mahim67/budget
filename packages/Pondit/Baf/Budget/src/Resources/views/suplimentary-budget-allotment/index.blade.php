@extends('pondit-limitless::layouts.master')



@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Supplementary Budget">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('New Code')}}</th> 
                <th>{{__('Fin Year')}}</th>
                <th  class="text-right">{{__('Supplementary')}}</th>
                <th  class="text-right">{{__('Current Balance')}}</th>
                <th  class="text-right">{{__('Document')}}</th>
               
                <th class="text-center">{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->budgetcode->newcode ?? ''}} </td>
            <td>{{ $datam->fin_year ?? '' }}</td>
            <td class="text-right">{{ $datam->suplimentary_amount ?? '' }}</td>
            <td  class="text-right">{{ currentBalance($datam->budget_allotment_id) ?? '' }}</td>
            <td  class="text-center">
                @if($datam->letter)
                    <a  href="{{route('supplementary-letter', $datam->letter)}}" target="_blank"><i class="fas fa-envelope-open-text"></i></a>
                @else
                    {{ " " }}
                @endif
            </td>
            <td class="d-flex justify-content-center">
                <x-pondit-act-link url="{{route('suplimentary_budget_allotment.show', $datam->id)}}" icon="eye" bg="success" tooltip="Show"/>
                <x-pondit-act-link url="{{route('suplimentary_budget_allotment.edit', $datam->id)}}" icon="pen" bg="primary" tooltip="Edit"/>
                <x-pondit-act-link-d url="{{route('suplimentary_budget_allotment.delete', $datam->id)}}" icon="trash" tooltip="Delete"/>
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('suplimentary_budget_allotment.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
