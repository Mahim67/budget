@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="Suplementary Budget Allotment">

    <div class="single-show w-75 m-auto">
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('New Code')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->budgetcode->newcode ?? ''}}</div>
                    </div>
                </li>

                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Description :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->description ?? ''}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Fin Year :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->fin_year ?? ''}}</div>
                    </div>
                </li>

                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i>Suplimentary Amount :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->suplimentary_amount ?? ''}}</div>
                    </div>
                </li>


            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('suplimentary_budget_allotment.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('suplimentary_budget_allotment.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-e url="{{route('suplimentary_budget_allotment.edit', $data->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('suplimentary_budget_allotment.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>

@endsection