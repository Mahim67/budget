@extends('pondit-limitless::layouts.master')
@section('content')
    <x-pondit-card title="Supplementary Budget">
        <x-pondit-form action="{{ route('suplimentary_budget_allotment.store') }}">
            <div class="row">
                <div class="col-6">
                    <div class='form-group row'>
                        <label for="reference_no" class="col-sm-3 col-form-label">Reference No</label>
                        <div class='col-sm-9'>
                            <input type="text" name="reference_no" id="reference_no" class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class='form-group row'>
                        <label for="letter" class="col-sm-2 col-form-label float-right">Letter</label>
                        <div class='col-sm-10'>
                            <input type="file" name="letter" id="letter" class="form-control" />
                        </div>
                    </div>
                 </div>
            </div>


            <div class="row mt-3">
                <div class="col">
                    <div class="form-group" id="urSection">
                        <div id="parametersBox">
                            <div class="form-row">
                                <div class="col-md-4">
                                    <select name="budget_allotment_id[]"  class="form-control budgetcode_id select-search">
                                        <option value="">--Select Badget Code--</option>
                                        @foreach ($budgetcodes as $budgetcode)
                                            <option value="{{ $budgetcode->id }}">{{ $budgetcode->new_code ?? null }} ( {{ $budgetcode->old_code ?? null }} ) - {{ $budgetcode->range_name ?? null }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                
                                <div class="col-md-3">
                                    <input type="number" name="suplimentary_amount[]" id="suplimentary_amount"
                                        class="form-control" placeholder="Suplimentary Amount" />
                                </div>

                                {{-- <button id="addParam" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button> --}}

                            </div>
                            <div id="params">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <x-pondit-btn icon="check" title="{{ __('save') }}" />
            <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger"
                title="{{ __('cancel') }}" />
        </x-pondit-form>
        <x-slot name="cardFooter">
            <div></div>
            <div>
                <x-pondit-act-i url="{{ route('suplimentary_budget_allotment.index') }}" />
            </div>
            <div></div>
        </x-slot>
    </x-pondit-card>
@endsection
@push('js')
    <script>
        $("body").on('change','.budgetcode_id', function() {
            let budgetcode_id = ($(this).val());
            let range_id = $(this).parent().parent().find('.range');

            $.ajax({
                url: "{{ route('budgetcode.get_range') }}",
                method: 'GET',
                data: {
                    budgetcode_id
                },
                success: function(res) {
                    if (res.data.length > 0) {
                        let html = [];
                        res.data.forEach(element => {
                            html += `<option value="${element.id}">${element.title}</option>`;
                        });
                        range_id.html(html);
                    } else {
                        range_id.html(`<option value="">No Data Found</option>`);

                    }

                },
                error: function(err) {
                    console.log(err)
                },
            })
        });



        // $('body').on('change', '.selectexample', function() {
        //     let itemSerNo = ($(this).val());
        //     // console.log(itemSerNo);
        //     partNo               =   $(this).parent().parent().find('.part');
        //     contractItemSerNo    =   $(this).parent().parent().find('.contract_item_serno');
        //     axios.get( baseUrl + '/get-data/' + itemSerNo )
        //         .then(response=>{
        //             // console.log(response.data);
        //             let  part_no = response.data['part_no'];
        //             let  contract_ser_no = response.data['ser_no'];
        //             partNo.val(part_no);
        //             contractItemSerNo.val(contract_ser_no);
        //         })
        //         .catch(function (error) {
        //             console.log(error)
        //         })
        // });
    </script>

    <script>
        function getElementFromString(string) {
            let div = document.createElement('div');
            div.innerHTML = string;
            return div.firstElementChild;
        }

        let addedParamCount = 0;
        let btn = document.getElementById('addParam');
        btn.addEventListener('click', function(e) {
            e.preventDefault();
            let params = document.getElementById('params');
            let string = `
                            <div class="form-group mt-1" id="urSection">
                                <div id="parametersBox">
                                    <div class="form-row">
                                        <div class="col-md-4">
                                            <select name="budgetcode_id[]"  class="form-control budgetcode_id select-search">
                                                <option value="">--Select Badget Code--</option>
                                                @foreach ($budgetcodes as $budgetcode)
                                                    <option value="{{ $budgetcode->id }}">OldCode : {{ $budgetcode->oldcode }} || NewCode : {{ $budgetcode->newcode }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="range_id[]" class="form-control range select-search">
                                                <option value="">--Select Range--</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <input type="number" name="suplimentary_amount[]" id="suplimentary_amount"
                                                class="form-control" placeholder="Suplimentary Amount" />
                                        </div>
                                        <button
                                            class="btn btn-danger deleteParam ">
                                            x
                                        </button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>`;

            let paramElement = getElementFromString(string);

            params.appendChild(paramElement);

            let deleteParam = document.getElementsByClassName('deleteParam');
            for (item of deleteParam) {
                item.addEventListener('click', (e) => {
                    e.target.parentElement.remove();
                })
            }
            addedParamCount++;

        })

    </script>


@endpush
