<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class RoutesExposeController extends Controller
{
    public function exposeRoutes()
    {
        $actions = [];
        $routes = Route::getRoutes();
        foreach ($routes as $route) {
            if (
                (
                    (preg_match("/^App(.*)/i", trim($route->getActionName())) == 0)
                    &&
                    (preg_match("/^Pondit(.*)/i", trim($route->getActionName())) == 0)
                )
                ||
                 preg_match("/^App\\\\Http\\\\Controllers\\\\Auth(.*)/i", trim($route->getActionName())) > 0)
            {
                continue;
            }

            $actionName = $route->getActionName();

            // dump($actionName);
            $method = $route->methods()[0];
            $action = substr($actionName, strpos($actionName, '@') + 1);
            $namespace = substr($actionName, 0, strrpos($actionName, '\\'));
            
            $controller = substr($actionName, strrpos($actionName, '\\') + 1, -(strlen($action) + 1));
            
            //changes
            $controllerPath = $namespace.'\\'.$controller;

            $controllerObj = new $controllerPath;
            if(isset($controllerObj::$visiblePermissions[$action])){
                // dump($controllerObj::$visiblePermissions);
                $actions[$namespace][$controller][$method][$action] = $controllerObj::$visiblePermissions[$action]; // replace action with controller visible permission
            }

        }        
        ksort(  $actions);
        return json_encode([
            'status' =>'ok',
            'data' =>$actions
        ]);
    }
}
