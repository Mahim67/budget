<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PermissionTestController extends Controller
{
    public static $visiblePermissions = [
        'testAuthorization' => 'Test Authorization'
    ];

    public function testAuthorization(){        
        echo 'hello';
    }
}
