<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

Route::get('/redirect', function (Request $request) {
    $request->session()->put('state', $state = Str::random(40));

    $request->session()->put(
        'code_verifier', $code_verifier = Str::random(128)
    );
    
    $codeChallenge = strtr(rtrim(
        base64_encode(hash('sha256', $code_verifier, true))
    , '='), '+/', '-_');

    $query = http_build_query([
        'client_id' => config('pondit_profile.client_id'),
        'redirect_uri' => config('pondit_profile.client_redirect').'/callback',
        'response_type' => 'code',
        'scope' => '',
        'state' => $state,
        'code_challenge' => $codeChallenge,
        'code_challenge_method' => 'S256',
    ]);

    return redirect(config('pondit_profile.pondit_profile_url').'/oauth/authorize?'.$query);
})->name('getToken');

Route::get('/callback', function (Request $request) {
    $state = $request->session()->pull('state');
    $codeVerifier = $request->session()->pull('code_verifier');
    throw_unless(
        strlen($state) > 0 && $state === $request->state,
        InvalidArgumentException::class
    );

    $http = new GuzzleHttp\Client;

    $requestData= [
        'grant_type' => 'authorization_code',
        'client_id' => config('pondit_profile.client_id'),
        'redirect_uri' => config('pondit_profile.client_redirect').'/callback',
        'code_verifier' => $codeVerifier,
        'code' => $request->code,
    ];

    return view('login-process', compact('requestData'));

    /*
    $client = new GuzzleHttp\Client([
        'header' => [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ],
        'verify' => false
    ]);

    $response = $client->request('POST', 'http://aims-master.pondit.com/oauth/token/', [
        'form_params' => $requestData
    ]);   

    $response = Http::asForm()->post('http://aims-master.pondit.com/oauth/token', $postData);
    $data = json_decode((string) $response->getBody(), true);
    session(['access_token'=>$data['access_token']]);
    return redirect('/pondit-profile');
    */
});

Route::get('/pondit-profile', function () {
    $http = new GuzzleHttp\Client;
    $response = $http->request('GET', config('pondit_profile.pondit_profile_url').'/api/pondit-profile', [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.session('access_token'),
        ],
    ]);
        
    $data = json_decode((string) $response->getBody(), true);
    session(['pondit_profile'=>$data ]);
    $userInfo = $data['info'];  
    $user  = User::where('service_number', $userInfo['service_number'])->first();

    Auth::login($user);
    return redirect()->intended('dashboard');
});
