<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/pondit_profile.php';

Route::get('/callback/routes', 'App\Http\Controllers\RoutesExposeController@exposeRoutes');


Route::get('/test_authorization', 'App\Http\Controllers\PermissionTestController@testAuthorization')
->middleware(['auth', 'app-authorize'])
->name('check_permissions');


