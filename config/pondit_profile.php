<?php

return [
    'client_id' => 9,
    'client_redirect' => 'http://budget.ponditapp.com',
    // 'pondit_profile_url' => 'http://aims-master.pondit.test',
    'pondit_profile_url' => 'http://aims-master.pondit.com',
    'permissions'=> [
        'spent'=>[
            'create'=>1,
            'edit'=>2
        ]
    ]
];
